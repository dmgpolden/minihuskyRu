<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-backend',
 //       'defaultRoute'=>'/post/default/index',
        'language'=>'ru',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'bootstrap' => ['log',
        'post',
     ], //'rbac',,'admin'
     'modules' => [

'present' => [
            'class' => 'common\modules\present\Module',
        ], //page
'file' => [
            'class' => 'common\modules\file\Module',
        ], //page
'page' => [
            'class' => 'common\modules\page\Module',
        ], //page
'post' => [
            'class' => 'common\modules\post\Module',
        ], //post
'pet' => [
            'class' => 'common\modules\pet\Module',
                    ], 
'dform' => [
            'class' => 'common\modules\dform\Module',
          ], 
    ], //modules
    'components' => [
        'user' => [
            'identityClass' => 'budyaga\users\models\User',
            'enableAutoLogin' => true,
            'loginUrl' => '/login',
            ],
/*        'authManager' => [
            'class' => 'yii\rbac\PhpManager', // or use 'yii\rbac\DbManager'
            'defaultRoles' => ['admin', 'admin'],
        //    'class' => 'yii\rbac\DbManager', // or use 'yii\rbac\DbManager'
        ],
*/
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
//                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'request'=>['cookieValidationKey'=>123],//yii\web\Request::cookieValidationKey must be configured with a secret key.
    ],
    'params' => $params,
];
