<?php
use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;

/* @var $this \yii\web\View */
/* @var $content string */

//vd(\Yii::$app);

AppAsset::register($this);

  header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
  header("Last-Modified: " . gmdate("D, d M Y H:i:s")." GMT");
  header("Cache-Control: no-store, no-cache, must-revalidate");
  header("Cache-Control: post-check=0,pre-check=0", false);
  header("Cache-Control: max-age=0", false);
  header("Pragma: no-cache");
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
<?php
//vd( Yii::$app->keyStorage->get('favicon') );
//vd( is_file(Yii::getAlias('@static/favicon/'.Yii::$app->keyStorage->get('favicon'))) );
/*
if (file_exists(Yii::getAlias('@static/favicon/'.Yii::$app->keyStorage->get('favicon'))) && is_file(Yii::getAlias('@static/favicon/'.Yii::$app->keyStorage->get('favicon'))) ) {
    echo \rmrevin\yii\favicon\Favicon::widget([
        'web' => '@web/favicon',
        'webroot' => '@webroot/favicon',
        'favicon' => '@static/favicon/'.Yii::$app->keyStorage->get('favicon'),
        'color' => '#2b5797',
        'viewComponent' => 'view',
    ]);
}
*/
?>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta http-equiv="Cache-Control" content="no-cache">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
    <?php $this->beginBody() ?>
    <div class="wrap">
        <?php
            NavBar::begin([
                'brandLabel' => '__',
                'brandUrl' => Yii::$app->homeUrl,
                'options' => [
                    'class' => 'navbar-inverse navbar-fixed-top',
                ],
            ]);
            $pageItems = [
               // ['label' => 'Home', 'url' => ['/site/index']],
                ['label' => Yii::t('app','Page'), 'url' => ['/page/default/index']],
	            ['label' => 'Statuses', 'url' => ['/page/status/index']],
                ['label' => Yii::t('app','Present'), 'url' => ['/present/default/index']],
                ['label' => Yii::t('post','Category'), 'url' => ['/post/category/index']],
            ];
        $categories=\common\modules\post\models\Category::find()->where(['lft'=>1])->all();
        foreach($categories as $category){
             $pageItems[]=[
                            'label' => Yii::t($category->slug,ucfirst($category->slug)),
                            'url' => ['/post/' . $category->slug . '/index']
                          ];
        }
            echo Nav::widget([
                'options' => ['class' => 'navbar-nav navbar-right'],
                'items' => $pageItems,
            ]);
            NavBar::end();
        ?>

        <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= $content ?>
        </div>
    </div>

    <footer class="footer">
        <div class="container">
        <p class="pull-left">&copy; My Company <?= date('Y') ?></p>
        <p class="pull-right"><?= Yii::powered() ?></p>
        </div>
    </footer>

    <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
