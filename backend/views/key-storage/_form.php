<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use vova07\fileapi\Widget as FileAPI;

/* @var $this yii\web\View */
/* @var $model common\models\KeyStorageItem */
/* @var $form yii\bootstrap\ActiveForm */
?>

<div class="key-storage-item-form">

    <?php $form = ActiveForm::begin(['enableClientValidation'=>false]); ?>

    <?= $form->field($model, 'key')->textInput() ?>
<?php
if ($model->key == 'favicon' ){
echo $form->field($model, 'value')->widget(
    FileAPI::className(),
    [
        'crop' => true,
        'cropResizeMaxWidth' => 300,
        'settings' => [
            'autoUpload' => false,
            'url' => ['fileapi-upload'],
        ]
    ]
);
}else{
?>
    <?= $form->field($model, 'value')->textInput() ?>
<?php } ?>

    <?= $form->field($model, 'comment')->textarea() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('backend', 'Create') : Yii::t('backend', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
