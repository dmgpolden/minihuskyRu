<?php

namespace frontend\widgets;

use \Yii;
use yii\base\Widget;
    
class Saleterm extends Widget
{
    public function run()
    {
        $model = new \frontend\models\SaletermForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail(Yii::$app->params['adminEmail'])) {
                Yii::$app->session->setFlash('success', Yii::t('app', 'Thanks for the questionnaire'));
          return $this->render('saleterm', [
                'model' => false,
            ]);
            } else {
                Yii::$app->session->setFlash('error', Yii::t('app', 'There was a mistake, try later'));
          return $this->render('saleterm', [
                'model' => $model,
            ]);
            }
        } else {
          return $this->render('saleterm', [
                'model' => $model,
            ]);
        }
    }

}

