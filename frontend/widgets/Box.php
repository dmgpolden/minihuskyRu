<?php

namespace frontend\widgets;

use yii\base\Widget;
use frontend\modules\page\models\Page;
use frontend\modules\post\models\Post;

class Box extends Widget
{
    public $limit;
		public $box;

    public function init()
    {
        parent::init();
        if ($this->limit === null) {
            $this->limit = 10;
        }
                $this->box=array_merge(Page::box($this->limit), Post::box($this->limit));
//                vd($this->box);
    }

    public function run()
    {
			return $this->render( 'box',
				[
					'box'=>$this->box
				]
			
			);
		}
}

