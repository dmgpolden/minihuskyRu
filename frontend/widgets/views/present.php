<?php

?>

<div class="present">							

<?= yii\bootstrap\Carousel::widget([
    'items' => $items,
		'showIndicators'=>false,
		'options'=>['class'=>'slide'],
		'controls' => ['<span class="glyphicon glyphicon-chevron-left"></span>','<span class="glyphicon glyphicon-chevron-right"></span>'],
]);
?>

</div> 
