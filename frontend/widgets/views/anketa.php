<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\switchinput\SwitchInput;
?>


<div class="col-lg-20 col-push-2">

<?php
$form = ActiveForm::begin([
    'id' => 'login-form',
    'options' => ['class' => 'form-horizontal'],
]) ?>
<?=$form->field($model, 'username');?>
<?=$form->field($model, 'old');?>
<?=$form->field($model, 'country');?>
<?=$form->field($model, 'city');?>
<?=$form->field($model, 'tel')->widget(yii\widgets\MaskedInput::classname(),['mask' => '9-999-999-99-99']);?>
<?=$form->field($model, 'email');?>
<?=$form->field($model, 'whereinfo');?>
<?=$form->field($model, 'whenbuy');?>
<?=$form->field($model, 'wasdog')->radioList([1=>'ДА',0=>'НЕТ']);?>
<?=$form->field($model, 'havepet')->radioList([1=>'ДА',0=>'НЕТ']);?>
<?=$form->field($model, 'havechildren')->radioList([1=>'ДА',0=>'НЕТ']);?>
<?=$form->field($model, 'olddog')->radioList($model->olddogOptions());?>
<?=$form->field($model, 'sexdog')->radioList($model->sexdogOptions());?>
<?=$form->field($model, 'colordog')->radioList($model->colordogOptions());?>
<?=$form->field($model, 'eyerdog')->radioList($model->colordogOptions());?>
<?=$form->field($model, 'typedog')->radioList($model->typedogOptions());?>
<?=$form->field($model, 'body')->textArea();?>

    <div class="form-group">
        <div class="col-lg-offset-1 col-lg-11">
            <?= Html::submitButton('Отправить', ['class' => 'btn btn-default']) ?>
        </div>
    </div>

<?php
 ActiveForm::end() ?>
</div>

