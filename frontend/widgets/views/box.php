<?php

use yii\helpers\Html;
use yii\helpers\Url;
use frontend\modules\page\Module;
?>

<div class="box">							
					<? foreach ( $box as $n ) : ?>
		<div class="col-xs-24 col-sm-8 col-md-8 ">
				<?=Html::img( '@big'. $n['image'],['class'=>'img-thumbnail'] );?>
				<h2 class='box'>
				<?=Html::a( $n['label'], Url::to( $n['url'] ) );?>
				</h2>
				<div class='caption'>
				<?=$n['caption'];?>
				</div>
		</div>
					<? endforeach ;?>
</div> 

