<?php

namespace frontend\widgets;

use \Yii;
use yii\base\Widget;
use common\modules\present\models\Present as P;
use yii\helpers\Html;
use yii\helpers\Url;

class Present extends Widget
{
    public $limit;
        public $present;

    public function init()
    {
        parent::init();
        if ($this->limit === null) {
            $this->limit = 10;
        }
        $this->present=P::getDb()->cache(function ($db) {
            return P::find()->limit($this->limit)->all();
            },
            24*3600,
            new \yii\caching\TagDependency(['tags'=>['present']])
            );
    }

    public function run()
    {
            if ( $this->present )
            {
                foreach(  $this->present as $p) 
                {    
                        $items[]=
                            ['content'=>Html::a('
<picture style="max-width: 100%;">
    <source srcset="/mdl/'.$p->name.'" media="(max-width: 360px)">
    <source srcset="/large/'.$p->name.'" media="(max-width: 768px)">
    <source srcset="/sl/'.$p->name. '">
    <img src="/mdl/'.$p->name. ' "srcset="/sl/'.$p->name . '" alt="фото Аляскинские кли кай">
</picture>
'
                                    , $p->url ) ,
                            'caption'=> Html::a( $p->description , $p->url )] ;
                }

            return $this->render( 'present',
                [
                    'items'=>$items
                ]
                );
            }
        }
}

