<?php

namespace frontend\widgets;

use \Yii;
use yii\base\Widget;
    
class Anketa extends Widget
{
    public function run()
    {
                $thanksMessage = "Спасибо за обращение!
                Мы постараемся ответить вам как можно скорее, но получаем большое количество писем, по этому, если вам нужен ответ срочно, то обратитесь к нам в Viber/WhatsApp по телефону +7-926-847-18-97";
        $model = new \frontend\models\AnketaForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail(Yii::$app->params['adminEmail'])) {
                Yii::$app->session->setFlash('success', $thanksMessage );
          return $this->render('anketa', [
                'model' => false,
            ]);
            } else {
                Yii::$app->session->setFlash('error', Yii::t('app', 'There was a mistake, try later'));
          return $this->render('anketa', [
                'model' => $model,
            ]);
            }
        } else {
          return $this->render('anketa', [
                'model' => $model,
            ]);
        }
    }

}
