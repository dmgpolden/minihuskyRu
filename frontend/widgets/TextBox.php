<?php

namespace frontend\widgets;

use yii\base\Widget;
use frontend\modules\page\models\Page;
use frontend\modules\post\models\Post;

class TextBox extends Widget
{
    public $limit;
		public $textbox;

    public function init()
    {
        parent::init();
        if ($this->limit === null) {
            $this->limit = 10;
        }
        
        $this->textbox=array_merge(Page::getTextbox($this->limit), Post::getTextbox($this->limit));
    }

    public function run()
    {
			return $this->render( 'textbox',
				[
					'textbox'=>$this->textbox
				]
			
			);
		}
}

