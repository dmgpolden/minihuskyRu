<?php
return [
    '/site/*' => [
        'type' => 2,
    ],
    'readesite' => [
        'type' => 2,
    ],
    'reader' => [
        'type' => 1,
        'children' => [
            'readesite',
        ],
    ],
];
