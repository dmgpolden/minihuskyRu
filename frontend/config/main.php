<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);
    $theme = 'klikay' ;
    if(array_key_exists('theme',$_COOKIE) )
        $theme = $_COOKIE['theme'];
    
    if (isset ($_GET['theme'] )  ){
        $theme = $_GET['theme'];
        SetCookie("theme",$theme);
        }

Yii::setAlias('@theme', '@frontend/themes/'.$theme);
return [
    'basePath' => dirname(__DIR__),
    'bootstrap' => [
                    'log',
                    'frontend\modules\post\Bootstrap',
                    'post',
                    'page',
        ],
    'controllerNamespace' => 'frontend\controllers',
    'components' => [
        'user' => [
            'identityClass' => 'budyaga\users\models\User',
        ],
        'i18n' => [
            'translations' => [
                'app'=>[
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath'=>'@app/messages',
                ],
            ],
        ],
                'urlManager' => [
                        'enablePrettyUrl' => true,
                        'showScriptName' => false,
                'rules'=>[
                   '/'=>'site/index',
                   'search'=>'site/search',
                    'contact'=>'site/contact',
                    'kontakt'=>'site/contact',
//                    'news'=>'post/news/index',
//                    'news/<id:.*>'=>'post/news/view',
//                    'article'=>'post/article/index',
//                    'article/<id:.*>'=>'post/article/view',
//                    'gallery'=>'post/album/index',
//                    'gallery/<id:.*>'=>'post/album/view',
    //              '<module:\w+>/<controller:\w+>/<action:\w+>/'=>'<module>/<controller>/<action>',
    //              '<controller:\w+>/<action:\w+>/*'=>'<controller>/<action>',
                    'page'=>'page/default/index',
                    'page/search'=>'page/default/search',
                    'page/<id:.*>'=>'page/default/view',
                    '<slug:[\w\-_]+>'=>'page/default/slugview',
                   ]
                ],
'formatter' => [
            'nullDisplay' => '&nbsp;',
            'locale' => 'ru-RU',
        ],
       'assetManager' => [
        'bundles' => [
            'yii\bootstrap\BootstrapAsset' => [
                    'sourcePath' => '@theme/assets',
//                    'basePath' => '@webroot',
//                    'baseUrl' => '@web',
                    'css' => [
                        'css/site.css'
                        ],
                    'js' => ['js/bootstrap.min.js'],
                ],
            'yii\bootstrap\BootstrapPluginAsset' => [
                    'sourcePath' => '@theme/assets',
                    'js' => ['js/bootstrap.min.js'],
                ],
            'yii\web\JqueryAsset' => [
                    'sourcePath' => '@theme/assets',
//                    'basePath' => '@webroot',
//                    'baseUrl' => '@web',
                    'js' => ['js/jquery.min.js'],
                ],
         ],
                 'linkAssets' => true
        ],

                    'view' => [
                            'theme' => [
                                'pathMap' => [
                                    '@app/views' =>'@theme/views', //layouts work
                                    '@app/modules' =>'@theme/modules',//not test
                                    '@app/widgets/views' =>'@theme/widgets', // work for extension
//                                    '@dektrium/user/views' =>'@theme/dektrium/user', // work for extension
                                ],
                                'baseUrl' => '@web/themes/'.$theme,
                            ],
                                
                    ],
        'request'=>[
//                        'class'=>'common\components\LangRequest',
        'cookieValidationKey'=>123],//yii\web\Request::cookieValidationKey must be configured with a secret key.
/*        'access' => [
        'class' => 'mdm\admin\components\AccessControl',
        'allowActions' => [
            'site/*', // add or remove allowed actions to this list
            'shop/*', // add or remove allowed actions to this list
            'page/*', // add or remove allowed actions to this list
        ],
      ],
*/        
    'authClientCollection' => [
        'class' => 'yii\authclient\Collection',
        'clients' => [
            'google' => [
                'class' => 'yii\authclient\clients\GoogleOpenId'
            ],
            'facebook' => [
                'class' => 'yii\authclient\clients\Facebook',
                'clientId' => 'facebook_client_id',
                'clientSecret' => 'facebook_client_secret',
            ],
        ],
    ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
            'mail' => [
                     'class' => 'yii\swiftmailer\Mailer',
                      ],
'mobiledetect' => [
            'class' => 'dkeeper\mobiledetect\Detect',
        ],
    ], //end components

        'modules' => [
'page' => [
            'class' => 'frontend\modules\page\Module',
        ], //page
'post' => [
            'class' => 'frontend\modules\post\Module',
        ], //shop
    ], //end modules

    'params' => $params,
];
