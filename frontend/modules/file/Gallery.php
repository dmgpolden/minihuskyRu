<?php

namespace frontend\modules\file;

use yii\base\Widget;
use frontend\modules\file\models\File;

class Gallery extends Widget
{
    public $limit;
		public $gallery;

    public function init()
    {
        parent::init();
        if ($this->limit === null) {
            $this->limit = 4;
        }
				$this->gallery=File::getGallery($this->limit);
    }

    public function run()
    {
			if( $this->gallery){
				return $this->render( 'gallery',
					[
						'gallery'=>$this->gallery
					]
				
				);
			}
		}
}


