<?php

namespace frontend\modules\file\models;

use Yii;
use yii\helpers\ArrayHelper;
use \yii\db\Query;
use  common\modules\post\models\Post;
use common\modules\file\models\File as BaseFile;
;

/**
 * This is the model class for table "page".
 *
 * @property integer $id
 * @property integer $root
 * @property integer $lft
 * @property integer $rgt
 * @property integer $level
 * @property string $name
 * @property string $caption
 * @property string $content
 * @property integer $visible
 * @property integer $sequence
 * @property string $note
 * @property string $slug
 * @property string $title
 * @property string $description
 * @property string $keywords
 * @property integer $go_to_type
 * @property string $external_link
 * @property integer $external_link_type
 * @property integer $removable
 * @property integer $image
 * @property integer $present
 * @property integer $box
 * @property integer $textbox
 */
class File extends BaseFile
{

	public $filepath;		
	public $image;		


		public static function getGallery($limit)
		{
			$res=array( );
				$query = (new Query())
				//->select(['*',])
				->select(['CONCAT( "/", name,".",ext) as image', self::tableName().'.*',])
				->from(self::tableName())
				->leftJoin(Post::tablePostFile(),Post::tablePostFile().'.file_id='.self::tableName().'.id')
//				->where(['item_type' =>ItemFile::ITEM_MENU])
//				->groupBy( self::tableName().'.id')
					->orderBy( 'RAND()')
					->limit( $limit)
						;
				
 $command=$query->createCommand();
	$command->fetchMode=[\PDO::FETCH_CLASS,__CLASS__];
	  $res = $command->queryAll( );
//vd( $command->sql);
//vd( $res,false);
					
				return $res;
		}

}
