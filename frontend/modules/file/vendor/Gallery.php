<?php
/**
 * @copyright Copyright (c) 2013 2amigOS! Consulting Group LLC
 * @link http://2amigos.us
 * @license http://www.opensource.org/licenses/bsd-license.php New BSD License
 */
namespace frontend\modules\file\vendor;

use dosamigos\gallery\Gallery as BaseGallery;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/**
 * Gallery renders a BlueImp Gallery items
 *
 * @author Antonio Ramirez <amigo.cobos@gmail.com>
 * @link http://www.ramirezcobos.com/
 * @link http://www.2amigos.us/
 * @package dosamigos\gallery
 */
class Gallery extends BaseGallery
{

    /**
     * @param mixed $item
     * @return null|string the item to render
     */
    public function renderItem($item)
    {
        if (is_string($item)) {
            return Html::a(Html::img($item), $item, ['class' => 'gallery-item']);
        }
        $src = ArrayHelper::getValue($item, 'src');
        if ($src === null) {
            return null;
        }
        $url = ArrayHelper::getValue($item, 'url', $src);
        $options = ArrayHelper::getValue($item, 'options', []);
        Html::addCssClass($options, 'gallery-item');

        return Html::a(Html::img($src,['class'=>'img-thumbnail']), $url, $options);
    }

} 
