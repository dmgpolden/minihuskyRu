<?php

use yii\helpers\Html;
use yii\helpers\Url;
use frontend\modules\file\vendor\Gallery;

?>

<?php

	foreach ($gallery as $item) {
		$items[]=[
				'url' =>  '@static/big'.$item->image, 
				'src' => '@static/tmb'. $item->image,
				'options'=>['class'=>'col-xs-8 col-sm-4 col-md-4'],
				 ];
	}
?>

<?= Gallery::widget( ['items' => $items]);?>

