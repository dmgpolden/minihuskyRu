<?php

namespace frontend\modules\shop;

use yii\base\Widget;
use frontend\modules\shop\models\Shop;

class LastShop extends Widget
{
    public $limit;
		public $lastshop;

    public function init()
    {
        parent::init();
        if ($this->limit === null) {
            $this->limit = 10;
        }
				$this->lastshop=Shop::getLast($this->limit);
    }

    public function run()
    {
			return $this->render( 'lastshop',
				[
					'lastshop'=>$this->lastshop
				]
			
			);
		}
}

