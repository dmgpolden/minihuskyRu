<?php

namespace frontend\modules\shop\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\modules\shop\models\Shop;

/**
 * ShopSearch represents the model behind the search form about `frontend\modules\shop\models\Shop`.
 */
class ShopSearch extends Shop
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [[ 'visible', 'sequence', 'go_to_type', 'external_link_type', 'removable', 'image', 'present', 'box', 'textbox'], 'integer'],
            [['name', 'caption', 'content', 'note', 'slug', 'title', 'description', 'keywords', 'external_link'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Shop::find()->with( 'imagen');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }
        
        $query->andFilterWhere([
            'id' => $this->id,
            'visible' => $this->visible,
            'sequence' => $this->sequence,
            'go_to_type' => $this->go_to_type,
            'external_link_type' => $this->external_link_type,
            'removable' => $this->removable,
            'image' => $this->image,
            'present' => $this->present,
            'box' => $this->box,
            'textbox' => $this->textbox,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'caption', $this->caption])
            ->andFilterWhere(['like', 'content', $this->content])
            ->andFilterWhere(['like', 'note', $this->note])
            ->andFilterWhere(['like', 'slug', $this->slug])
            ->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'keywords', $this->keywords])
            ->andFilterWhere(['like', 'external_link', $this->external_link]);

        return $dataProvider;
    }
}
