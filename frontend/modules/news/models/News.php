<?php

namespace frontend\modules\shop\models;

use Yii;
use creocoder\behaviors\NestedSet;
use yii\helpers\ArrayHelper;
use \yii\db\Query;
use \yii\helpers\Html;
use \yii2\web\UploadedFile;
use common\modules\file\models\ItemFile;
use common\modules\file\models\File;
/**
 * This is the model class for table "shop".
 *
 * @property integer $id
 * @property integer $root
 * @property integer $lft
 * @property integer $rgt
 * @property integer $level
 * @property string $name
 * @property string $caption
 * @property string $content
 * @property integer $visible
 * @property integer $sequence
 * @property string $note
 * @property string $slug
 * @property string $title
 * @property string $description
 * @property string $keywords
 * @property integer $go_to_type
 * @property string $external_link
 * @property integer $external_link_type
 * @property integer $removable
 * @property integer $image_id
 * @property integer $present
 * @property integer $box
 * @property integer $textbox
 */
class News extends \yii\db\ActiveRecord
{
	public $image;		
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'shop';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [[ 'visible', 'sequence', 'go_to_type', 'external_link_type', 'removable', 'present', 'box', 'textbox'], 'integer'],
            [['content'], 'string'],
            [['name'], 'required'],
            [['name', 'caption', 'slug', 'title', 'description', 'keywords', 'external_link'], 'string', 'max' => 255],
            [['note'], 'string', 'max' => 200],
            [['image'], 'save'],

        ];
    }
    public function getImage()
    {
		 return $this->hasOne(File::className(), ['id' => 'file_id'])
            ->viaTable('item_file', ['item_id' => 'id']);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name' => 'Name',
            'caption' => Yii::t('shop','Caption'),
            'content' => 'Content',
            'visible' => 'Visible',
            'sequence' => 'Sequence',
            'note' => 'Note',
            'slug' => 'Slug',
            'title' => 'Title',
            'description' => 'Description',
            'keywords' => 'Keywords',
            'go_to_type' => 'Go To Type',
            'external_link' => 'External Link',
            'external_link_type' => 'External Link Type',
            'removable' => 'Removable',
            'image' => 'Image',
            'present' => 'Present',
            'box' => 'Box',
            'textbox' => 'Textbox',
        ];
    }
	public function behaviors() {
		return [
								/*	'slug' => [
									'class' => 'Zelenin\yii\behaviors\Slug',
									'source_attribute' => 'name',
									'slug_attribute' => 'slug',
									// optional params
									'translit' => true,
									'replacement' => '-',
									'lowercase' => true,
									'unique' => true
							]
*/
		];

	}
    
		public static function find()
    {
			return parent::find( )->where( 'visible = 1');
    }

/*    public static function find()
    {
        return new ShopQuery(get_called_class());
    }
*/
		public static function getOption( )
		{	
			$items = self::getAll();
		return  $items;
		}
		public static function getList( )
		{	
			$items = (new Query())
			->select(['id','name','level'])
			->from(self::tableName())
			->orderBy( 'id DESC' )
			->all();
				foreach ( $items as $key=>$item ){
					$res[$item['id']] = str_repeat('-', $item['level']-1) . $item['name'];
				}
			return $res;
			//return ArrayHelper::map($items,'id', 'name');
		}

		public function getImages()
		{
			$res=array( );
				$item = (new Query())
				->select(['file_id'])
				->from(ItemFile::tableName())
				->where([
						'item_id' => $this->id,
						'item_type' => ItemFile::ITEM_NEWS,
						])
						;
				
				$query = (new Query())
				->select(['CONCAT( "/", SUBSTRING( filename,1,2),"/",filename,".",fileext) as image','id'])
				->from(File::tableName())
				->where(['id'=>$item]);
//				->all();
 $command=$query->createCommand();
	$command->fetchMode=[\PDO::FETCH_CLASS,__CLASS__];
	  $res = $command->queryAll( );
//vd( $res);
					
				return $res;
		}
		
		public static function getLast($last=10)
		{	
			$query = (new Query())
			->select([self::tableName().'.*','CONCAT( "/", SUBSTRING( filename,1,2),"/",filename,".",fileext) as image',])
			->from(self::tableName())
				->leftJoin(ItemFile::tableName(),self::tableName().'.id=item_id AND item_type='.ItemFile::ITEM_NEWS)
				->leftJoin(File::tableName(),File::tableName().'.id=file_id')
			->andWhere( 'visible = 1')
				->groupBy( self::tableName().'.id')
			->orderBy( 'id DESC' )
			->limit( $last);
 $command=$query->createCommand();
	$command->fetchMode=[\PDO::FETCH_CLASS,__CLASS__];
	  $res = $command->queryAll( );
		return $res;
		}
		
		public static function getAll( )
		{	
			$rows = (new Query())
			->select(['*'])
			->from(self::tableName())
			->andWhere( 'visible = 1')
			->orderBy( 'id DESC' )
			->all();
		return $rows;
		}
		
		public static function getNews()
		{	
			$items =  self::getAll();
				$level = 0;
				$res='';
				foreach ($items as $n => $shop)
				{ 
					if ($shop['level'] == $level) {
						$res.= Html::endTag('li') . "\n";
					} elseif ($shop['level'] > $level) {
						$res.= Html::beginTag('ul') . "\n";
					} else {
						$res.= Html::endTag('li') . "\n";

						for ($i = $level - $shop['level']; $i; $i--) {
							$res.= Html::endTag('ul') . "\n";
							$res.= Html::endTag('li') . "\n";
						}
					}
					$res.= Html::beginTag('li');
					$res.= Html::a( Html::encode($shop['name']) , ['/page/'.$shop['slug'] ]  ) ; 
					$level = $shop['level'];
				}
				for ($i = $level; $i; $i--) {
					$res.= Html::endTag('li') . "\n";
					$res.= Html::endTag('ul') . "\n";
				}
		return $res;
		}
		 public function afterSave( $insert,$changedAttributes){

        parent::afterSave( $insert,$changedAttributes);
		}


}
