<?php

namespace frontend\modules\shop\models;

use Yii;
use creocoder\behaviors\NestedSetQuery;

class ShopQuery extends \yii\db\ActiveQuery
{
    public function behaviors() {
        return [
            [
                'class' => NestedSetQuery::className(),
            ],
        ];
    }
}

