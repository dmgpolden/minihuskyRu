<?php

namespace frontend\modules\shop;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'frontend\modules\shop\controllers';

    public function init()
    {
        parent::init();
				 $this->registerTranslations( );
		}

		public function registerTranslations()
		{

        \Yii::$app->i18n->translations['shop'] = [
            'class' => 'yii\i18n\PhpMessageSource',
            'basePath' =>__DIR__.'/messages',
        ];

        // custom initialization code goes here
    }

		 public static function t( $message, $params = [], $language = null)
		 {
				         return \Yii::t( 'shop' , $message, $params, $language);
			}	
}
