<?php

use yii\helpers\Html;
use yii\helpers\Url;
use frontend\modules\shop\Module;
?>

<div class="shop">							
					<?=Module::t('Last shop');?>
					<? foreach ( $lastshop as $n ) : ?>
				<div class='date'>
<?=\Yii::$app->formatter->asDate( $n->create_at, 'long') ?>
				</div>
				<div class='name'>
				<?=Html::a( $n->name, Url::to( ['/shop/default/view','id'=>$n->id] ) );?>
				</div>
				<div class='caption'>
				<?=$n->caption;?>
				</div>

					<? endforeach ;?>
				<?=Html::a( Module::t('All shop'),  Url::to( ['/shop/default/index'] ) );?>
</div> 
