<?php

namespace frontend\modules\post\models;

use yii\db\ActiveQuery;
use \yii\db\Query;
use \yii\helpers\ArrayHelper;

/**
 * This is the model class for table "tbl_category".
 *
 * @property string $id
 * @property string $tree
 * @property string $lft
 * @property string $rgt
 * @property integer $depth
 * @property string $title
 * @property string $description
 */
class Category extends \yii\db\ActiveRecord {

    public $parent;
    public $parent_name;
    public $next;
    public $prev;
    /**
     * @inheritdoc
     */


    public static function tableName() {
        return '{{%post_category}}';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
                [ [ 'tree', 'lft', 'rgt', 'depth' ], 'integer' ],
                [ [ 'title','slug' ], 'required' ],
                [ [ 'description' ], 'string' ],
                [ [ 'title' ], 'string', 'max' => 255 ],
                [['parent'], 'safe']
        ];
    }
    
    public static function findFullOne($id)
    {
        $cat=self::tableName();
            $primaryKey = parent::primaryKey();
            if (isset($primaryKey[0])) {
                $condition = [$cat.'.'.$primaryKey[0] => $id];
            } else {
                throw new InvalidConfigException(get_called_class() . ' must have a primary key.');
            }
        $query = parent::find($id)
            ->select($cat.'.*,prev.id as prev, next.id as next, parent.id as parent,parent.title as parent_name')
            ->leftJoin($cat.' parent', $cat.'.lft > parent.lft AND '. $cat.'.rgt<parent.rgt AND '. $cat.'.tree=parent.tree')
            ->leftJoin($cat.' prev', $cat.'.lft-1 = prev.rgt AND '. $cat.'.tree=prev.tree')
            ->leftJoin($cat.' next', $cat.'.rgt+1 = next.lft AND '. $cat.'.tree=next.tree')
            ->andWhere($condition);
        return $query->one();
    }

    public static function findFull()
    {
        $cat=self::tableName();
        $query = parent::find()
            ->select($cat.'.*,prev.id as prev, next.id as next, parent.id as parent,parent.title as parent_name')
            ->leftJoin($cat.' parent', $cat.'.lft > parent.lft AND '. $cat.'.rgt<parent.rgt AND '. $cat.'.tree=parent.tree')
            ->leftJoin($cat.' prev', $cat.'.lft-1 = prev.rgt AND '. $cat.'.tree=prev.tree')
            ->leftJoin($cat.' next', $cat.'.rgt+1 = next.lft AND '. $cat.'.tree=next.tree')
            ->orderBy( [ 'tree' => SORT_ASC, 'lft' => SORT_ASC]);
        return $query;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
                'title' => \Yii::t ( 'app', 'Title' ),
                'description' => \Yii::t ( 'app', 'Description' )
        ];
    }
    /**
     * Configure extra behaviors
     *
     * @see \yii\base\Component::behaviors()
     */
    public function behaviors() {
        return [
                'nestedSet' => [
                    
                       'class' => \creocoder\nestedsets\NestedSetsBehavior::className(),
                ]
        ];
    }

    public static function trees()
    {
            $q=new Query();
        return $query = $q->find()->andWhere("[[$this->leftAttribute]] = 1");
    }
    
    public static function options(){
                $res = self::find()
                ->select(['id,CONCAT(REPEAT("-",depth),title) as text'])
                ->from(self::tableName())
                ->orderBy(['tree'=>SORT_ASC,'lft'=>SORT_ASC])
                ->asArray()
                ->all()
                ;
        return ArrayHelper::map($res,'id','text');
    }


    public static function listoption(){
                $query = (new Query())
                ->select(['id as value,CONCAT(REPEAT("-",depth),title) as text'])
                ->from(self::tableName())
                ->orderBy(['tree'=>SORT_ASC,'lft'=>SORT_ASC])
                ;
    $command=$query->createCommand();
    $command->fetchMode=[\PDO::FETCH_ASSOC];
      $res = $command->queryAll( );
        return $res;
    }

        public static function getBreadcrumbs($cat)
        {
                $cat=self::find()
                    ->select(['id','lft','rgt'])
                    ->andWhere(['=','id',$cat])
                    ->one();
                   ;
        if($cat)
         return self::normalizeItems(
            $query = (new Query())
            ->select(['label'=>'title','slug','id'])
            ->from(self::tableName())
            ->andWhere(['<=','lft',$cat->lft])
            ->andWhere(['>=','rgt',$cat->rgt])
            ->andWhere(['tree'=>$cat->tree])
            ->andWhere(['>','depth',1])
            ->orderBy( 'lft' )
                ->all());
         else 
             return [];

        }

    protected static function normalizeItems($items )
    {
            $res=[];
//            vd($items);
        foreach ($items as $i => $item) {
            $res[$i]['label'] = $item['label'];
                            if (  $item['slug'] == null ){
                                $res[$i]['url'] =['/post/'.$items[0]['slug'].'/index', 'category_id'=> $item['id']];
                            }
                            else{
                                $res[$i]['url'] =['/post/'.$items[0]['slug'].'/slugview', 'slug'=> $item['slug']];
                            }
            if (isset($item['items'])) {
                $res[$i]['items'] = self::normalizeItems($item['items']);
                if (empty($res[$i]['items']))  {
                    unset($res[$i]['items']);
                }
            }
        }
        return array_values($res);
    }

}
