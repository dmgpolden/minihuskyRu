<?php

namespace frontend\modules\post\models;

use Yii;
use yii\base\Model;
use yii\sphinx\Query;

class Search extends Model
{
    public $q = '';

    public function attributeLabels()
    {
        return [
            'q' => \Yii::t('app', 'Do Search') . '...'
        ];
    }

    public function rules()
    {
        return [
            ['q', 'safe'],
            //['q', 'string', 'min' => 3, 'skipOnEmpty' => false],
        ];
    }
    
    public function searchPostsByDescription()
    {
//        vd();
       $index = Yii::$app->sphinx->getIndexSchema('{{%post}}')->name;
        $result = (new Query())
            ->select('`id`,`content`')
            ->from($index)
            ->match($this->q)
            ->andWhere('visible=1')
            ->snippetCallback(function ($rows) {
                $result=['xxx'];
                    if(count($rows)>0){
                        foreach ($rows as $id=>$row) {
                            $result[] = $row['content'];
                        }
                    unset($result[0]);
                    }
                return $result;

            }) 
            ->all();
//            vd($result);
   foreach ($result as $id=>$row){
                unset($result[$id]['content']);
            }
     return yii\helpers\ArrayHelper::index($result, function ($element) {
          return $element['id'];
          });
    }

}

