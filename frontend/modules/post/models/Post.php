<?php

namespace frontend\modules\post\models;

use Yii;
use yii\helpers\ArrayHelper;
use \yii\db\Query;
use \yii\helpers\Html;
use \yii2\web\UploadedFile;
use yii\behaviors\SluggableBehavior;
use common\modules\file\models\ItemFile;
use common\modules\file\models\File;
/**
 * This is the model class for table "post".
 *
 * @property integer $id
 * @property integer $category_id
 * @property integer $tree
 * @property integer $lft
 * @property integer $rgt
 * @property integer $level
 * @property string $title
 * @property string $caption
 * @property string $content
 * @property integer $visible
 * @property integer $sequence
 * @property string $note
 * @property string $slug
 * @property string $title
 * @property string $description
 * @property string $keywords
 * @property integer $go_to_type
 * @property string $external_link
 * @property integer $external_link_type
 * @property integer $removable
 * @property integer $image_id
 * @property integer $present
 * @property integer $box
 * @property integer $textbox
 */
class Post extends \yii\db\ActiveRecord
{
    public $filepath;        
    public $parent_name;
    public $tree;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'post';
    }
    
    public static function tablePostFile()
    {
        return 'post_file';
    }

/**
     * @inheritdoc
     * @return PostQuery
     */
    public static function find()
    {
        return new PostQuery(get_called_class());
    }
   

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category_id', 'visible', 'sequence', 'go_to_type', 'external_link_type', 'removable', 'present', 'box', 'textbox'], 'integer'],
            [[ 'delImage'], 'safe'],
            [['content'], 'string'],
            [['title','create_at'], 'required'],
            [['title', 'caption', 'slug', 'description', 'keywords', 'external_link'], 'string', 'max' => 255],
            [['note'], 'string', 'max' => 200],
            [['create_at'], 'date','format'=>'yyyy-mm-dd'],
            [['image_id'], 'integer'],

        ];
    }
    
    public function getCategory()
        {
            return $this->hasOne(Category::className(), ['id' => 'category_id']);
        }


    public function getImages()
        {
            return $this->hasMany(File::className(), ['id' => 'file_id'])
                ->viaTable('post_file', ['post_id' => 'id']);
        }

    public function getFiles()
        {
            return $this->hasMany(File::className(), ['id' => 'file_id'])
                ->viaTable('post_file', ['post_id' => 'id']);
        }

    public function getImage()
        {
            return $this->hasOne(File::className(), ['id' => 'image_id']);
        }

    public static function present($limit=10)
    {
        $query = self::find()
//          ->select(['CONCAT( "/",'. File::tableName(). '.name,".",ext) as image ',self::tableName().'.*',])
            ->from(self::tableName())
            ->joinWith('image')
            ->joinWith('category')
            ->andWhere([self::tableName().'.present' =>1])
            ->groupBy( self::tableName().'.id')
             ->limit($limit);
         $res = $query->all( );
       return $res;
    }
    public static function box($limit=10)
    {
        $query = self::find()
//          ->select(['CONCAT( "/",'. File::tableName(). '.name,".",ext) as image ',self::tableName().'.*',])
            ->from(self::tableName())
            ->joinWith('image')
            ->joinWith('category')
            ->andWhere([self::tableName().'.box' =>1])
            ->groupBy( self::tableName().'.id')
             ->limit($limit);
        $res = $query->all( );
        $data = self::normalizeItems($res);

       return $data;
    }

    public static function getTextbox($limit=10)
    {
            $cache = Yii::$app->cache;
            $dependency = new \yii\caching\TagDependency(['tags'=>['post']]) ;
            $key = 'Post_Box_'.__FILE__.__LINE__;
            $data =  $cache->get($key);
            if ($data === false) {
               $query = self::find()
                ->from(self::tableName())
                ->where(['textbox' =>1])
                ->limit($limit);
                 $res = $query->all( );
                 $data = self::normalizeItems($res);
            $cache->set($key, $data, 60*3600, $dependency );
            }

            return $data;
    }
    
    public static function lastArticle($limit=10)
    {
        return self::findArticle()
                    ->limit($limit);
    }
    
    public static function lastNews($limit=10)
    {
        return self::findNews()
                    ->limit($limit);
    }

    protected static function normalizeItems($items )
    {
            $res=[];
        foreach ($items as $i => $item) {
            $res[$i]['label'] = $item->title;
                            
                            $res[$i]['url'] =['/post/'.$item->category->slug.'/view', 'id'=> $item->id];
                            
                            if (  $item->image == null ){
                                $res[$i]['image'] ='';
                            }
                            else{
                                $res[$i]['image'] =$item->image->name;
                            }
                            if (  $item->caption == null ){
                                $res[$i]['caption'] ='';
                            }
                            else{
                                $res[$i]['caption'] =$item->caption;
                            }
            if (isset($item['items'])) {
                $res[$i]['items'] = self::normalizeItems($item['items']);
                if (empty($res[$i]['items']))  {
                    unset($res[$i]['items']);
                }
            }
        }
        return array_values($res);
    }
    protected static function normalizeArrayItems($items )
    {
            $res=[];
        foreach ($items as $i => $item) {
            $res[$i]['label'] = $item['label'];
                            if (  $item['slug'] == null ){
                                $res[$i]['url'] =['/page/default/view', 'id'=> $item['id']];
                            }
                            else{
                                $res[$i]['url'] =['/page/default/slugview', 'slug'=> $item['slug']];
                            }
            if (isset($item['items'])) {
                $res[$i]['items'] = self::normalizeArrayItems($item['items']);
                if (empty($res[$i]['items']))  {
                    unset($res[$i]['items']);
                }
            }
        }
        return array_values($res);
    }


    public static function findNews()
    {
        $query = (new Query())
            ->select(['category_id'=>'id'])
            ->from(Category::tableName())
            ->where(['tree'=>1])
            ;
           $cat=$query->createCommand();
           //$cat->fetchMode=[\PDO::FETCH_NUM];
$sel=$cat->queryAll();
//vd($sel,false);
        return Post::find()
                    ->where(['category_id'=>$sel])
                    ->andWhere(['visible'=>1])
                    ->orderBy( 'id DESC' )
                    ;
    }
    
    public static function findArticle()
    {
        $query = (new Query())
            ->select(['category_id'=>'id'])
            ->from(Category::tableName())
            ->where(['tree'=>4])
            ;
        $query = (new Query())
            ->select(['category_id'=>'id'])
            ->from(Category::tableName())
            ->where(['tree'=>4])
            ;
           $cat=$query->createCommand();
           //$cat->fetchMode=[\PDO::FETCH_NUM];
            $sel=$cat->queryAll();
//vd($sel,false);
        return Post::find()
                    ->where(['category_id'=>$sel])
                    ->andWhere(['visible'=>1])
                    ->orderBy( 'id DESC' )
                    ;
    }

    public static function getLast($last=10,$cat=null)
    {
            $cache = Yii::$app->cache;
            $dependency = new \yii\caching\TagDependency(['tags'=>['post']]) ;
            $key = ($cat == null) ? 'post-'.$last : 'post-'.$last.'-'.$cat ;
            $data =  $cache->get($key);
            if ($data === false) {
                $q_cats = (new Query())
                     ->select(['id'])
                     ->from(Category::tableName())
                     ->andWhere(['visible'=>1]);
                if ($cat !== null){
                    $q_cats->andWhere(['slug'=>$cat]);
                }
                $query = (new Query())
                 ->select(['id','title','create_at','caption'])
                 ->from(self::tableName())
                 ->andWhere(['visible'=>1])
                 ->andWhere(['category_id'=>$q_cats])
                 ->orderBy( 'create_at DESC' )
                 ->limit( $last);
                $command=$query->createCommand();
                $command->fetchMode=[\PDO::FETCH_CLASS,__CLASS__];
                $data = $command->queryAll();
            $cache->set($key, $data, 60*3600, $dependency );
            }

        return $data;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'title' =>  Yii::t('post','Title'),
            'caption' => Yii::t('post','Caption'),
            'create_at' => Yii::t('post','Create at'),
            'content' => Yii::t('post','Content'),
            'note' => Yii::t('post','Note'),
            'title' => Yii::t('post','Title'),
        ];
    }
    public function behaviors() {
        return [
                        ['class' => SluggableBehavior::className(),
              'attribute' => 'title',
              'slugAttribute' => 'slug',
                            'ensureUnique' => true
                        ],
    ];

    }

}
