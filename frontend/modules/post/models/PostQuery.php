<?php
namespace frontend\modules\post\models;

use yii\db\ActiveQuery;

class PostQuery extends ActiveQuery
{
    
    public function news()
    {
      $this->addWhere(['root' => 1]);
      $this->joinWith('category');
      return $this;
    }

}
