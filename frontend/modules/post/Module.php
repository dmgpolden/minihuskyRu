<?php

namespace frontend\modules\post;

use yii\base\BootstrapInterface;

class Module extends \yii\base\Module implements BootstrapInterface
{
    public $controllerNamespace = 'frontend\modules\post\controllers';

    public function init()
    {
        // custom initialization code goes here
        parent::init();
    }

    public function bootstrap($app)
    {
            $cache = \Yii::$app->cache;
            $dependency = new \yii\caching\TagDependency(['tags'=>['post']]) ;
            $key = 'post-category'.__FILE__.__LINE__;
            $categories =  $cache->get($key);
            if ($categories === false) {
                $categories=\common\modules\post\models\Category::find()->where(['lft'=>1])->all();
            $cache->set($key, $categories, 60*3600, $dependency );
            }
        foreach($categories as $category){
            $app->get('i18n')->translations[$category->slug.'*'] = [
                'class'    => 'yii\i18n\PhpMessageSource',
                'basePath' => __DIR__ . '/messages',
            ];
            
        //vd($category->slug,false);
        $this->controllerMap[$category->slug]='frontend\modules\post\controllers\DefaultController';
        $app->getUrlManager()->addRules([
            $category->slug.'/<id:\d+>'=>$this->id. '/'. $category->slug . '/view',
            $category->slug.'/<category:[\w\-_]+\d?>'=>$this->id. '/'. $category->slug . '/index',
            $category->slug=>$this->id. '/'. $category->slug . '/index',
        ], false);

        }
    }

}
