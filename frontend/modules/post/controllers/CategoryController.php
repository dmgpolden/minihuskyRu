<?php

namespace common\modules\post\controllers;

use Yii;
use common\modules\post\models\Category;
use common\modules\post\models\CategorySearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CategoryController implements the CRUD actions for Category model.
 */
class CategoryController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }
    public function actionTest()
    {
/*$root = new Category;
$root->title = 'Mobile Phones';
$root->saveNode();
$root = new Category;
$root->title = 'Cars';
$root->saveNode();

$category1 = new Category;
$category1->title = 'Ford';
$category2 = new Category;
$category2->title = 'Mercedes';
$category3 = new Category;
$category3->title = 'Audi';
*/
$root = Category::findOne(77);
//$root = Category::find()->where(['id'=>82])->all();
//vd($root);
$c1 = Category::findOne(82);
//$c2 = Category::findOne(85);
$c1->moveAsFirst($root);
//$c2->insertAfter($c1);
//vd(Category::findFull()->all());
    
            return $this->redirect(['index']);
    
    }


    /**
     * Lists all Category models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CategorySearch;
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    /**
     * Displays a single Category model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionCreate()
    {
        $model = new Category;

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
        	if ($model->parent == 0){
        		$model->saveNode();
        	} elseif ($model->parent){
        		$root = Category::findOne($model->parent);
        		$model->appendTo($root);
        	}
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Category model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findFullModel($id);
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->saveNode();
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }
    /**
     * Updates an existing Category model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdateparent()
    {   
        $post=Yii::$app->request->post();
        $model = $this->findFullModel((int)$post['pk']);
        $oldparent= $model->parent;
        $model->parent=(int)$post['value'];
            if($model->parent==0 && $oldparent!== $model->parent &&  $model->lft!=1 ){
                $model->moveAsRoot();
            }else
                if ($model->parent != $model->id){
                    $root = Category::findOne($model->parent);
        		    $model->moveAsFirst($root);
        	        }
             else
                throw new NotFoundHttpException('self to self 2');
    
    }

    /**
     * Updates an existing Category model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUp($id)
    {
        $model = $this->findModel($id);
        if ($prev=$model->prev()->one()){
        $model->moveBefore($prev);
        }

            return $this->redirect(['index']);
    }
    /**
     * Updates an existing Category model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDown($id)
    {
        $model = $this->findModel($id);
        if ($next=$model->next()->one()){
            $model->moveAfter($next);
        }

        return $this->redirect(['index']);
    }

    /**
     * Deletes an existing Category model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->deleteNode();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Category model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Category the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if ($id !== null && ($model = Category::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    protected function findFullModel($id)
    {
        if ($id !== null && ($model = Category::findFullOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
