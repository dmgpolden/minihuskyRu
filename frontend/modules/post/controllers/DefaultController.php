<?php

namespace frontend\modules\post\controllers;

use Yii;
use frontend\modules\post\models\Post;
use frontend\modules\post\models\Category;
use frontend\modules\file\models\File;
use frontend\modules\post\models\PostSearch;
use frontend\modules\post\models\Search;
use yii\data\Pagination;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use frontend\modules\file\Upload;
use frontend\modules\file\models\ItemFile;
use yii\filters\AccessControl;
use yii\web\Response;

/**
 * PostController implements the CRUD actions for Post model.
 */
class DefaultController extends Controller
{
        /*
    public function behaviors()
    {
            ['access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index','view'],
                        'allow' => true,
                        'roles' => ['*'],
                    ],
                ],
            ],
        ];
    }
        */

    /**
     * Lists all Post models.
     * @return mixed
     */
    public function actionIndex()
    {
        //vd(Yii::$app->request->get());
        $searchModel = new PostSearch();
        $category=Category::find()->where(['slug'=>$this->id])->one();
        $dataProvider = $searchModel->search(array_merge(Yii::$app->request->queryParams,['PostSearch'=>['category_id'=>$category->id]]));

            $view='@frontend/modules/post/views/'.$category->slug.'/index';
            $themeView=\Yii::getAlias('@theme').'/modules/post/views/'.$category->slug.'/index';
//            vd(\Yii::getAlias('@theme').'/modules/post/views/'.$category->slug.'/index.php');
        if((!file_exists(\Yii::getAlias($view).'.php')) && (!file_exists($themeView.'.php'))){
            $view = '@frontend/modules/post/views/default/index';
        }

        return $this->render($view, [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'category' => $category,
        ]);
    }

    /**
     * Displays a single Post model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    protected function findModel($id)
    {
        if (($model = Post::find()->andWhere(['id' => $id, 'visible' => 1])->limit(1)->one())  !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    public function actionSearch()
    {
    /*    if (!Yii::$app->request->isAjax) {
            throw new ForbiddenHttpException();
        }
      */  
        
        $model = new Search();
        $model->load(Yii::$app->request->get());
//        vd($model);
        $cacheKey = 'PostSearchIds: ' . $model->q;
        $ids = Yii::$app->cache->get($cacheKey);
        if ($ids === false) {
            $ids = $model->searchPostsByDescription();      
            Yii::$app->cache->set(
                $cacheKey,
                $ids,
                //86400
                1
                /*    ,
                new TagDependency(
                    [
                        'tags' => ActiveRecordHelper::getCommonTag(Page::className()),
                    ]
                )
                */
            );
        }
        $posts = new Pagination(
            [
                'defaultPageSize' => 50,
                'forcePageParam' => false,
                'totalCount' => count($ids),
            ]
        );
        $cacheKey .= ' : ' . $posts->offset;
        $postlist = Yii::$app->cache->get($cacheKey);
            if ($postlist === false) {
            $postlist = Post::find()->where(
                [
                    'in',
                    '`id`',
                    array_slice(
yii\helpers\ArrayHelper::getColumn($ids,'id'),
                        $posts->offset,
                        $posts->limit
                    )
                ]
            )
            //            ->addOrderBy('sort_order')
            ->all();
            Yii::$app->cache->set(
                $cacheKey,
                $postlist,
                //86400
                1
                /*new TagDependency(
                    [
                        'tags' => ActiveRecordHelper::getCommonTag(Page::className()),
                    ]
                )*/
            );
        }

       Yii::$app->response->format = Response::FORMAT_JSON;
        return [
            'view' => $this->renderPartial(
                'search',
                [
                    'model' => $model,
                    'postlist' => $postlist,
                    'posts' => $posts,
                    'snippet'=>$ids,
                ]
            ),
            'totalCount' => count($ids),
        ];
    }

}
