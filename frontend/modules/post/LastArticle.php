<?php

namespace frontend\modules\post;

use yii\base\Widget;
use frontend\modules\post\models\Post;

class LastArticle extends Widget
{
    public $limit;
    public $last;

    public function init()
    {
        parent::init();
        if ($this->limit === null) {
            $this->limit = 10;
        }
        $this->last=Post::getLast($this->limit,'article');
    }

    public function run()
    {
			return $this->render( 'lastarticle',
				[
					'last'=>$this->last
				]
			
			);
		}
}

