<?php

namespace frontend\modules\post;

use yii\base\Widget;
use frontend\modules\post\models\Post;

class Box extends Widget
{
    public $limit;
		public $box;

    public function init()
    {
        parent::init();
        if ($this->limit === null) {
            $this->limit = 10;
        }
				$this->box=Post::box($this->limit);
    }

    public function run()
    {
			return $this->render( 'box',
				[
					'box'=>$this->box
				]
			
			);
		}
}

