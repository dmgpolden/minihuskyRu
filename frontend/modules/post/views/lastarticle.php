<?php

use yii\helpers\Html;
use yii\helpers\Url;
?>

<div class="article">							
					<?=Yii::t('post','Last articles');?>
					<? foreach ( $last as $n ) : ?>
				<div class='date'>
<?=\Yii::$app->formatter->asDate( $n->create_at, 'long') ?>
				</div>
				<div class='name'>
				<?=Html::a( $n->title, Url::to( ['/post/article/view','id'=>$n->id] ) );?>
				</div>
				<div class='caption'>
				<?=$n->caption;?>
				</div>

					<? endforeach ;?>
				<?=Html::a( Yii::t('post','All article'),  Url::to( ['/post/article/index'] ) );?>
</div> 
