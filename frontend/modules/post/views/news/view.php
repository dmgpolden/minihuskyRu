<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use frontend\modules\post\Module;
use kartik\datecontrol\DateControl;

/* @var $this yii\web\View */
/* @var $model frontend\modules\news\models\Shop */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('news','News'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
vd($this);
?>
<div class="news-view">

    <h1><?= Html::encode($this->title) ?></h1>
<div>
<?=  Yii::$app->formatter->asDate( $model->create_at, Yii::$app->modules['datecontrol']['displaySettings']['date']) ?>
</div>
<div>
<?= $model->content ?>
</div>


</div>
