<?php

use yii\helpers\Html;
use yii\helpers\Url;
use frontend\modules\news\Module;
?>

<div class="news">							
					<?=Yii::t('news','Last news');?>
					<? foreach ( $lastnews as $n ) : ?>
				<div class='date'>
<?=\Yii::$app->formatter->asDate( $n->create_at, 'long') ?>
				</div>
				<div class='name'>
				<?=Html::a( $n->title, Url::to( ['/post/news/view','id'=>$n->id] ) );?>
				</div>
				<div class='caption'>
				<?=$n->caption;?>
				</div>

					<? endforeach ;?>
				<?=Html::a( Yii::t('news','All news'),  Url::to( ['/post/news/index'] ) );?>
</div> 
