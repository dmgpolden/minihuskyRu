<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\modules\news\models\News */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('news', 'News'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="news-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'datetime',
            'caption',
            'content:ntext',
            'visible',
            'sequence',
            'note',
            'slug',
            'title',
            'description',
            'keywords',
            'go_to_type',
            'external_link',
            'external_link_type',
            'removable',
            'image',
            'present',
            'box',
            'textbox',
        ],
    ]) ?>

</div>
