<?php
    /**
     * @var \yii\web\View $this
     * @var \yii\data\Pagination $posts
     */
?>
        <?php //vd($snippet);?>
<?php if (count($postlist) > 0): ?>
    <div class="tab-pane  active" id="blockView">
        <ul>
            <?php foreach($postlist as $post): ?>
                <li><a href="<?= \yii\helpers\Url::to(['/post/'.$post->category->slug.'/view', 'id' => $post->id]) ?>"><?= $post->title; ?></a></li>
              <div>из раздела <?= $post->category->title;?></div>
              <div><?= $snippet[$post->id]['snippet'];?></div>
                <a href="<?= \yii\helpers\Url::to(['/post/'.$post->category->slug.'/view', 'id' => $post->id]) ?>"><?= Yii::t('app','more')?></a>
            <?php endforeach; ?>
        </ul>
    </div>
    <?php if ($posts->pageCount > 1): ?>
        <div class="pagination">
            <?=
            \app\widgets\Linkpostr::widget(
                [
                    'firstpostLabel' => '&laquo;&laquo;',
                    'lastpostLabel' => '&raquo;&raquo;',
                    'pagination' => $posts,
                ]
            );
            ?>
        </div>
    <?php endif; ?>
<?php else: ?>
    <p class="no-results"><?= Yii::t('app', 'No results found') ?></p>
<?php endif; ?>

