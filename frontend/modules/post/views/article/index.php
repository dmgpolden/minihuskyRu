<?php

use yii\helpers\Html;
use yii\widgets\ListView;
use frontend\modules\post\Module;

/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\post\models\ShopSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title = Yii::t('post','Articles');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="post-index">
    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>


    <?= ListView::widget([
        'dataProvider' => $dataProvider,
        'itemOptions' => ['class' => 'item'],
        'itemView' => '_index',
				/*        'itemView' => function ($model, $key, $index, $widget) {
            return Html::a(Html::encode($model->title), ['view', 'id' => $model->id]);
        }
				,
				*/
    ]) ?>

</div>
