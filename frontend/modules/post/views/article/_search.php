<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\modules\news\models\NewsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="news-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'title') ?>

    <?= $form->field($model, 'create_at') ?>

    <?= $form->field($model, 'caption') ?>

    <?= $form->field($model, 'category_id') ?>

    <?php // echo $form->field($model, 'visible') ?>

    <?php // echo $form->field($model, 'sequence') ?>

    <?php // echo $form->field($model, 'note') ?>

    <?php // echo $form->field($model, 'slug') ?>

    <?php // echo $form->field($model, 'title') ?>

    <?php // echo $form->field($model, 'description') ?>

    <?php // echo $form->field($model, 'keywords') ?>

    <?php // echo $form->field($model, 'go_to_type') ?>

    <?php // echo $form->field($model, 'external_link') ?>

    <?php // echo $form->field($model, 'external_link_type') ?>

    <?php // echo $form->field($model, 'removable') ?>

    <?php // echo $form->field($model, 'image') ?>

    <?php // echo $form->field($model, 'present') ?>

    <?php // echo $form->field($model, 'box') ?>

    <?php // echo $form->field($model, 'textbox') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
