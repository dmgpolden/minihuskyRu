<?php

use yii\helpers\Html;
use yii\helpers\Url;
use frontend\modules\page\Module;
?>

<div class="box">							
					<?=Module::t('Last page');?>
					<? foreach ( $box as $n ) : ?>
				<?=Html::img( '@static/tmb'. $n->image );?>
				<div class='name'>
				<?=Html::a( $n->name, Url::to( ['/page/default/view','id'=>$n->id] ) );?>
				</div>
				<div class='caption'>
				<?=$n->caption;?>
				</div>
					<? endforeach ;?>
</div> 
