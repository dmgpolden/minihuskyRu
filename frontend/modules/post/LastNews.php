<?php

namespace frontend\modules\post;

use yii\base\Widget;
use frontend\modules\post\models\Post;

class LastNews extends Widget
{
    public $limit;
    public $last;

    public function init()
    {
        parent::init();
        if ($this->limit === null) {
            $this->limit = 10;
        }
        $this->last=Post::getLast($this->limit,'news');
    }

    public function run()
    {
			return $this->render( 'lastnews',
				[
					'lastnews'=>$this->last
				]
			
			);
		}
}

