<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use frontend\modules\page\models\Page;

/* @var $this yii\web\View */
/* @var $model common\modules\page\models\Page */
//vd ( Page::getBreadcrumbs( $model->id)); //
//$root = $model->parent()->one();


$this->params['breadcrumbs'][] = $model->name;
$this->params['breadcrumbs']= array_merge( Page::getBreadcrumbs( $model->lft), $this->params['breadcrumbs']);
?>

<div class="page-view" dgh>
<?= \Yii::t( 'page', 'Page');?>
   
<h1><?= Html::encode($model->title) ?></h1>

<?= Html::encode($model->caption) ?>
<?= Html::encode($model->content) ?>

</div>
<? foreach( $model->getImages() as $im ) { ?>
<?		$items[] = 
    [
        'url' => 'http://static.test.net.pol-den.ru/big'. $im['filepath'],
        'src' => 'http://static.test.net.pol-den.ru/tmb'. $im['filepath'],
        'options' => array('title' =>  @$model['title'])
    ];
} ?>

<?=( is_array( @$items) ) ?  dosamigos\gallery\Gallery::widget(['items' => $items]) : '' ;?>

