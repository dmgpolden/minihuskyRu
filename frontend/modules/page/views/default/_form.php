<?php

use yii\helpers\Html;
use yii\helpers\Url;
//use yii\bootstrap\ActiveForm;
use kartik\widgets\ActiveForm;
use common\extensions\fileapi\FileAPI;
use frontend\modules\page\models\Page;
/* @var $this yii\web\View */
/* @var $model common\modules\page\models\Page */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="page-form">

    <?php

		 $form = ActiveForm::begin([
									//'layout' => 'horizontal',
								'options' => ['enctype'=>'multipart/form-data'],
//      'type' => ActiveForm::TYPE_INLINE,
					      'fieldConfig' => ['autoPlaceholder'=>true],
								
								]); 
								
								//vd ( Yii::$app->i18n->translations);
								?>

    <?= $form->field($model, 'root')->dropDownList(Page::getList( )) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => 255]) ?>
    <?// $form->field($model, 'slug')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'caption')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'content')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'note')->textInput(['maxlength' => 200]) ?>

    <?= $form->field($model, 'visible')->checkbox() ?>

    <div class="form-group">
    <?= $form->field($model, 'present')->checkbox() ?>
    <?= $form->field($model, 'box')->checkbox() ?>
    <?= $form->field($model, 'textbox')->checkbox() ?>
    </div>

		<?=Yii::t( 'page','Tags')?>
    <?= $form->field($model, 'title')->textInput(['maxlength' => 255]) ?>
    <?= $form->field($model, 'description')->textInput(['maxlength' => 255]) ?>
    <?= $form->field($model, 'keywords')->textInput(['maxlength' => 255]) ?>

    <?php //= $form->field($model, 'go_to_type')->textInput() ?>

    <?php //= $form->field($model, 'external_link')->textInput(['maxlength' => 255]) ?>

    <?php //= $form->field($model, 'external_link_type')->textInput() ?>

    <?php //= $form->field($model, 'removable')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

<?= $form->field( $model, 'image[]')->fileInput( ['multiple' => ''])->label( '') ?>
<? foreach( $model->getImages() as $im ) { 
        echo Html::img( 'http://static.test/tmb'. $im['filepath'] ) ;

	echo Html::a( 'del','#',
		['id'=>$im['id'],
		'class'=>'delete',
		]);

} 
?>
    <?php ActiveForm::end(); ?>

</div>
<?    $this->registerJs( "jQuery( '.delete').click(function(){
			$.ajax({
      url: '" . Url::toRoute( '/image/default/delete') . "',
      global: false,
      type: 'POST',
      data: ({id : this.getAttribute('id')}),
      dataType: 'html',
      success: function(msg){
         alert(msg);
      }
			})
			return false;
			})
			;");

