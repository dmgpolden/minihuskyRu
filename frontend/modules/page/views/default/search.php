<?php
    /**
     * @var \yii\web\View $this
     * @var \app\models\Page[] $pagelist
     * @var \yii\data\Pagination $pages
     */
?>
        <?php //vd($snippet);?>
<?php if (count($pagelist) > 0): ?>
    <div class="tab-pane  active" id="blockView">
        <ul>
            <?php foreach($pagelist as $page): ?>
                <li><a href="<?= \yii\helpers\Url::to(['/page/default/slugview', 'slug' => $page->slug]) ?>"><?= $page->title; ?></a></li>
              <div><?= $snippet[$page->id]['snippet'];?></div>
                <a href="<?= \yii\helpers\Url::to(['/page/default/slugview', 'slug' => $page->slug]) ?>"><?= Yii::t('app','more')?></a>
            <?php endforeach; ?>
        </ul>
    </div>
    <?php if ($pages->pageCount > 1): ?>
        <div class="pagination">
            <?=
            \app\widgets\LinkPager::widget(
                [
                    'firstPageLabel' => '&laquo;&laquo;',
                    'lastPageLabel' => '&raquo;&raquo;',
                    'pagination' => $pages,
                ]
            );
            ?>
        </div>
    <?php endif; ?>
<?php else: ?>
    <p class="no-results"><?= Yii::t('app', 'No results found') ?></p>
<?php endif; ?>
