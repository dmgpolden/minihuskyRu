<?php

use yii\helpers\Html;
use yii\helpers\Url;
use frontend\modules\page\Module;
?>

<div class="page">							
					<?=Module::t('Last page');?>
					<? foreach ( $textbox as $n ) : ?>
				<div class='name'>
				<?=Html::a( $n->name, Url::to( ['/page/default/view','id'=>$n->id] ) );?>
				</div>
				<div class='caption'>
				<?=$n->caption;?>
				</div>

					<? endforeach ;?>
				<?=Html::a( Module::t('All page'),  Url::to( ['/page/default/index'] ) );?>
</div> 
