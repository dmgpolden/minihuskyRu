<?php

namespace frontend\modules\page;

use Yii;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'frontend\modules\page\controllers';

    public function init()
    {
        parent::init();
				 $this->registerTranslations( );
    }
		
		public function registerTranslations()
    {
        Yii::$app->i18n->translations['page'] = [
            'class' => 'yii\i18n\PhpMessageSource',
            'basePath' => '@frontend/modules/page/messages',
        ];
    
		}

    public static function t( $message, $params = [], $language = null)
    {
        return Yii::t('page', $message, $params, $language);
    }
}
