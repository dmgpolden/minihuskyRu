<?php

namespace frontend\modules\page;

use yii\base\Widget;
use frontend\modules\page\models\Page;

class LastPage extends Widget
{
    public $limit;
		public $lastpage;

    public function init()
    {
        parent::init();
        if ($this->limit === null) {
            $this->limit = 10;
        }
				$this->lastpage=Page::getLast($this->limit);
    }

    public function run()
    {
			return $this->render( 'lastpage',
				[
					'lastpage'=>$this->lastpage
				]
			
			);
		}
}

