<?php

namespace frontend\modules\page\models;

use Yii;
use yii\base\Model;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\data\ActiveDataProvider;
use frontend\modules\page\models\Page;

/**
 * PageSearch represents the model behind the search form about `frontend\modules\page\models\Page`.
 */
class PageSearch extends Page
{
    public $q = '';
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [[ 'visible', 'sequence', 'go_to_type', 'external_link_type', 'removable', 'image', 'present', 'box', 'textbox'], 'integer'],
            [['title', 'caption', 'content', 'note', 'slug', 'title', 'description', 'keywords', 'external_link'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Page::find();
        
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

vd($this->load($params),false);
vd($this->validate(),false);
//vd($this->getErrors());
        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }
        
        $query->andWhere(['<>','level',1]);
        
        $query->andFilterWhere([
            'id' => $this->id,
            'visible' => $this->visible,
            'sequence' => $this->sequence,
            'go_to_type' => $this->go_to_type,
            'external_link_type' => $this->external_link_type,
            'removable' => $this->removable,
            'image' => $this->image,
            'present' => $this->present,
            'box' => $this->box,
            'textbox' => $this->textbox,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'caption', $this->caption])
            ->andFilterWhere(['like', 'content', $this->content])
            ->andFilterWhere(['like', 'note', $this->note])
            ->andFilterWhere(['like', 'slug', $this->slug])
            ->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'keywords', $this->keywords])
            ->andFilterWhere(['like', 'external_link', $this->external_link]);

        return $dataProvider;
    }
}
