<?php

namespace frontend\modules\page\models;

use Yii;
use creocoder\nestedsets\NestedSetsBehavior;
use yii\helpers\ArrayHelper;
use \yii\db\Query;
use \yii\helpers\Html;
use common\modules\file\models\ItemFile;
use common\modules\file\models\File;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
;

/**
 * This is the model class for table "page".
 *
 * @property integer $id
 * @property integer $tree
 * @property integer $lft
 * @property integer $rgt
 * @property integer $depth
 * @property string $title
 * @property string $caption
 * @property string $content
 * @property integer $visible
 * @property integer $sequence
 * @property string $note
 * @property string $slug
 * @property string $title
 * @property string $description
 * @property string $keywords
 * @property integer $go_to_type
 * @property string $external_link
 * @property integer $external_link_type
 * @property integer $removable
 * @property integer $image_id
 * @property integer $present
 * @property integer $box
 * @property integer $textbox
 */
class Page extends \common\modules\page\models\Page
{

    public $file;
    public $label;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'page';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
        ];
    }
    
    function afterSave($insert, $changedAttributes)
    {
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'title' => Yii::t('page','title page'),
            'caption' => Yii::t('page','Caption'),
            'content' => Yii::t('page','Content'),
            'visible' => Yii::t('page','Visible'),
            'sequence' => Yii::t('page','Sequence'),
            'note' => Yii::t('page','Note'),
            'slug' => Yii::t('page','Slug'),
            'title' => Yii::t('page','Title'),
            'description' => Yii::t('page','Description'),
            'keywords' => Yii::t('page','Keywords'),
            'go_to_type' => Yii::t('page','Go To Type'),
            'external_link' => Yii::t('page','External Link'),
            'external_link_type' => Yii::t('page','External Link Type'),
            'removable' => Yii::t('page','Removable'),
            'image' => Yii::t('page','Image'),
            'present' => Yii::t('page','Present'),
            'box' => Yii::t('page','Box'),
            'textbox' => Yii::t('page','Textbox'),
        ];
    }
        
    public function getImage()
        {
            return $this->hasOne(File::className(), ['id' => 'image_id']);
        }
    
    public static function present($limit=10)
    {
        $query = self::find()
//          ->select(['CONCAT( "/",'. File::tableName(). '.name,".",ext) as image ',self::tableName().'.*',])
            ->from(self::tableName())
            ->joinWith('image')
//            ->joinWith('category')
            ->andWhere([self::tableName().'.present' =>1])
            ->groupBy( self::tableName().'.id')
             ->limit($limit);
         $res = $query->all( );
       return $res;
    }
    public static function box($limit=10)
    {
            $cache = Yii::$app->cache;
            $dependency = new \yii\caching\TagDependency(['tags'=>['page']]) ;
            $key = 'page-box'.__FILE__.__LINE__;
            $data =  $cache->get($key);
            if ($data === false) {
                $query = self::find()
                    ->from(self::tableName())
                    ->joinWith('image')
                    ->andWhere([self::tableName().'.box' =>1])
                    ->groupBy( self::tableName().'.id')
                     ->limit($limit);
                 $res = $query->all( );
                 $data = self::normalizeItems($res);
            $cache->set($key, $data, 60*3600, $dependency );
            }

            return $data;
    }
        public static function getTextbox($limit=10)
        {
            $cache = Yii::$app->cache;
            $dependency = new \yii\caching\TagDependency(['tags'=>['page']]) ;
            $key = 'page-box'.__FILE__.__LINE__;
            $data =  $cache->get($key);
            if ($data === false) {
               $query = self::find()
                ->from(self::tableName())
                ->where(['textbox' =>1])
                ->limit($limit);
                 $res = $query->all( );
                 $data = self::normalizeItems($res);
            $cache->set($key, $data, 60*3600, $dependency );
            }

            return $data;
        }

        public static function getLast($last=10)
        {
                $query = (new Query())
                 ->select(['*'])
                 ->from(self::tableName())
                 ->where(['<>','tree',1])
                 ->orderBy( 'id DESC' )
                 ->limit( $last);
                $command=$query->createCommand();
                $command->fetchMode=[\PDO::FETCH_CLASS,__CLASS__];
                $res = $command->queryAll( );
            return $res;
        }
    
    public static function tablePageFile()
    {
        return 'page_file';
    }

        public function childrenWithImage()
        {
            $cache = Yii::$app->cache;
            $dependency = new \yii\caching\TagDependency(['tags'=>['page']]) ;
            $key = 'page-childrenWithImage'.$this->id;
            $data =  $cache->get($key);
            if ($data === false) {
                $query = (new Query())
                ->select([self::tableName().'.title',self::tableName().'.slug','CONCAT(name,".",ext) as file'])
                ->from(self::tableName())
                ->andWhere(['>','lft',$this->lft])
                ->andWhere(['<','rgt',$this->rgt])
                ->andWhere(['<>',self::tableName().'.id',1])
                ->andWhere(['depth'=>$this->depth+1])
                ->andWhere(['tree'=>$this->tree])
                ->andWhere(['visible'=>1])
                    ->leftJoin(File::tableName(),File::tableName().'.id=image_id')
    //            ->andWhere(['item_type'=>ItemFile::ITEM_MENU])
                ->groupBy(self::tableName().'.id')
                ->orderBy( 'lft' );
                $command=$query->createCommand();
                $command->fetchMode=[\PDO::FETCH_CLASS,__CLASS__];
            $data = $command->queryAll( );
            $cache->set($key, $data, 60*3600, $dependency );
            }

            return $data;
                
        }


        public static function getOption( )
        {    
            $items = self::getAll();
        return  $items;
        }
        public static function getList( )
        {    
        $res=array( );
            $items = (new Query())
            ->select(['id','title','depth'])
            ->from(self::tableName())
            ->orderBy( 'lft' )
            ->all();
                foreach ( $items as $key=>$item ){
                    $res[$item['id']] = str_repeat('-', $item['depth']-1) . $item['title'];
                }
            return $res;
            //return ArrayHelper::map($items,'id', 'name');
        }

        public static function findSlug($slug ){
                $query = (new Query())
                ->select(['*'])
                ->from(Page::tableName())
                ->where( ['slug' => $slug]);
//                ->all();
 $command=$query->createCommand();
    $command->fetchMode=[\PDO::FETCH_CLASS,__CLASS__];
      $res = $command->queryAll( );
//        vd( $res);
        return  $res        ;
        }

        public static function getAll( )
        {    
            $rows = (new Query())
            ->select(['*'])
            ->from(self::tableName())
            ->andWhere( 'id > 1')
            ->andWhere( 'visible = 1')
            ->orderBy( 'lft' )
            ->all();
        return $rows;
        }
        
        public function getPps()
        {
            $query = (new Query())
            ->select(['*'])
            ->from(self::tableName())
            ->andWhere(['<','lft',$this->id])
            ->andWhere(['>','rgt',$this->id])
            ->andWhere(['<>','id',1])
            ->andWhere(['<>','parent_id','null'])
            ->orderBy( 'lft' );
//            ->all();
 $command=$query->createCommand();
//    $command->fetchMode=[\PDO::FETCH_CLASS,__CLASS__];
      $res = $command->queryAll( );
        //vd( $res);
        return $res;
        }

       
        public function getParents()
        {
            $id=$this->id;
            $query = (new Query())
            ->select(['*'])
            ->from(self::tableName())
            ->andWhere(['<','lft',$id])
            ->andWhere(['>','rgt',$id])
            ->andWhere(['=','tree',$this->tree])
            ->orderBy( 'lft' );
//            ->all();
 $command=$query->createCommand();
//    $command->fetchMode=[\PDO::FETCH_CLASS,__CLASS__];
      $res = $command->queryAll( );
        //vd( $res);
        return $res;
    }

        public function getBreadcrumbs()
        {
            $cache = Yii::$app->cache;
            $dependency = new \yii\caching\TagDependency(['tags'=>['page']]) ;
            $key = 'page-Breadcrumbs-'.__FILE__.__LINE__.$this->id;
            $data =  $cache->get($key);
            if ($data === false) {
                $data = self::normalizeArrayItems(
                $query = (new Query())
                ->select(['label'=>'title','slug'])
                ->from(self::tableName())
                ->andWhere(['<','lft',$this->lft])
                ->andWhere(['>','rgt',$this->rgt])
                ->andWhere(['=','tree',$this->tree])
                ->andWhere(['>','depth',1])
                ->orderBy( 'lft' )
                    ->all());

            $cache->set($key, $data, 60*3600, $dependency );
            }

            return $data;
        }

        public static function getPage()
        {    
            $items =  self::getAll();
                $depth = 0;
                $res='';
                foreach ($items as $n => $page)
                { 
                    if ($page['depth'] == $depth) {
                        $res.= Html::endTag('li') . "\n";
                    } elseif ($page['depth'] > $depth) {
                        $res.= Html::beginTag('ul') . "\n";
                    } else {
                        $res.= Html::endTag('li') . "\n";

                        for ($i = $depth - $page['depth']; $i; $i--) {
                            $res.= Html::endTag('ul') . "\n";
                            $res.= Html::endTag('li') . "\n";
                        }
                    }
                    $res.= Html::beginTag('li');
                    $res.= Html::a( Html::encode($page['title']) , ['/page/'.$page['slug'] ]  ) ; 
                    $depth = $page['depth'];
                }
                for ($i = $depth; $i; $i--) {
                    $res.= Html::endTag('li') . "\n";
                    $res.= Html::endTag('ul') . "\n";
                }
        return $res;

        }

        public static function getMenu($depth=1)
        {
            $cache = Yii::$app->cache;
            $dependency = new \yii\caching\TagDependency(['tags'=>['page']]) ;
            $key = 'page-mainLeftMenu';
            $data =  $cache->get($key);
            if ($data === false) {
                $data = self::normalizeArrayItems(self::getAsArray(['depth'=>$depth+1]));
            $cache->set($key, $data, 60*3600, $dependency );
            }

            return $data;
        }

        public static function mainLeftMenu()
        {
            $cache = Yii::$app->cache;
            $dependency = new \yii\caching\TagDependency(['tags'=>['page']]) ;
            $key = 'page-mainLeftMenu';
            $data =  $cache->get($key);
            if ($data === false) {
                $cat=self::find()
                    ->select(['id'])
                    ->andWhere(['=','slug','mainleft'])
                   ;
                $data = self::normalizeArrayItems(self::getAsArray(['tree'=>$cat]));
            $cache->set($key, $data, 60*3600, $dependency );
            }

            return $data;
        }


        public static function mainRightMenu()
        {
            $cache = Yii::$app->cache;
            $dependency = new \yii\caching\TagDependency(['tags'=>['page']]) ;
            $key = 'page-mainRightMenu';
            $data =  $cache->get($key);
            if ($data === false) {
                $cat=self::find()
                    ->select(['id'])
                    ->andWhere(['=','slug','mainright'])
                   ;
                $data = self::normalizeArrayItems(self::getAsArray(['tree'=>$cat]));
            $cache->set($key, $data, 60*3600, $dependency );
            }
            
            return $data;
        }


        public static function getMainPage()
        {
                $res = self::normalizeItems(self::getAsArray()); 
                return $res;
        }

    protected static function normalizeItems($items )
    {
            $res=[];
        foreach ($items as $i => $item) {
            $res[$i]['label'] = $item->title;
                            if (  $item->slug == null ){
                                $res[$i]['url'] =['/page/default/view', 'id'=> $item->id];
                            }
                            else{
                                $res[$i]['url'] =['/page/default/slugview', 'slug'=> $item->slug];
                            }
                            if (  $item->image == null ){
                                $res[$i]['image'] ='';
                            }
                            else{
                                $res[$i]['image'] =$item->image->name;
                            }
                            if (  $item->note != null ){
                                $res[$i]['caption'] =$item->note;
                            }
                            else{
                                $res[$i]['caption'] =$item->caption;
                            }
            if (isset($item['items'])) {
                $res[$i]['items'] = self::normalizeItems($item['items']);
                if (empty($res[$i]['items']))  {
                    unset($res[$i]['items']);
                }
            }
        }
        return array_values($res);
    }

    protected static function normalizeArrayItems($items )
    {
            $res=[];
        foreach ($items as $i => $item) {
            $res[$i]['label'] = $item['label'];
                            if (  $item['slug'] == null ){
                                $res[$i]['url'] =['/page/default/view', 'id'=> $item['id']];
                            }
                            else{
                                $res[$i]['url'] =['/page/default/slugview', 'slug'=> $item['slug']];
                            }
            if (isset($item['items'])) {
                $res[$i]['items'] = self::normalizeArrayItems($item['items']);
                if (empty($res[$i]['items']))  {
                    unset($res[$i]['items']);
                }
            }
        }
        //vd($res);
        return array_values($res);
    }

public static function getAsArray($condition=false)
{
            $query = (new Query())
            ->select(['*'])
            ->from(self::tableName())
            ->andWhere( 'visible = 1')
            ->andWhere( 'lft>1')
            ->andWhere(['<>','slug','main'])
            ->orderBy( 'lft' );
    if ($condition)
        $query->andWhere($condition);
            $nodes = $query->all();

static $tree = null;
 
    $result = array();
    $depth = 0;
    $stack = array();
 
    foreach ($nodes as $node) {
        $item = $node;
        $item['items'] = array();
 
        $depth = count($stack);
 
        while($depth > 0 && $stack[$depth - 1]['depth'] >= $item['depth']) {
            array_pop($stack);
            $depth--;
        }
                $item['label']=$item['title'];
                $item['url']=['page/view/'.( $item['slug']==null ? $item['slug'] : $item['id'])];
        if ($depth == 0) {
            $i = count($result);
            $result[$i] = $item;
            $stack[] = & $result[$i];
        } else {
            $i = count($stack[$depth - 1]['items']);
            $stack[$depth - 1]['items'][$i] = $item;
            $stack[] = & $stack[$depth - 1]['items'][$i];
        }
    }
 
    $tree = $result;
 
    return $tree;
}
public static function getTree()
{
   static $tree = null;
 
    $nodes = self::getAll();
 
    $result = array();
    $depth = 0;
    $stack = array();
 
    foreach ($nodes as $node) {
        $item = $node;
        $item['children'] = array();
 
        $depth = count($stack);
 
        while($depth > 0 && $stack[$depth - 1]['depth'] >= $item['depth']) {
            array_pop($stack);
            $depth--;
        }
 
        if ($depth == 0) {
            $i = count($result);
            $result[$i] = $item;
            $stack[] = & $result[$i];
        } else {
            $i = count($stack[$depth - 1]['children']);
            $stack[$depth - 1]['children'][$i] = $item;
            $stack[] = & $stack[$depth - 1]['children'][$i];
        }
    }
 
    $tree = $result;
 
    return $tree;
}
     /**
     * Returns the enire tree in a nested array
     * Every "node" in this array is an array which has two key/value combinations:
     * <ul>
     *  <li>'node': The actual node object (like this one)</li>
     *  <li>'children': A list of children of the node. Every child is again an array with these to key/value combinations.</li>
     * </ul>
     * @param $returntreenode  Whether the rood node should be included in de result
     */
    public static function getNestedTree($returntreenode = true, $keyfield = null)
    {
        if($keyfield == null)
        {
            $keyfield = 'id';
        }
        // Fetch the flat tree
        //$rawtree = Page::find()->select('id,tree,lft,rgt,depth,title,slug')->all();
        $rawtree = (new Query())
    ->select('id,tree,lft,rgt,depth,title,slug')
    ->from(self::tableName())
    ->where('tree is not null')
        ->orderBy( 'lft' )
    ->all();

        // Init variables needed for the array conversion
        $tree = array();
        $node =& $tree;
        $depth = 0;
        $position = array();
        $lastitem = '';

        foreach($rawtree as $rawitem) 
        {      
            // If its a deeper item, then make it subitems of the current item
            if ($rawitem['depth'] > $depth) 
            {
                $position[] =& $node; //$lastitem;
                $iposition[] =& $items; //$lastitem;
                $depth = $rawitem['depth'];
                $node =& $node[$lastitem]['children'];
                    $items =& $items[$lastitem];
            }
            // If its less deep item, then return to a depth up
            else
            {
                while ($rawitem['depth'] < $depth) 
                {
                    
                    end($position);
                    end($iposition);
                    $node =& $position[key($position)];
                    $items =& $iposition[key($iposition)];
                    array_pop($position);
                    array_pop($iposition);
                    $depth = $node[key($node)]['node']['depth'];
                }
            }

            // Add the item to the final array
            $node[$rawitem[$keyfield]]['node'] = $rawitem;
            $items[$rawitem[$keyfield]] = ['label'=>$rawitem['title'],'url'=>$rawitem['slug'] ] ;
                    
            // save the last items' title
            $lastitem = $rawitem[$keyfield];
        }

        // we don't care about the tree node
        if (!$returntreenode) 
        {
            reset($tree);
            $tree = $tree[key($tree)]['children'];
            //array_shift($tree);
        }


        return $items;
//        return $tree;
    }
 
  public function behaviors()
  {
      return [
			[
				'class' => NestedSetsBehavior::className(),
			],
         [
             'class' => SluggableBehavior::className(),
              'attribute' => 'title',
              'slugAttribute' => 'slug',
          ],
      ];
  }

}
