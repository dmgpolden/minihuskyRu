<?php
            
namespace frontend\modules\page\models;

use Yii;

/**
 * This is the model class for index "k_pageidx".
 *
 * @property integer $id
 * @property string $title
 * @property string $caption
 * @property string $content
 * @property string $create_at
 */
class PageIdx extends \yii\sphinx\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function indexName()
    {
        //return 'k_pageidx';
        return '{{%pageidx}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id'], 'unique'],
            [['id'], 'integer'],
            [['title', 'caption', 'content', 'create_at'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('page', 'ID'),
            'title' => Yii::t('page', 'Title'),
            'caption' => Yii::t('page', 'Caption'),
            'content' => Yii::t('page', 'Content'),
            'create_at' => Yii::t('page', 'Create At'),
        ];
    }
  public function getSnippetSource()
    {
      return $this->content;

    }
}
