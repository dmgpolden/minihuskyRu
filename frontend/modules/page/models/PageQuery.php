<?php

namespace frontend\modules\page\models;

use Yii;
use creocoder\behaviors\NestedSetQuery;

class PageQuery extends \yii\db\ActiveQuery
{
    public function behaviors() {
        return [
            [
                'class' => NestedSetQuery::className(),
            ],
        ];
    }
}

