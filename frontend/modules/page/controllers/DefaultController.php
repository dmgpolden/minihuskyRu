<?php

namespace frontend\modules\page\controllers;

use Yii;
use frontend\modules\file\Upload;
use frontend\modules\file\models\ItemFile;
use frontend\modules\page\models\Page;
use frontend\modules\page\models\PageIdx;
use frontend\modules\page\models\PageTree;
use frontend\modules\page\models\PageSearch;
use frontend\modules\page\models\Search;
use yii\data\Pagination;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\ForbiddenHttpException;
use yii\web\UploadedFile;
use yii\filters\VerbFilter;
use yii\caching\TagDependency;
use yii\web\Response;
use yii\data\ActiveDataProvider;
use yii\sphinx\Query;


/**
 * PageController implements the CRUD actions for Page model.
 */
class DefaultController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['shop'],
                ],
            ],
        ];
    }
    /**
     * Lists all Page models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PageSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
    //                    'pages' => $pages ,
        ]);
    }

        /**
     * Displays a First Page model.
     * @param integer $id
     * @return mixed
     */
    public function actionViewindex()
    {
            $this->layout='/main-index';
        return $this->render('view', [
            'model' => $this->findModel(1),
        //                'breadcrumbs' => 
        ]);
    }

    /**
     * Displays a single Page model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        //                'breadcrumbs' => 
        ]);
    }
    
        public function actionSlugview($slug)
    {
        $view = Yii::getAlias('@theme/modules/page/views/default/'.$slug);
        if (is_file($view.'.php')) {
            $view=$slug;
        } else {
            $view = 'view';
        }
        return $this->render($view, [
            'model' => $this->findModelSlug($slug),
        ]);
    }
        
        /**
     * Creates a new Page model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */

        public function actionCreate()
        {
        $model = new PageTree();
        if ($model->load(Yii::$app->request->post())) {
                 $model->image = UploadedFile::getInstances( $model, 'image');
                             if ( $model->validate( ) ) {
                                    $images=$model->image;
                        unset( $model->image);
                        $root = PageTree::findOne( $model->root);
                            $model->appendTo($root) ;
                                            $this->Process($model->id,$images );
                            }
                            else {
                                        return $this->render('create', [
                                 'model' => $model,
                               ]);
                            }

            return $this->redirect(['view', 'id' => $model->id]);
                        
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Page model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
                
        $model = $this->findModel($id);
        $modelTree = $this->findModelTree($id);

                                            vd( $model);
        if ($model->load(Yii::$app->request->post()) ) {
                // подключить модуль для файлов, если нет - ругаться {}, обработка модулем с файлами
                 $model->image = UploadedFile::getInstances( $model, 'image');
                             if ( $model->validate( ) ) {
                                    $images=$model->image;
                        unset( $model->image);
                                if(   $model->getDirtyAttributes(['root']) ){
                                        $root = PageTree::findOne( $model->root);
                                        $modelTree->moveAsFirst($root) ;
                                    }
                                    $model->save();
                                    $this->Process($model->id,$images );
            return $this->redirect(['view', 'id' => $model->id]);
                        }
                 } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }
        public function Process($model_id,$images)
        {
                            foreach( $images as $im ) {
                                            $file = Upload::upload( $im );
                                            vd( $file->errors,false);
                                        Upload::FiletoItem($file->id, ItemFile::ITEM_MENU, $model_id);
                            }
//                        vd( $model);
            }

        public function actionTest()
        {
                $Page1 = new Page;
                $Page1->name = 'Ford';
                $Page2 = new Page;
                $Page2->name = 'Mercedes';
                $Page3 = new Page;
                $Page3->name = 'Audi';
                $root = Page::findOne(3);
//                vd( $root);
                $Page1->appendTo($root);
                $Page2->insertAfter($Page1);
                $Page3->insertBefore($Page1);
        }

    /**
     * Deletes an existing Page model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionSearch()
    {
    /*    if (!Yii::$app->request->isAjax) {
            throw new ForbiddenHttpException();
        }
      */  
        
        $model = new Search();
        $model->load(Yii::$app->request->get());
//        vd($model);
        $cacheKey = 'PageSearchIds: ' . $model->q;
        $ids = Yii::$app->cache->get($cacheKey);
        if ($ids === false) {
            $ids = $model->searchPagesByDescription();      
            Yii::$app->cache->set(
                $cacheKey,
                $ids,
                //86400
                1
                /*    ,
                new TagDependency(
                    [
                        'tags' => ActiveRecordHelper::getCommonTag(Page::className()),
                    ]
                )
                */
            );
        }
        $pages = new Pagination(
            [
                'defaultPageSize' => 50,
                'forcePageParam' => false,
                'totalCount' => count($ids),
            ]
        );
        $cacheKey .= ' : ' . $pages->offset;
        $pagelist = Yii::$app->cache->get($cacheKey);
            if ($pagelist === false) {
            $pagelist = Page::find()->where(
                [
                    'in',
                    '`id`',
                    array_slice(
yii\helpers\ArrayHelper::getColumn($ids,'id'),
                        $pages->offset,
                        $pages->limit
                    )
                ]
            )
            //            ->addOrderBy('sort_order')
            ->all();
            Yii::$app->cache->set(
                $cacheKey,
                $pagelist,
                //86400
                1
                /*new TagDependency(
                    [
                        'tags' => ActiveRecordHelper::getCommonTag(Page::className()),
                    ]
                )*/
            );
        }

       Yii::$app->response->format = Response::FORMAT_JSON;
        return [
            'view' => $this->renderPartial(
                'search',
                [
                    'model' => $model,
                    'pagelist' => $pagelist,
                    'pages' => $pages,
                    'snippet'=>$ids,
                ]
            ),
            'totalCount' => count($ids),
        ];
    }


    /**
     * Finds the Page model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Page the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Page::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    protected function findModelSlug($slug)
    {
        if (($model = Page::find()->where( ['slug'=>$slug])->one()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page slug does not exist.');
        }
    }
    protected function findModelTree($id)
    {
        if (($modelTree = Page::findOne($id)) !== null) {
            return $modelTree;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
