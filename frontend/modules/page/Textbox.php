<?php

namespace frontend\modules\page;

use yii\base\Widget;
use frontend\modules\page\models\Page;

class Textbox extends Widget
{
    public $limit;
		public $textbox;

    public function init()
    {
        parent::init();
        if ($this->limit === null) {
            $this->limit = 10;
        }
				$this->textbox=Page::getTextbox($this->limit);
    }

    public function run()
    {
			return $this->render( 'textbox',
				[
					'textbox'=>$this->textbox
				]
			
			);
		}
}

