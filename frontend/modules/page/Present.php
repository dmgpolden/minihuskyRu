<?php

namespace frontend\modules\page;

use yii\base\Widget;
use frontend\modules\page\models\Page;
use yii\helpers\Html;
use yii\helpers\Url;

class Present extends Widget
{
    public $limit;
		public $present;

    public function init()
    {
        parent::init();
        if ($this->limit === null) {
            $this->limit = 10;
        }
				$this->present=Page::getPresent($this->limit);
    }

    public function run()
    {
			if ( $this->present )
			{
				foreach(  $this->present as $p) 
				{	
				$items[]=
						['content'=>Html::a( Html::img( '@sl'.$p->image), $p->slug ) ,
						'caption'=> Html::a( $p->caption , $p->slug )] ;
				}

			return $this->render( 'present',
				[
					'items'=>$items
				]
				);
			}
		}
}

