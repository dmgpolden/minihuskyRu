<?php

namespace frontend\modules\page;

use yii\base\Widget;
use frontend\modules\page\models\Page;

class Box extends Widget
{
    public $limit;
		public $box;

    public function init()
    {
        parent::init();
        if ($this->limit === null) {
            $this->limit = 10;
        }
				$this->box=Page::getBox($this->limit);
    }

    public function run()
    {
			return $this->render( 'box',
				[
					'box'=>$this->box
				]
			
			);
		}
}

