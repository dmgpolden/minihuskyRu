<?php
namespace frontend\controllers;

use Yii;
use frontend\models\LoginForm;
use app\models\Search;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use frontend\modules\page\models\Page;
use yii\sphinx\Query;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
                'transparent' => true, 
            ],
            'auth' => [
                'class' => 'yii\authclient\AuthAction',
                'successCallback' => [$this, 'successCallback'],
            ],
        ];
    }

    public function actionSearch()
    {
        $model = new Search();
        $model->q=Yii::$app->request->get('q');
        if(!$model->validate()){
                vd($model->getErrors(),false);
        }
            return $this->render(
                'search',
                [
                    'model' => $model,
                ]
            );
    }

    public function actionAutoCompleteSearch($term)
    {
        $model = new Search();
        $model->q=Yii::$app->request->get('term');
        Yii::$app->response->format = Response::FORMAT_JSON;
        if($model->validate()){
       $index = Yii::$app->sphinx->getIndexSchema('{{%post}}')->name;
        $result = (new Query())
            ->select('content')
            ->from($index)
            ->match($model->q)
            ->andWhere('visible=1')
            ->snippetCallback(function ($rows) {
                $result=['xxx'];
                    if(count($rows)>0){
                        foreach ($rows as $id=>$row) {
                            $result[] = $row['content'];
                        }
                    unset($result[0]);
                    }
                return $result;

            }) 
            ;
        return $result->limit(5)->all();
        }
    }

    public function actionIndex()
    {
			$this->layout='main-index';
            $cache = Yii::$app->cache;
            $dependency = new \yii\caching\TagDependency(['tags'=>['page']]) ;
            $key = 'page-main'.__FILE__.__LINE__;
            $page =  $cache->get($key);
            if ($page === false) {
            $page=Page::find()->where(['slug'=>'main'])->limit(1)->one();
            $cache->set($key, $page, 60*3600, $dependency );
            }
        return $this->render('index',['main'=>$page]);
    }

    public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return
            Yii::$app->getResponse()->redirect(Yii::$app->getUser()->getReturnUrl($defaultUrl));
        } else {
          return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->testEmail(Yii::$app->params['testEmail']);
            if ($model->sendEmail(Yii::$app->params['adminEmail'])) {
                $thanksMessage = "Спасибо за обращение!
                Мы постараемся ответить вам как можно скорее, но получаем большое количество писем, по этому, если вам нужен ответ срочно, то обратитесь к нам в Viber/WhatsApp по телефону +7-926-847-18-97";
                Yii::$app->session->setFlash('success', $thanksMessage );
            } else {
                Yii::$app->session->setFlash('error',Yii::t('app','There was a mistake, try later' ));
            }
            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }

    public function actionAbout()
    {
        return $this->render('about');
    }

    public function actionSignup()
    {
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                if (Yii::$app->getUser()->login($user)) {
                    return $this->goHome();
                }
            }
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->getSession()->setFlash('success', 'Check your email for further instructions.');

                return $this->goHome();
            } else {
                Yii::$app->getSession()->setFlash('error', 'Sorry, we are unable to reset password for email provided.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->getSession()->setFlash('success', 'New password was saved.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }

    public function successCallback($client)
    {
        $attributes = $client->getUserAttributes();
        // user login or signup comes here
    }


}
