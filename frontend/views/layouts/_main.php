<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\widgets\Alert;
use frontend\modules\page\models\Page;
use frontend\modules\shop\models\Shop;

/* @var $this \yii\web\View */
/* @var $content string */
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>

    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
    <?php $this->beginBody() ?>
      
       <?php
            NavBar::begin([
                'brandLabel' => 'ПИТОМНИК',
                'brandUrl' => Yii::$app->homeUrl,
                'options' => [
                    'class' => 'navbar navbar-fixed-top navbar-inverse ' 
                ],
            ]);
            $pageItems = [							
                ['label' => Yii::t('app','Home'), 'url' => ['/site/index']],
                ['label' => Yii::t('app','About'), 'url' => ['/site/about']],
                ['label' => Yii::t('app','Shop'), 'url' => ['/shop/default/index']],
								];
                //['label' => 'Contact', 'url' => ['/site/contact']],
            $pageItems= array_merge($pageItems,  Page::getPageArray());

					echo Nav::widget([
                'options' => ['class' => 'navbar-nav '],
                'items' => $pageItems,
            ]);
            NavBar::end();
        ?>
    
<div class="container">
        <?= Alert::widget() ?>
        <?= $content ?>


</div> <? // container ?>
				
    <footer class="footer raw"  style="text-align: center;">
  <p style="margin: 20px">	© 2014&nbsp;
 сайт фотографа Екатерины Соколовой'
 Веб-дизайн и техническая поддержка <a href="http://pol-den.ru">pol-den.ru</a>
 </p>
    </footer>

    <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
