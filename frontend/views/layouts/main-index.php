<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use frontend\widgets\Alert;
use frontend\widgets\Present;
use frontend\widgets\Box;
use frontend\widgets\TextBox;
use frontend\modules\post\LastNews; 
use frontend\modules\post\LastArticle; 

/* @var $this \yii\web\View */
/* @var $content string */

//AppAsset::register($this);
?>
<?php $this->beginContent( '@theme/views/layouts/_main.php'); ?>

<div class="row">
           
        <div class="col-lg-8" id="content">

<?= Present::widget(); ?>

       <?= $content ?>
<?= Box::widget(); ?>
<?= Textbox::widget(); ?>
</div>

    <div class="col-lg-4" id="sidebarRight">
		<?= LastNews::widget(); ?>
    </div>
        
</div>

<?php $this->endContent( ); 
