<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
//use frontend\assets\AppAsset;
use frontend\widgets\Alert;
use frontend\modules\page\models\Page;
use frontend\modules\shop\LastShop;

/* @var $this \yii\web\View */
/* @var $content string */

//AppAsset::register($this);
?>
<?php $this->beginContent( '@theme/views/layouts/_main.php'); ?>
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
      
        <div class="page col-lg-9">
        <?= $content ?>
        </div>

        <div class="sidebar col-lg-3">
		<?= LastShop::widget(); ?>
        </div> <?//sidebar ?>
<?php $this->endContent( ); 
