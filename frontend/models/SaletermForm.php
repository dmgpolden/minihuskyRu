<?php
namespace frontend\models;

use Yii;
use yii\base\Model;

class SaletermForm extends Model
{
    public $username;
    public $email;
    public $kennel;
    public $website;
    public $number;
    public $comment;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['username', 'email', 'kennel' , 'number' , 'comment'  ], 'required'],
            [['email'], 'email'],
        ];
    }
    
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
//            'verifyCode' => 'Verification Code',
            'username'     =>Yii::t('app', 'Name'												),
            'email'        =>Yii::t('app', 'Your email address'											),
            'kennel'       =>Yii::t('app', 'Name of your AKK kennel'					),
            'website'      =>Yii::t('app', 'Website URL of your AKK kennel'							),
            'number'       =>Yii::t('app', 'Number of AKK dogs in your kennel'					),
            'comment'      =>Yii::t('app', 'Comment'),
        ];
    }
    
    public function attrType($attr)
    {
        return 'textInput';
    }

    public function getOption($attr,$val)
    {
        $f=$attr.'Options';
        $m=$this->$f();
      return $m[$val];
    }

    /**
     * Sends an email to the specified email address using the information collected by this model.
     *
     * @param  string  $email the target email address
     * @return boolean whether the email was sent
     */
    public function sendEmail($email)
    {
        return Yii::$app->mailer->compose('saleterm',['body'=>$this])
            ->setTo(Yii::$app->params['adminEmail'])
            ->setFrom(Yii::$app->params['robotEmail'])
            ->setReplyTo([$this->email=>$this->username])
            ->setSubject('Анкета с сайта ' . Yii::$app->request->hostInfo )
            ->send();
    }

}


