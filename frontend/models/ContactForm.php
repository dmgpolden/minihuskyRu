<?php

namespace frontend\models;

use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class ContactForm extends Model
{
    public $name;
    public $email;
    public $subject;
    public $body;
    public $verifyCode;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // name, email, subject and body are required
            [['name', 'email', 'subject', 'body'], 'required'],
            [['subject', 'body'], 'match', 'pattern' => '#(https?://[^\s]+)#', 'not' => 'true'],
            [['subject', 'body'], 'match', 'pattern' => '#[A-Z0-9.-]+\.[A-Z]{2,}#', 'not' => 'true'],
            // email has to be a valid email address
            ['email', 'email'],
            // verifyCode needs to be entered correctly
            ['verifyCode', 'captcha'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'verifyCode' => Yii::t('app','Verification code'),
            'name'=> Yii::t('app','Your name'),
            'email'=> Yii::t('app','Your e-mail'),
            'subject'=> Yii::t('app','Subject'),
            'body'=> Yii::t('app','Message'),
        ];

    }

    /**
     * Sends an email to the specified email address using the information collected by this model.
     *
     * @param  string  $email the target email address
     * @return boolean whether the email was sent
     */
    public function sendEmail($email)
    {
        return Yii::$app->mailer->compose()
            ->setTo(Yii::$app->params['adminEmail'])
//            ->setTo(Yii::$app->params['testEmail'])
            ->setFrom(Yii::$app->params['robotEmail'])
            ->setReplyTo([$this->email=>$this->name])
            ->setSubject($this->subject)
            ->setTextBody(
            "Имя: " . $this->name ."\n" .
            "E-mail: " . $this->email ."\n" .
            "Тема: " . $this->subject ."\n" .
            "Сообщение: " . $this->body ."\n" 
                        )
            ->send();
    }
    public function testEmail($email)
    {
        return Yii::$app->mailer->compose()
            ->setTo(Yii::$app->params['testEmail'])
            ->setFrom(Yii::$app->params['robotEmail'])
            ->setReplyTo([$this->email=>$this->name])
            ->setSubject($this->subject)
            ->setTextBody(
            "Имя: " . $this->name ."\n" .
            "E-mail: " . $this->email ."\n" .
            "Тема: " . $this->subject ."\n" .
            "Сообщение: " . $this->body ."\n" 
                        )
            ->send();
    }
}
