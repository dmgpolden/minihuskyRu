<?php
namespace frontend\models;

use Yii;
use yii\base\Model;

class AnketaForm extends Model
{
    public $username;
    public $old;
    public $country;
    public $city;
    public $tel;
    public $email;
    public $whereinfo;
    public $whenbuy;
    public $wasdog;
    public $havepet;
    public $havechildren;
    public $olddog;
    public $sexdog;
    public $colordog;
    public $eyedog;
    public $typedog;
    public $typefor;
    public $body;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['username', 'old', 'country', 'city', 'tel', 'email', 'whereinfo', 'whenbuy'], 'required'],
            [['wasdog','havepet', 'havechildren', 'olddog', 'sexdog', 'colordog', 'eyedog', 'typedog', 'typefor'], 'required'],
            [['old'], 'integer'],
            [['email'], 'email'],
            [['body'], 'safe'],
        ];
    }
    
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
//            'verifyCode' => 'Verification Code',
            'username'      =>Yii::t('app', 'Name'												),
            'old'           =>Yii::t('app', 'Age'												),
            'country'       =>Yii::t('app', 'Country of residence'                              ),
            'city'          =>Yii::t('app', 'City of residence'									),
            'tel'           =>Yii::t('app', 'Phone'											),
            'email'         =>Yii::t('app', 'E-mail'											),
            'whereinfo'     =>Yii::t('app', 'How did you hear about the kennel Song of Ice?'	),
            'whenbuy'       =>Yii::t('app', 'When would you like to get a AKK?'					),
            'wasdog'        =>Yii::t('app', 'Have you previously had a dog?'						),
            'havepet'       =>Yii::t('app', 'Do you own any pets now?'				),
            'havechildren'  =>Yii::t('app', 'Do you have children?'								),
            'olddog'        =>Yii::t('app', 'What age of the AKK do you want to buy?'				),
            'sexdog'        =>Yii::t('app', 'What gender of the AKK would you like to buy?'					),
            'colordog'      =>Yii::t('app', 'What color do you prefer?'							),
            'eyedog'        =>Yii::t('app', 'What color eyes do you prefer?'					),
            'typedog'       =>Yii::t('app', 'AKK of what size would you like to buy?'						),
            'typefor'       =>Yii::t('app', 'What type of AKK you looking for?'					),
            'body'          =>Yii::t('app','Please list any requests or questions you might have that have not been previously mentioned'),
        ];
    }
    
    public function attrType($attr)
    {
        return 'textInput';
    }

    public function getOption($attr,$val)
    {
        $f=$attr.'Options';
        $m=$this->$f();
      return $m[$val];
    }
    public function eyedogOptions()
    {
        return [
            1=>Yii::t('app','No preference'),
            2=>Yii::t('app','Both eyes are brown'                    ),
            3=>Yii::t('app','One eye is brown, the other one is blue'),
            4=>Yii::t('app','Both eyes are blue'                     ),
            5=>Yii::t('app','Mosaic within one or both eyes'         ),
        ];
    }
    
    public function typeforOptions()
    {
        return [
            1=>yii::t('app','Pet (companion)'    ),
            2=>yii::t('app','Show or breeding'  ),
        ];
    }
    
    public function typedogOptions()
    {
        return [
            1=>yii::t('app','No preference'),
            2=>yii::t('app','Standart'),
            3=>yii::t('app','Mini'    ),
            4=>yii::t('app','Toy'     ),
        ];
    }

    public function colordogOptions()
    {
        return [
            1=>Yii::t('app','No preference'),
            2=>Yii::t('app','Dark grey (wolf) with a white'),
            3=>Yii::t('app','Black and white'              ),
            4=>Yii::t('app','Red-white'                    ),
            5=>Yii::t('app','Brown-white'                  ),
            6=>Yii::t('app','Gray with white'              ),
            7=>Yii::t('app','White'                        ),
            ];
    }
    
    public function sexdogOptions()
    {
        return [
            1=>Yii::t('app','Male'  ),
            2=>Yii::t('app','Female'),
            0=>Yii::t('app','No preference'),
        ];
    }

    public function olddogOptions()
    {
        return [
            1=>Yii::t('app','2-4 months puppy'               ),
            2=>Yii::t('app','5-12 grown-up puppy'         ),
            3=>Yii::t('app','1-3 years young dog'            ),
            4=>Yii::t('app','Adult dog older than 3 y.o.'),
            0=>Yii::t('app','No preference'),
        ];
    }

    /**
     * Sends an email to the specified email address using the information collected by this model.
     *
     * @param  string  $email the target email address
     * @return boolean whether the email was sent
     */
    public function sendEmail($email)
    {
        return Yii::$app->mailer->compose('anketa',['body'=>$this])
            ->setTo(Yii::$app->params['adminEmail'])
            ->setFrom(Yii::$app->params['robotEmail'])
            ->setReplyTo([$this->email=>$this->username])
            ->setSubject('Анкета с сайта ' . Yii::$app->request->hostInfo )
            ->send();
    }

}

