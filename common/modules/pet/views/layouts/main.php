<?php
use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
    <?php $this->beginBody() ?>
    <div class="wrap">
        <?php
            NavBar::begin([
                'brandLabel' => 'My Company',
                'brandUrl' => Yii::$app->homeUrl,
                'options' => [
                    'class' => 'navbar-inverse navbar-fixed-top',
                ],
            ]);
            $pageItems = [
                ['label' => 'Home', 'url' => ['/site/index']],
                ['label' => Yii::t('pet', 'Pets'), 'url' => ['/pet/pet/index']],
            ];
            if (Yii::$app->user->isGuest) {
                $pageItems[] = ['label' => 'Login', 'url' => ['/user/security/login']];
            } else {
                $pageItems[] = [
                    'label' => 'Logout (' . Yii::$app->user->identity->username . ')',
                    'url' => ['/site/logout'],
                    'linkOptions' => ['data-method' => 'shop']
                ];
            }
            echo Nav::widget([
                'options' => ['class' => 'navbar-nav navbar-right'],
                'items' => $pageItems,
            ]);
            NavBar::end();
        ?>

        <div class="container">
        <?= Breadcrumbs::widget([
						'homeLink'=>['label'=>Yii::t( 'pet', 'Pets' ) ,'url'=>['/pet/pet/index']],
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
<?
//            vd( $this->params['breadcrumbs']); 
$pages = [
'pet',
'breed',
'category',
'standart',
'registrator',
'action',
'event',
];
foreach ($pages as $m) {
$items[]=
	['label'=>$m, 'url'=>[$m.'/index' ] ] ;
}
?>
<div class="row">
    <div class="col-lg-9">
        <?= $content ?>
    </div>
    <div class="col-lg-3">
        <div id="manager-page" class="list-group">
            <?php
            echo \yii\widgets\Page::widget( ['items'=> $items]);
						?>
        </div>
    </div>
</div>


    </div>

    <footer class="footer">
        <div class="container">
        <p class="pull-left">&copy; My Company <?= date('Y') ?></p>
        <p class="pull-right"><?= Yii::powered() ?></p>
        </div>
    </footer>

    <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
