<?php

use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $content string */

$controller = $this->context;
$pages = $controller->module->pages;
$route = $controller->route;
foreach ($pages as $i => $page) {
    $pages[$i]['active'] = strpos($route, trim($page['url'][0], '/')) === 0;
}
$this->params['nav-items'] = $pages;
?>
<?php $this->beginContent($controller->module->mainLayout) ?>
<div class="row">
    <div class="col-lg-9">
        <?= $content ?>
    </div>
    <div class="col-lg-3">
        <div id="manager-page" class="list-group">
            <?php
            foreach ($pages as $page) {
                $label = Html::tag('span', Html::encode($page['label']), []);
                $active = $page['active'] ? ' active' : '';
                echo Html::a($label, $page['url'], [
                    'class' => 'list-group-item' . $active,
                ]);
            }
            ?>
        </div>
    </div>
</div>
<?php $this->endContent(); ?>
