<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\modules\pet\models\Action */

$this->title = Yii::t('pet', 'Create {modelClass}', [
    'modelClass' => 'Action',
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('pet', 'Actions'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="action-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
