<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\modules\pet\models\Registrator */

$this->title = Yii::t('pet', 'Create {modelClass}', [
    'modelClass' => 'Registrator',
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('pet', 'Registrators'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="registrator-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
