<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\modules\pet\models\Registrator */

$this->title = Yii::t('pet', 'Update {modelClass}: ', [
    'modelClass' => 'Registrator',
]) . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('pet', 'Registrators'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('pet', 'Update');
?>
<div class="registrator-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
