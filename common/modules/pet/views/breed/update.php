<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\modules\pet\models\Breed */

$this->title = Yii::t('pet', 'Update {modelClass}: ', [
    'modelClass' => 'Breed',
]) . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('pet', 'Breeds'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('pet', 'Update');
?>
<div class="breed-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
