<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\modules\pet\models\Breed */

$this->title = Yii::t('pet', 'Create {modelClass}', [
    'modelClass' => 'Breed',
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('pet', 'Breeds'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="breed-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
