<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\modules\pet\models\Breed;
use common\modules\pet\models\Category;

/* @var $this yii\web\View */
/* @var $searchModel common\modules\pet\models\PetSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('pet', 'Pets');
$this->params['breadcrumbs'][] = $this->title;
//vd( Yii::$app->request->get('PetSearch')['breed_id']);
?>
<div class="pet-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('pet', 'Create {modelClass}', [
    'modelClass' => 'Pet',
]), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'name',
            'fullname',
            'birthday',
             ['attribute'=>'breed_id',
				'filter'=>Html::DropDownList( 'PetSearch[breed_id]', Yii::$app->request->get('PetSearch')['breed_id'], Breed::getList()),
							'value'=> 'breed.name',
						 ],
             ['attribute'=>'category_id',
				'filter'=>Html::DropDownList( 'PetSearch[category_id]', Yii::$app->request->get('PetSearch')['category_id'], Category::getList()),
							'value'=> 'category.name',
						 ],
             ['attribute'=>'standart_id',
							'value'=> 'standart.name',
						 ],
						 

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
