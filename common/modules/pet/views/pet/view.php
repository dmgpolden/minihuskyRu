<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\modules\pet\models\Pet */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('pet', 'Pets'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pet-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('pet', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('pet', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('pet', 'Are you sure you want to delete this item?'),
                'method' => 'shop',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'fullname',
            'birthday',
            'weight',
            'stature',
						[	'attribute' => 'Breed',
							'value' => $model->breed->name,],
            'breed.name',
						[	'attribute' => 'category',
							'value' => $model->category->name,],
						[	'attribute' => 'standart',
							'value' => $model->standart!==null ? $model->standart->name : '' ,],
						[	'attribute' => 'registrator',
							'value' => $model->registrator->name,],
            'reg_num',
            'tatoo',
            'chip',

        ],
    ]) ?>

</div>
