<?php

use yii\helpers\Html;
use yii\helpers\Url;
//use yii\widgets\ActiveForm;
use yii\bootstrap\ActiveForm;

use yii\widgets\Pjax;
//use yii\jui\DatePicker;

/* @var $this yii\web\View */
/* @var $model common\modules\pet\models\Pet */
/* @var $form yii\widgets\ActiveForm */
//$this->registerAssetBundle( 'yii\web\JqueryAsset');
?>
<? $js = '$("#pet-breed_id").on( "change", function(){
var breed_id = $( this).val( );
	$.ajax({
                url: "' . Url::to(['/pet/standart/list']) . '",
								data: {breed_id: breed_id},
                type: "shop",
                success: function(list) {
									if ( list){
										$( ".field-pet-standart_id").css( "display", "block");
										$( "#pet-standart_id").html( list);
									}
									else {
										$( ".field-pet-standart_id").css( "display", "none");									
									}
								}
			});
 }
);' 
; 
 $this->registerJs( $js, \yii\web\View::POS_READY); ?>

<div class="pet-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'fullname')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'date')->textInput() ?>

    <?= $form->field($model, 'chip')->textInput() ?>
    <?= $form->field($model, 'tatoo')->textInput() ?>

    <?= $form->field($model, 'weight')->textInput(['maxlength' => 10]) ?>

    <?= $form->field($model, 'stature')->textInput(['maxlength' => 10]) ?>

<?= $form->field($model, 'registrator_id')->dropDownList(common\modules\pet\models\Registrator::getList() ) ?>
    <?= $form->field($model, 'reg_num')->textInput() ?>

    <?= $form->field($model, 'breed_id')->dropDownList(common\modules\pet\models\Breed::getList() ) ?>

    <?//= $form->field($model, 'category_id')->textInput() ?>
    <?= $form->field($model, 'category_id')->dropDownList(common\modules\pet\models\Category::getList( )
		) ?>
		<? $display=$model->standart_id==null ? 'none':''; ?>
		<?= $form->field($model, 'standart_id',['options'=>['style'=>'display:'.$display]])->dropDownList(['promt'=>'']) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('pet', 'Create') : Yii::t('pet', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
