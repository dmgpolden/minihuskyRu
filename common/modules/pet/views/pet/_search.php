<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\modules\pet\models\PetSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pet-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'name') ?>

    <?= $form->field($model, 'fullname') ?>

    <?= $form->field($model, 'birthday') ?>

    <?//= $form->field($model, 'dieday') ?>

    <?php // echo $form->field($model, 'weight') ?>

    <?php // echo $form->field($model, 'stature') ?>

    <?php // echo $form->field($model, 'breed_id') ?>

    <?php // echo $form->field($model, 'category_id') ?>

    <?php // echo $form->field($model, 'standart_id') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('pet', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('pet', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
