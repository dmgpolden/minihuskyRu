<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use kartik\datecontrol\DateControl;

/* @var $this yii\web\View */
/* @var $model common\modules\pet\models\Event */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="event-form">

    <?php $form = ActiveForm::begin(['layout' => 'horizontal']); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'desc')->textarea(['rows' => 6]) ?>

    <?//= $form->field($model, 'text')->textarea(['rows' => 6]) ?>

    <?//= $form->field($model, 'count')->textInput() ?>
		<?= $form->field($model, 'datetime')->widget(DateControl::classname(), [
'type'=>DateControl::FORMAT_DATETIME
]);		
		?>

    <?= $form->field($model, 'action_id')->dropDownList(common\modules\pet\models\Action::getList() ) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('pet', 'Create') : Yii::t('pet', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
