<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\modules\pet\models\Standart */

$this->title = Yii::t('pet', 'Create {modelClass}', [
    'modelClass' => 'Standart',
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('pet', 'Standarts'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="standart-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
