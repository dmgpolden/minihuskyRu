<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\modules\pet\models\Standart */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="standart-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'desc')->textarea(['rows' => 6]) ?>

    <?//= $form->field($model, 'breed_id')->dropDownList($model->getTypes()) ?>
    <?= $form->field($model, 'breed_id')->dropDownList(common\modules\pet\models\Breed::getList( )) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('pet', 'Create') : Yii::t('pet', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
