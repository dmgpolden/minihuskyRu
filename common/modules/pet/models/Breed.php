<?php

namespace common\modules\pet\models;

use Yii;
use yii\helpers\ArrayHelper;
/**
 * This is the model class for table "breed".
 *
 * @property integer $id
 * @property string $name
 *
 * @property Pet[] $pets
 * @property Standart[] $standarts
 */
class Breed extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%breed}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('pet', 'ID'),
            'name' => Yii::t('pet', 'Name'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPets()
    {
        return $this->hasMany(Pet::className(), ['breed_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStandarts()
    {
        return $this->hasMany(Standart::className(), ['breed_id' => 'id']);
    }

		public static function getList( )
		{	
	
		$rows = (new \yii\db\Query())
    ->select('id, name')
    ->from(self::tableName())
    ->all();
		
		return ArrayHelper::map($rows,'id', 'name');
		}

}
