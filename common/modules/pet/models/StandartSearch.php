<?php

namespace common\modules\pet\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\modules\pet\models\Standart;

/**
 * StandartSearch represents the model behind the search form about `common\modules\pet\models\Standart`.
 */
class StandartSearch extends Standart
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'breed_id'], 'integer'],
            [['name', 'desc'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Standart::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'breed_id' => $this->breed_id,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'desc', $this->desc]);

        return $dataProvider;
    }
}
