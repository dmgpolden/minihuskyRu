<?php

namespace common\modules\pet\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "registrator".
 *
 * @property integer $id
 * @property string $name
 */
class Registrator extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%registrator}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('pet', 'ID'),
            'name' => Yii::t('pet', 'Name'),
        ];
    }
		public static function getList( )
		{	
	
		$rows = (new \yii\db\Query())
    ->select('id, name')
    ->from(self::tableName())
    ->all();
		
		return ArrayHelper::map($rows,'id', 'name');
		}
}
