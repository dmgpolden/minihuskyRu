<?php

namespace common\modules\pet\models;

use Yii;

/**
 * This is the model class for table "event".
 *
 * @property integer $id
 * @property string $name
 * @property string $desc
 * @property string $text
 * @property string $datetime
 * @property integer $count
 * @property integer $action_id
 *
 * @property Action $action
 */
class Event extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%event}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'datetime'], 'required'],
            [['desc', 'text'], 'string'],
            [['datetime'], 'safe'],
            [['count', 'action_id'], 'integer'],
            [['name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('pet', 'ID'),
            'name' => Yii::t('pet', 'Name'),
            'desc' => Yii::t('pet', 'Desc'),
            'text' => Yii::t('pet', 'Text'),
            'datetime' => Yii::t('pet', 'Date time'),
            'count' => Yii::t('pet', 'Count'),
            'action_id' => Yii::t('pet', 'Action ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAction()
    {
        return $this->hasOne(Action::className(), ['id' => 'action_id']);
    }

		public function beforeSave($insert)
		{
				if (parent::beforeSave($insert)) {
						
						//vd($this);

						return true;
				} else {
						return false;
				}
		}



}
