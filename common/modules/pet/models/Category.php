<?php

namespace common\modules\pet\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "category".
 *
 * @property integer $id
 * @property string $name
 * @property string $desc
 * @property integer $breed_id
 *
 * @property Pet[] $pets
 */
class Category extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%category}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['desc'], 'string'],
            [['breed_id'], 'integer'],
            [['name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('pet', 'ID'),
            'name' => Yii::t('pet', 'Name'),
            'desc' => Yii::t('pet', 'Desc'),
            'breed_id' => Yii::t('pet', 'Breed ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPets()
    {
        return $this->hasMany(Pet::className(), ['category_id' => 'id']);
    }
		
		public static function getList( )
		{	
		$rows = (new \yii\db\Query())
    ->select('id, name')
    ->from(self::tableName())
    ->all();
		
		return ArrayHelper::map($rows,'id', 'name');
		}

}
