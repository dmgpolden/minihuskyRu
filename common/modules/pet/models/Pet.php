<?php

namespace common\modules\pet\models;

use Yii;

/**
 * This is the model class for table "pet".
 *
 * @property integer $id
 * @property string $name
 * @property string $fullname
 * @property string $birthday
 * @property string $dieday
 * @property string $weight
 * @property string $stature
 * @property integer $breed_id
 * @property integer $category_id
 * @property integer $standart_id
 * @property integer $registrator_id
 *
 * @property Standart $standart
 * @property Breed $breed
 * @property Class $class
 */
class Pet extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%pet}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['birthday', 'dieday', 'tatoo','chip', 'reg_num'], 'safe'],
            [['weight', 'stature'], 'number'],
            [['breed_id', 'category_id', 'registrator_id'], 'integer'],
            [['name', 'fullname'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('pet', 'ID'),
            'name' => Yii::t('pet', 'Name'),
            'fullname' => Yii::t('pet', 'Fullname'),
            'birthday' => Yii::t('pet', 'Birthday'),
            'dieday' => Yii::t('pet', 'Dieday'),
            'weight' => Yii::t('pet', 'Weight'),
            'stature' => Yii::t('pet', 'Stature'),
            'reg_num' => Yii::t('pet', 'Reg num'),
            'chip' => Yii::t('pet', 'Chip'),
            'tatoo' => Yii::t('pet', 'Tatoo'),
            'breed_id' => Yii::t('pet', 'Breed'),
            'breed' => Yii::t('pet', 'Breed'),
            'category_id' => Yii::t('pet', 'Class'),
            'standart_id' => Yii::t('pet', 'Standart'),
            'standart' => Yii::t('pet', 'Standart'),
            'category' => Yii::t('pet', 'Class'),
            'registrator_id' => Yii::t('pet', 'Registrator'),
            'registrator' => Yii::t('pet', 'Registrator'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStandart()
    {
        return $this->hasOne(Standart::className(), ['id' => 'standart_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBreed()
    {
        return $this->hasOne(Breed::className(), ['id' => 'breed_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRegistrator()
    {
        return $this->hasOne(Registrator::className(), ['id' => 'registrator_id']);
    }
}
