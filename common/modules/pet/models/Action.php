<?php

namespace common\modules\pet\models;

use Yii;
use yii\helpers\ArrayHelper;
		

/**
 * This is the model class for table "action".
 *
 * @property integer $id
 * @property string $name
 * @property string $desc
 * @property string $text
 * @property string $date
 * @property integer $count
 *
 * @property Event[] $events
 */
class Action extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%action}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'date'], 'required'],
            [['desc', 'text'], 'string'],
            [['date'], 'safe'],
            [['count'], 'integer'],
            [['name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('pet', 'ID'),
            'name' => Yii::t('pet', 'Name'),
            'desc' => Yii::t('pet', 'Desc'),
            'text' => Yii::t('pet', 'Text'),
            'date' => Yii::t('pet', 'Date'),
            'count' => Yii::t('pet', 'Count'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEvents()
    {
        return $this->hasMany(Event::className(), ['action_id' => 'id']);
    }
		
		public static function getList( )
		{	
		$rows = (new \yii\db\Query())
    ->select(['id','CONCAT_WS(" ", name ,date) as name'])
    ->from(self::tableName())
		->orderBy( 'id DESC' )
    ->all();
		return ArrayHelper::map($rows,'id', 'name');
		}
}
