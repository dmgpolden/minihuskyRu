<?php

namespace common\modules\pet\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\modules\pet\models\Pet;

/**
 * PetSearch represents the model behind the search form about `common\modules\pet\models\Pet`.
 */
class PetSearch extends Pet
{
	public $standart;
	public $category;
	public $breed;
	public $registrator;
		
			/**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'breed_id', 'category_id', 'standart_id'], 'integer'],
            [['name', 'fullname', 'tatoo','chip','reg_num','birthday', 'dieday', 'standart','category','breed','registrator'], 'safe'],
            [['weight', 'stature'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Pet::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

				// join with relation `author` that is a relation to the table `users`
				// and set the table alias to be `author`
				$query->joinWith( ['standart','category','breed','registrator']);
				// enable sorting for the related column
				$dataProvider->sort->attributes['standart'] = [
				    'asc' => ['standart.name' => SORT_ASC],
				    'desc' => ['standart.name' => SORT_DESC],
						];
				
				$dataProvider->sort->attributes['category'] = [
				    'asc' => ['category.name' => SORT_ASC],
				    'desc' => ['category.name' => SORT_DESC],
						];

				$dataProvider->sort->attributes['breed'] = [
				    'asc' => ['breed.name' => SORT_ASC],
				    'desc' => ['breed.name' => SORT_DESC],
						];

        $query->andFilterWhere([
            'id' => $this->id,
            'birthday' => $this->birthday,
            'dieday' => $this->dieday,
            'weight' => $this->weight,
            'stature' => $this->stature,
            'stature' => $this->stature,
            'tatoo' => $this->tatoo,
            'chip' => $this->chip,
            'pet.breed_id' => $this->breed_id,
            'pet.category_id' => $this->category_id,
            'pet.standart_id' => $this->standart_id,
            'pet.registrator_id' => $this->registrator_id,
        ]);

        $query->andFilterWhere(['like', 'pet.name', $this->name])
            ->andFilterWhere(['like', 'pet.fullname', $this->fullname]);

        return $dataProvider;
    }
}
