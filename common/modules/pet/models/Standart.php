<?php

namespace common\modules\pet\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "standart".
 *
 * @property integer $id
 * @property string $name
 * @property string $desc
 * @property integer $breed_id
 *
 * @property Pet[] $pets
 * @property Breed $breed
 */
class Standart extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%standart}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['desc'], 'string'],
            [['breed_id'], 'integer'],
            [['name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('pet', 'ID'),
            'name' => Yii::t('pet', 'Name'),
            'desc' => Yii::t('pet', 'Desc'),
            'breed_id' => Yii::t('pet', 'Breed ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPets()
    {
        return $this->hasMany(Pet::className(), ['standart_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBreed()
    {
        return $this->hasOne(Breed::className(), ['id' => 'breed_id']);
    }

		public static function getList($id=null )
		{	
		$query = new \yii\db\Query();
    $query->select('id, name')
    ->from(self::tableName());
		
		if ( $id !== null ) $query->where('breed_id=:id',[':id'=>$id ] );
		$command = $query->createCommand();
		$list = $command->queryAll();
		
		return ArrayHelper::map($list,'id', 'name');
		
		
				

		}
}
