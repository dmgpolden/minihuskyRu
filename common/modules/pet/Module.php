<?php

namespace common\modules\pet;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'common\modules\pet\controllers';
		public $layout='main';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
