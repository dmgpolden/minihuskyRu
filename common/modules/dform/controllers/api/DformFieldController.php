<?php

namespace common\modules\dform\controllers\api;

/**
 * This is the class for REST controller "DformFieldController".
 */
class DformFieldController extends \yii\rest\ActiveController
{
    public $modelClass = 'common\modules\dform\models\DformField';
}
