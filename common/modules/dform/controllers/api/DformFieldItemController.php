<?php

namespace common\modules\dform\controllers\api;

/**
 * This is the class for REST controller "DformFieldItemController".
 */
class DformFieldItemController extends \yii\rest\ActiveController
{
    public $modelClass = 'common\modules\dform\models\DformFieldItem';
}
