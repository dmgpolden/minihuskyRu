<?php

namespace common\modules\dform\controllers\api;

/**
 * This is the class for REST controller "DformController".
 */
class DformController extends \yii\rest\ActiveController
{
    public $modelClass = 'common\modules\dform\models\Dform';
}
