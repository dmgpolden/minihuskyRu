<?php

namespace common\modules\dform\controllers;

/**
 * This is the class for controller "DformFieldController".
 */
class DformFieldController extends \common\modules\dform\controllers\base\DformFieldController
{
    public function actionOrder( )   {
        $post = \Yii::$app->request->post( );
        if (isset( $post['key'], $post['pos'] ))   {
            $this->findModel( $post['key'] )->order( $post['pos'] );
        }
    }
}
