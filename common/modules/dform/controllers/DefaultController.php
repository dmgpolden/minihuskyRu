<?php

namespace common\modules\dform\controllers;

use yii\web\Controller;

use common\modules\dform\models\Dform;
use common\modules\dform\models\search\Dform as DformSearch;
use yii\web\HttpException;
use yii\helpers\Url;
use yii\filters\AccessControl;
use dmstr\bootstrap\Tabs;
use yii\base\DynamicModel;
use \yii\helpers\ArrayHelper;

class DefaultController extends Controller
{
	
	/**
	 * Lists all Dform models.
	 * @return mixed
	 */
	public function actionIndex()
	{
		$searchModel  = new DformSearch;
		$dataProvider = $searchModel->search($_GET);

		Tabs::clearLocalStorage();

        Url::remember();
        \Yii::$app->session['__crudReturnUrl'] = null;

		return $this->render('index', [
			'dataProvider' => $dataProvider,
			'searchModel' => $searchModel,
		]);
	}

    /**
	 * Displays a single Dform model.
	 * @param integer $id
     *
	 * @return mixed
	 */
	public function actionView($id)
	{
        \Yii::$app->session['__crudReturnUrl'] = Url::previous();
        Url::remember();
        Tabs::rememberActiveState();

        $form = $this->findModel($id);
//        vd(ArrayHelper::toArray($form->dformFields));
        $cols = ArrayHelper::getcolumn($form->dformFields,'slug') ;
        $model = new DynamicModel($cols);
        $dform = $form->dformFields;
        $model->attributeLabels(ArrayHelper::map($form->dformFields,'slug','name'));
        foreach ( $dform as $field){
            if ($field->required ) $model->addRule($field->slug, 'required',['message' => 'Необходимо заполнить: '. $field->name]);
            $model->addRule($field->slug, 'trim');
            $model->addRule($field->slug, $field->validator);
        }

		try {
            if ($model->load($_POST) && $model->validate()) {
                return $this->render('mail',[
                    'model' => $model,
                    'dform' => $dform
                ]);
            } else  {
                return $this->render('view', [
        			'model' => $model,
                    'dform' => $dform
		]);
            }
        } catch (\Exception $e) {
            $msg = (isset($e->errorInfo[2]))?$e->errorInfo[2]:$e->getMessage();
            $model->addError('_exception', $msg);
		}
	}
	
    /**
	 * Finds the Dform model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * @param integer $id
	 * @return Dform the loaded model
	 * @throws HttpException if the model cannot be found
	 */
	protected function findModel($id)
	{
		if (($model = Dform::findOne($id)) !== null) {
			return $model;
		} else {
			throw new HttpException(404, 'The requested page does not exist.');
		}
	}
}
