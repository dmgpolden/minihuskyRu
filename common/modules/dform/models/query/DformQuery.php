<?php

namespace common\modules\dform\models\query;

/**
 * This is the ActiveQuery class for [[\common\modules\dform\models\Dform]].
 *
 * @see \common\modules\dform\models\Dform
 */
class DformQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return \common\modules\dform\models\Dform[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\modules\dform\models\Dform|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
