<?php

namespace common\modules\dform\models\query;

/**
 * This is the ActiveQuery class for [[\common\modules\dform\models\DformFieldItem]].
 *
 * @see \common\modules\dform\models\DformFieldItem
 */
class DformFieldItemQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return \common\modules\dform\models\DformFieldItem[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\modules\dform\models\DformFieldItem|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
