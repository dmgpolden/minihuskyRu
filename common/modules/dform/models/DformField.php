<?php

namespace common\modules\dform\models;

use Yii;
use \common\modules\dform\models\base\DformField as BaseDformField;
use \yii\behaviors\TimestampBehavior;
use \yii\helpers\ArrayHelper;

/**
 * This is the model class for table "dform_field".
 */
class DformField extends BaseDformField
{
    const TYPE_RADIOLIST = 1;
    const TYPE_CHECKBOXLIST = 2;
    const TYPE_TEXTAREA = 3;
    const TYPE_TEXTINPUT = 4;

    const VALIDATOR_STRING = 1;
    const VALIDATOR_NUMBER = 2;
    const VALIDATOR_EMAIL = 3; 

    public function getType(){
        $types = self::types();
        return $types[$this->type_id];
   }
   public static function types(){
       return [
        self::TYPE_RADIOLIST => 'radioList',
        self::TYPE_CHECKBOXLIST => 'checkboxList',
        self::TYPE_TEXTAREA => 'textarea',
        self::TYPE_TEXTINPUT => 'textInput',
       ];
  }

    public function getValidator(){
        $validators = self::validators();
        return ArrayHelper::getValue($validators, $this->validator_id, 'safe') ;
   }
   public static function validators(){
       return [
        self::VALIDATOR_STRING => 'string',
        self::VALIDATOR_NUMBER => 'number',
        self::VALIDATOR_EMAIL => 'email',
       ];
  }
    
    public function rules()
    {
        return [
            [['dform_id', 'type_id', 'validator_id', 'created_at', 'updated_at'], 'integer'],
            [['required'],'safe'],
            [['name', 'slug',], 'string', 'max' => 25],
            [['name', 'slug',], 'trim'],
            [['slug'], 'match', 'pattern' => '/^[a-zA-Z0-9_]+$/', 'message' => 'Только латинские буквы, "_" и цифры'],
            [['dform_id'], 'exist', 'skipOnError' => true, 'targetClass' => Dform::className(), 'targetAttribute' => ['dform_id' => 'id']],
        ];
    }

    public function behaviors() {
        return [
            [
                'class' => TimestampBehavior::className(),
            ],
            [
                'class' => 'sjaakp\sortable\Sortable',
            ],
		];
	}
}
