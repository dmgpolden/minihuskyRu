<?php

namespace common\modules\dform\models;

use Yii;
use \common\modules\dform\models\base\DformFieldItem as BaseDformFieldItem;

/**
 * This is the model class for table "dform_field_item".
 */
class DformFieldItem extends BaseDformFieldItem
{
}
