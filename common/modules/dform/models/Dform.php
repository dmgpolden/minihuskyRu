<?php

namespace common\modules\dform\models;

use Yii;
use \common\modules\dform\models\base\Dform as BaseDform;
use \yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "dform".
 */
class Dform extends BaseDform
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'slug'], 'required'],
            [['created_at', 'updated_at'], 'integer'],
            [['name', 'slug'], 'string', 'max' => 25],
            [['slug'], 'match', 'pattern' => '/[\w-_]+/','message' => 'Только латинские буквы, "-", "_" и цифры']
        ];
    }
    public function behaviors() {
        return [
              [
                  'class' => TimestampBehavior::className(),
              ],
		];
	}
}
