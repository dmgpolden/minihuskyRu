<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use \dmstr\bootstrap\Tabs;
use \yii\helpers\ArrayHelper;

?>

<div class="">
<h1><?= $this->context->action->uniqueId ?></h1>
<?php
    $form = ActiveForm::begin([
    'id' => 'DformField',
    'layout' => 'horizontal',
    'enableClientValidation' => false, // true,
    'errorSummaryCssClass' => 'error-summary alert alert-error'
    ]
    );
    ?>

    <div class="">

        <p>

<?php
    foreach ($dform as $field){?>
<?php
     $type = $field->type;
     $list = ArrayHelper::map($field->dformFieldItems,'id','value') ;
?>

<?= $form->field($model, $field->slug)
    ->label($field->name)
    ->$type($list) ?>
<?php 
     } ?>
        <hr/>

        <?php echo $form->errorSummary($model); ?>

        <?= Html::submitButton(
        '<span class="glyphicon glyphicon-check"></span> ' .
            //($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save')),
        Yii::t('app', 'Save'),
        [
        'id' => 'save-' . $model->formName(),
        'class' => 'btn btn-success'
        ]
        );
        ?>

        <?php ActiveForm::end(); ?>

    </div>

</div>
