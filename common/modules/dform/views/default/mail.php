<?php

//use yii\helpers\Html;
use yii\bootstrap\Html;
use \yii\helpers\ArrayHelper;

echo __FILE__;
?>

<div class="">
<h1><?= $this->context->action->uniqueId ?></h1>

    <div class="form-horizontal">

        <p>

<?php
    foreach ($dform as $field){
     $type = $field->type;
     $attr = $field->slug;
echo Html::beginTag('div', ['class'=>'row']);
  if ( $list = $field->dformFieldItems){
        echo Html::tag('label', $field->name,['class'=>'col-sm-3 control-label']);
        echo Html::beginTag('div',['class'=>'col-sm-6']);
    foreach (ArrayHelper::map($list,'id','value') as $value=>$label){
            $selection = $model->$attr ;
            $checked = $selection !== null &&
                (!is_array($selection) && !strcmp($value, $selection)
                    || is_array($selection) && in_array($value, $selection));
        $ch = ( $checked ) ? ' - V' : ' - x' ;
        echo Html::tag('div', $label . $ch );
    }
        echo Html::endTag('div');
      } else {
        echo Html::tag('label',$field->name,['class'=>'col-sm-3 control-label ']);
echo Html::beginTag('div',['class'=>'col-sm-6']);
        echo Html::tag('span', $model->$attr);
echo Html::endTag('div');
      }
echo Html::endTag('div');
 }
/*
    foreach ($dform as $field){
     $type = $field->type;
     $attr = $field->slug;
echo Html::beginTag('div', ['class'=>'form-group']);
  if ( $list = $field->dformFieldItems){
        echo Html::tag('label',$field->name,['class'=>'col-sm-3 control-label ']);
        $input=substr($type,0,strpos($type,'List'));
            $options['item'] = function ($index, $label, $name, $checked, $value) use ($input) {
                return '<div class="checkbox">' . Html::$input($name, $checked, ['label' => $label, 'value' => $value, 'disabled'=>true]) . '</div>';
                };
            $options['class']='radio col-sm-6';
            $options['itemOptions']=['readonly'=>true];
        echo Html::$type($field->slug,$model->$attr,ArrayHelper::map($list,'id','value'), $options );
      } else {
        echo Html::tag('label',$field->name,['class'=>'col-sm-3 control-label ']);
echo Html::beginTag('div',['class'=>'radio col-sm-6']);
            unset($options);
            $options['readonly']=true;
        echo Html::$type($field->slug, $model->$attr, $options );
echo Html::endTag('div'); 
      }
echo Html::endTag('div'); 
 }
*/
?>
        <hr/>

    </div>

</div>

<?php
