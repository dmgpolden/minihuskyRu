<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use \dmstr\bootstrap\Tabs;

/**
* @var yii\web\View $this
* @var common\modules\dform\models\Dform $model
* @var yii\widgets\ActiveForm $form
*/

?>

<div class="dform-form">

    <?php $form = ActiveForm::begin([
    'id' => 'Dform',
    'layout' => 'horizontal',
    'enableClientValidation' => true,
    'errorSummaryCssClass' => 'error-summary alert alert-error'
    ]
    );
    ?>

    <div class="">
        <?php $this->beginBlock('main'); ?>
        <p>
			<?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
			<?= $form->field($model, 'slug')->textInput(['maxlength' => true]) ?>
        </p>
        <?php $this->endBlock(); ?>
        <?php $this->beginBlock('main2'); ?>
        <p>
			<?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
			<?= $form->field($model, 'slug')->textInput(['maxlength' => true]) ?>
        </p>
        <?php $this->endBlock(); ?>
        
<?=
    Tabs::widget(
             [
               'encodeLabels' => false,
                 'items' => [ 
                    [
                    'label'   => 'Dform',
                    'content' => $this->blocks['main'],
                    'active'  => true,
                    ],
                    [
                    'label'   => 'Dform2',
                    'content' => $this->blocks['main2'],
//                    'active'  => true,
                    ],
                 ]
             ]
    );
    ?>
        <hr/>

        <?php echo $form->errorSummary($model); ?>

        <?= Html::submitButton(
        '<span class="glyphicon glyphicon-check"></span> ' .
        ($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save')),
        [
        'id' => 'save-' . $model->formName(),
        'class' => 'btn btn-success'
        ]
        );
        ?>

        <?php ActiveForm::end(); ?>

    </div>

</div>

