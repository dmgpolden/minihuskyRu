<?php

namespace common\modules\dform;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'common\modules\dform\controllers';

    public function init()
    {
        parent::init();
        // custom initialization code goes here
    }
}
