<?php

use yii\db\Migration;
use yii\db\Schema;

class m151223_132100_init extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%dform}}', [
            'id'            => Schema::TYPE_PK,
            'name'      => Schema::TYPE_STRING . '(25) NOT NULL',
            'slug'      => Schema::TYPE_STRING . '(25) NOT NULL',
            'created_at' => Schema::TYPE_INTEGER . ' NOT NULL',
            'updated_at' => Schema::TYPE_INTEGER . ' NOT NULL',
        ]);

        $this->createTable('{{%dform_field}}', [
            'id'           => Schema::TYPE_PK,
            'dform_id'     => Schema::TYPE_INTEGER,
            'required'     => Schema::TYPE_BOOLEAN,
            'name'         => Schema::TYPE_STRING . '(25) NOT NULL',
            'slug'         => Schema::TYPE_STRING . '(25) NOT NULL',
            'type_id'      => Schema::TYPE_INTEGER . ' NOT NULL',
            'validator_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'created_at'   => Schema::TYPE_INTEGER . ' NOT NULL',
            'updated_at'   => Schema::TYPE_INTEGER . ' NOT NULL',
        ]);
        $this->addColumn('{{%dform_field}}', 'ord', Schema::TYPE_INTEGER);

        $this->createTable('{{%dform_field_item}}', [
            'id'           => Schema::TYPE_PK,
            'field_id'      => Schema::TYPE_INTEGER . ' NOT NULL',
            'value'      => Schema::TYPE_STRING . '(25) NOT NULL',
        ]);
        
        $this->addForeignKey('fk_dform_field', '{{%dform_field}}', 'dform_id', '{{%dform}}', 'id', 'CASCADE', 'RESTRICT');
        $this->addForeignKey('fk_dform_field_item', '{{%dform_field_item}}', 'field_id', '{{%dform_field}}', 'id', 'CASCADE', 'RESTRICT');
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_dform_field', '{{%dform_field}}');
        $this->dropForeignKey('fk_dform_field_item', '{{%dform_field_item}}');
        $this->dropTable('{{%dform}}');
        $this->dropTable('{{%dform_field}}');
        $this->dropTable('{{%dform_field_item}}');
        
    }
}
