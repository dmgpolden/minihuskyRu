<?php

namespace common\modules\present\controllers;

use Yii;
use common\modules\present\models\Present;
use common\modules\present\models\PresentSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use vova07\fileapi\actions\UploadAction as FileAPIUpload;
use common\extensions\fileapi\actions\UploadAction;

/**
 * DefaultController implements the CRUD actions for Present model.
 */
class DefaultController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

public function actions()
{
    return [
          'uploadTemp' => [
              'class' => UploadAction::className(),
              'path' => \Yii::getAlias('@static/image'),
//              'item' => 'common\modules\present\models\Present',
              'types' => ['jpg', 'png', 'gif'],
              'minHeight' => 100,
              'maxHeight' => 2000,
              'minWidth' => 100,
              'maxWidth' => 3000,
              'maxSize' => 3145728 // 3*1024*1024 = 3MB
          ],
        'upload' => [
            'class' => FileAPIUpload::className(),
            'path' => '@static/temp'
        ]
    ];
}

    /**
     * Lists all Present models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PresentSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Present model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Present model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Present();
//            vd($model->load(Yii::$app->request->post()));
         if ($model->load(Yii::$app->request->post())  ) {
            $model->imageFile = UploadedFile::getInstance($model, 'imageFile');
                if ( $model->validate()) {
                    if ($model->imageFile !==null ){
                        $fileName = $model->imageFile->name;
                        $model->imageFile->saveAs(Yii::getAlias('@static/image/') . $model->imageFile->baseName . '.' . $model->imageFile->extension);
                        $model->name = $fileName ;
                    }
                        if ($model->save(false) ) {
              //          vd($model);
                            return $this->redirect(['index']);
                    }
            }
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Present model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
            
         if ($model->load(Yii::$app->request->post())  ) {
            $model->imageFile = UploadedFile::getInstance($model, 'imageFile');
                if ( $model->validate()) {
                    if ($model->imageFile !==null ){
                        $fileName = $model->imageFile->name;
                        $model->imageFile->saveAs(Yii::getAlias('@static/image/') . $model->imageFile->baseName . '.' . $model->imageFile->extension);
                        $model->name = $fileName ;
                    }
                        if ($model->save(false) ) {
              //          vd($model);
                            return $this->redirect(['index']);
                    }
                }
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Present model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Present model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Present the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Present::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
