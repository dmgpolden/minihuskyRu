<?php

namespace common\modules\present;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'common\modules\present\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
