<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\modules\present\models\Present */

$this->title = Yii::t('app', 'Create {modelClass}', [
    'modelClass' => 'Present',
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Presents'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="present-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
