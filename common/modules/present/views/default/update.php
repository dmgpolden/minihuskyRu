<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\modules\present\models\Present */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Present',
]) . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Presents'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="present-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
