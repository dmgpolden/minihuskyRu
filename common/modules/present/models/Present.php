<?php

namespace common\modules\present\models;

use Yii;
use vova07\fileapi\behaviors\UploadBehavior;

/**
 * This is the model class for table "{{%present}}".
 *
 * @property integer $id
 * @property string $name
 * @property string $ext
 * @property string $size
 * @property string $type
 * @property string $title
 * @property string $url
 * @property string $description
 * @property string $datetime
 */
class Present extends \yii\db\ActiveRecord
{
    /**
    *
    */
    public $imageFile;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%present}}';
    }

    function afterSave($insert, $changedAttributes)
    {
         $cache = \Yii::$app->cache;
        \yii\caching\TagDependency::invalidate($cache,['present']);
    }

    function afterDelete()
    {
         $cache = \Yii::$app->cache;
        \yii\caching\TagDependency::invalidate($cache,['present']);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'description','title','url'], 'string'],
            [['datetime'], 'safe'],
            [['name', 'ext', 'size', 'type'], 'default', 'value'=> '' ],
            [['ext', 'size', 'type', 'title', 'url'], 'string', 'max' => 255],
            [['imageFile'], 'file', 'skipOnEmpty' => true,  'extensions' => 'png, jpg']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'ext' => Yii::t('app', 'Ext'),
            'size' => Yii::t('app', 'Size'),
            'type' => Yii::t('app', 'Type'),
            'title' => Yii::t('app', 'Title'),
            'url' => Yii::t('app', 'Url'),
            'description' => Yii::t('app', 'Description'),
            'datetime' => Yii::t('app', 'Datetime'),
        ];
    }

}
