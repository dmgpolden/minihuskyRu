<?php

namespace common\modules\page;

use Yii;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'common\modules\page\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here

       $this->registerTranslations( );
    }
		
		public function registerTranslations()
    {
        Yii::$app->i18n->translations['page'] = [
            'class' => 'yii\i18n\PhpMessageSource',
            'basePath' => '@common/modules/page/messages',
/*          'fileMap' => [
                'modules/users/validation' => 'validation.php',
                'modules/users/form' => 'form.php',
            ],
*/
        ];
    }

    public static function t( $message, $params = [], $language = null)
    {
        return Yii::t('page', $message, $params, $language);
    }
}
