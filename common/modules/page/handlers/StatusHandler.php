<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace common\modules\page\handlers;

use common\components\ImageWatermark;
use common\modules\page\models\Status;
use common\modules\file\models\File;
use frontend\modules\page\models\Page;
use Yii;
use yii\helpers\FileHelper;

/**
 * Description of StatusHandler
 *
 * @author dmg
 */
class StatusHandler {
	
    /**
     * @var string Directory for original image
     */
    private $originalDir;
    
    /**
     * @var string Directory for target image
     */
    private $targetDir;
    
    /**
     * @var string Directory for status image
     */
    private $statusImageDir;
    
    public function __construct() {
        $this->originalDir = Yii::getAlias('@static/imageOriginal');
        $this->targetDir = Yii::getAlias('@static/image');
        $this->statusImageDir = Yii::getAlias('@static/imageStatus');
    }
    
    /**
     * 
     * @param Status $status
     */
    public function changeStatusImage($status) {
        $pages = Page::find()->where(['status' => $status->id])->all();
        foreach($pages as $page){
            $this->pageStasusImage($page, $status);
        }
    }
    
    /**
     * Change page status
     * Create, change or clear
     * @param Page $page
     */
    public function changePageStatus($page) {
        if( 0 == $page->status || null == $page->status ){
            $this->clearStatusImage($page);
        }else{
            $status= Status::findOne($page->status);
            $this->pageStasusImage($page, $status);
        }
    }
    
    /**
     * Clear image watermark
     * @param type $page
     */
    public function clearStatusImage($page) {
        foreach($page->images as $file){
            if(file_exists($this->originalDir . DIRECTORY_SEPARATOR . $file->name)){
                if(file_exists( $this->targetDir . DIRECTORY_SEPARATOR . $file->name)){
                unlink($this->targetDir . DIRECTORY_SEPARATOR . $file->name);
                }
                \copy($this->originalDir . DIRECTORY_SEPARATOR . $file->name, $this->targetDir . DIRECTORY_SEPARATOR . $file->name);
                unlink($this->originalDir . DIRECTORY_SEPARATOR . $file->name);
            }
        }
    }
    
    /**
     * Status images for pages
     * @param Page $page
     * @param Status $status 
     */
    public function pageStasusImage($page, $status) {
        if (null !== $page->image) {
            $this->generate($page->image, $status);
        }
        
        foreach($page->images as $file) {
            $this->generate($file, $status);
        }
    }
    
     /**
     * Work with new file
     * @param Page $page
     * @param File $file
     */
    public function newImage($page, $file) {
        if( null != $page->status && 0 != $page->status ){
            $status= Status::findOne($page->status);
            $this->generate($file, $status);
        }
    }
     /**
     * Generate watrmark image
     * @param File $file
     * @param Status $status 
     */
    public function generate($file, $status) {
        FileHelper::createDirectory($this->originalDir);
        FileHelper::createDirectory($this->targetDir);
        $statusImage = $this->statusImageDir . DIRECTORY_SEPARATOR . 'status' . ucfirst($status->slug) . '.png';
           if(!file_exists($this->originalDir . DIRECTORY_SEPARATOR . $file->name)){
                \copy($this->targetDir . DIRECTORY_SEPARATOR . $file->name, $this->originalDir . DIRECTORY_SEPARATOR . $file->name);
            }
            if(file_exists( $this->targetDir . DIRECTORY_SEPARATOR . $file->name)){
                unlink($this->targetDir . DIRECTORY_SEPARATOR . $file->name);
            }
            $watermark = new ImageWatermark(
                $this->originalDir . DIRECTORY_SEPARATOR . $file->name,
                $this->targetDir . DIRECTORY_SEPARATOR . $file->name,
                $statusImage,
                $status->position
                );
            $watermark->generateWatermark();
        
    }
}
