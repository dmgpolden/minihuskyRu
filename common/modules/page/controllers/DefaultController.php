<?php

namespace common\modules\page\controllers;

use common\extensions\fileapi\actions\UploadAction;
use common\modules\page\models\Page;
use common\modules\page\models\PageSearch;
use vova07\imperavi\actions\GetImagesAction as GetAction;
use Yii;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * PageController implements the CRUD actions for Page model.
 */
class DefaultController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'uploadTemp' => [
              'class' => UploadAction::className(),
              'path' => \Yii::getAlias('@static/image'),
              'item' => 'common\modules\page\models\Page',
              'types' => ['jpg', 'png', 'gif'],
              'minHeight' => 100,
              'maxHeight' => 5000,
              'minWidth' => 100,
              'maxWidth' => 5000,
              'maxSize' => 3145728 // 3*1024*1024 = 3MB
            ],
            'images-get' => [
                'class' => GetAction::className(),
                'url' =>'/big/', // Directory URL address, where files are stored.
                'path' => '@static/image', // Or absolute path to directory where files are stored.
            ],
            'image-upload' => [                                                                                                                 'class' => 'common\extensions\imperavi\actions\UploadAction',                                                                    'url' =>'/big/', // Directory URL address, where files are stored.
                'path' => '@static/image', // Or absolute path to directory where files are stored.
            ],   
        ];
    }

    /**
     * Lists all Page models.
     * @return mixed
     */
    public function actionIndex()
    {

        $searchModel = new PageSearch();
        $session = \Yii::$app->session;
        $searchQuery = $session->has('pageIndex') ?  $session->get('pageIndex') : [];
        $searchQuery = array_merge($searchQuery, Yii::$app->request->getQueryParams() );
        $session->set('pageIndex', $searchQuery);
        $dataProvider = $searchModel->search($searchQuery );

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    /**
     * Displays a single Page model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionCreate()
    {
        $model = new Page;
        if ($model->load(Yii::$app->request->post()) ) 
            if($model->parent_id){
                $parent=Page::find()->where(['id'=>$model->parent_id])->one();
                if($model->appendTo($parent)) {
                    return $this->redirect(['index']);
                }
            }else{
                if($model->makeRoot()) {
                    return $this->redirect(['index']);
                }
            }
    
        return $this->render('create', [
                    'model' => $model,
                ]);
    }

    /**
     * Updates an existing Page model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findFullModel($id);
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
           if($model->isAttributeChanged('parent_id',false)){
                if($model->parent_id==0 ){
                    $model->save();
                }else{
                    $parent = Page::findOne($model->parent_id);
                    $model->prependTo($parent);
                }
            }else{
                $model->save();
            }
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }
    /**
     * Updates an existing Page model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdateparent()
    {
        $post=Yii::$app->request->post();
        $pk = unserialize(base64_decode($post['pk']));
        $model = $this->findFullModel((int)$pk);
        $oldparent= $model->parent;
        $model->parent=(int)$post['value'];
            if($model->parent==0 ){
                $model->makeRoot();
            }else
                if ($model->parent != $model->id){
                    $root = Page::findOne($model->parent);
                    $model->prependTo($root);
                    }
             else
                throw new NotFoundHttpException('self to self 2');
    
    }

    /**
     * Updates an existing Page model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUp($id)
    {
        $model = $this->findModel($id);
        if ($prev=$model->prev()->one()){
        $model->insertBefore($prev);
        }
            return $this->redirect(['index']);
    }
    /**
     * Updates an existing Page model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDown($id)
    {
        $model = $this->findModel($id);
        if ($next=$model->next()->one()){
            $model->insertAfter($next);
        }
        return $this->redirect(['index']);
    }

    /**
     * Deletes an existing Page model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->deleteWithChildren();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Page model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Page the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if ($id !== null && ($model = Page::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    protected function findFullModel($id)
    {
        if ($id !== null && ($model = Page::findFullOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
