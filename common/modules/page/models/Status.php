<?php

namespace common\modules\page\models;

use common\modules\page\handlers\StatusHandler;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;

/**
 * This is the model class for table "status".
 *
 * @property string $id
 * @property string $name
 * @property string $slug
 * @property string $file
 * @property string $position
 **/

class Status extends ActiveRecord {
    
    /**
    * @var UploadedFile Filed for form
    **/
    public $fileLoad;
    
    /**
    * @var string path to image Status
    **/
    public $pathStatus = '@static/imageStatus/';
    
    /**
    * @var string path to image
    **/
    public $pathImage = '@static/image/';
    
    /**
	 * @inheritdoc
	 * @return string
	 */
	public static function tableName() {
        return '{{%status}}';
    }
        
    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [[ 'name', 'slug', 'position' ], 'required' ],
            [[ 'name', 'slug', 'position' ], 'string' ],
            [[ 'file' ], 'string' ],
            [[ 'fileLoad' ], 'image', 'skipOnEmpty' => true, 'extensions' => 'png'],
            ];
    }
    
    public function afterSave($insert, $changedAttributes) {
        if (false === $insert){
            $statusHandler = new StatusHandler();
            $statusHandler->changeStatusImage($this);
        }
        parent::afterSave($insert, $changedAttributes);
    }
    
    public function upload() {
        if( null !== $this->fileLoad = UploadedFile::getInstance($this, 'fileLoad')){
            $fileName = 'status' . ucfirst($this->slug) . '.png';
            $this->fileLoad->saveAs($this->pathImage . $fileName, false);
            $this->fileLoad->saveAs($this->pathStatus . $fileName);
            $this->file = $fileName;
            
        }
    }
    
    public static function positionOptions(){
        return [
            'bottomRight' => 'Нижний правый',
            'bottomLeft' => 'Нижний левый',
            'topRight' => 'Верхний правый',
            'topLeft' => 'Верхний левый',
            ];
    }


    public static function options(){
        $res = self::find()
			->from(self::tableName())
			->asArray()
            ->all()
            ;
        return ArrayHelper::map($res,'id','name');
    }

}
