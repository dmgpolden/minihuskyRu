<?php

namespace common\modules\page\models;

use common\modules\file\models\File;
use common\modules\page\handlers\StatusHandler;
use creocoder\nestedsets\NestedSetsBehavior;
use Exception;
use PDO;
use Yii;
use yii\base\Component;
use yii\base\InvalidConfigException;
use yii\caching\TagDependency;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "tbl_Page".
 *
 * @property string $id
 * @property string $tree
 * @property string $lft
 * @property string $rgt
 * @property integer $depth
 * @property string $title
 * @property string $slug
 * @property string $description
 */
class Page extends ActiveRecord {

    public $parent;
    public $parent_name;
    public $tree_name;
    public $delImage;
    public $add_image_id;
    public $del_image_id;

    /**
     * @inheritdoc
     */


    public static function tableName() {
        return '{{%page}}';
    }
    
    public static function tablePageFile()
    {
        return 'page_file';
    }


    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [[ 'visible','box','textbox','present', 'image_id', 'parent_id', 'status'], 'integer' ],
            [[ 'delImage'], 'safe'],
	    [[ 'delImage','add_image_id'], 'safe'],
            [[ 'title' ], 'required' ],
            [[ 'slug' ], 'string' ],
            [['title', 'caption', 't_title', 'description', 'keywords', 'external_link'], 'string', 'max' => 255],
            [['content'], 'string'],
            [['note'], 'string', 'max' => 200],
            [['image'], 'image', 'maxFiles'=>10],
        ];
    }
    
    function beforeSave($insert)
    {
       if($this->add_image_id){
        $this->image_id=$this->add_image_id;
       }
        return parent::beforeSave($insert);
    }


    function afterSave($insert, $changedAttributes)
    {
         $statusHandler = new StatusHandler();
         $statusHandler->changePageStatus($this);
         
         $cache = \Yii::$app->cache;
         TagDependency::invalidate($cache,['page']);
       
       if($this->add_image_id){
            \Yii::$app->db->createCommand()
            ->delete(self::tablePageFile(),
            ['page_id'=>$this->id,
            'file_id'=>$this->add_image_id
            ])
            ->execute();
       }

        parent::afterSave($insert, $changedAttributes);
       if (is_array($this->delImage))     {
            $this->unlinkFiles();
           }

       if(array_key_exists('sphinx',Yii::$app->components)){
	  try {
		if($insert){
                    Yii::$app->sphinx->createCommand()
                        ->insert('{{%page}}', [
                            'content' => $this->content,
                            'id' => $this->id,
                            'visible' => $this->visible,
                         ])->execute();
            } else {
                    Yii::$app->sphinx->createCommand()
                        ->replace('{{%page}}', [
                            'id' => $this->id,
                            'content' => $this->content,
                            'visible' => $this->visible,
                         ])->execute();
            }
	  } catch (Exception $e){
		  /**
		   * @TODO: error log
		   */
		}
      }
    }
    

    function afterDelete()
    {
       if (is_array($this->delImage))     {
            $this->unlinkFiles();
           }
       if(array_key_exists('sphinx',Yii::$app->components)){
                Yii::$app->sphinx->createCommand()
                    ->delete('{{%page}}', [
                        'id' => $this->id
                     ])->execute();
      }
    }
        
        private function unlinkFiles()
        {   
            \Yii::$app->db->createCommand()
            ->delete(Page::tablePageFile(),
            ['page_id'=>$this->id,
            'file_id'=>$this->delImage
            ])
            ->execute();
        }

        private function unlinkPageFiles()
        {   
            \Yii::$app->db->createCommand()
            ->delete(Page::tablePageFile(),
            ['page_id'=>$this->id,
            ])
            ->execute();
        }

    public function getFiles()
        {
            return $this->hasMany(File::className(), ['id' => 'file_id'])
                ->viaTable('page_file', ['page_id' => 'id']);
        }
    
    public function getImages()
        {
            return $this->hasMany(File::className(), ['id' => 'file_id'])
                ->viaTable('page_file', ['page_id' => 'id']);
//                ->asArray();
        }

    public function getImage()
        {
            return $this->hasOne(File::className(), ['id' => 'image_id']);
        }

        public static function getList( )
        {    
        $res=array( );
            $items = (new Query())
            ->select(['id','title','depth'])
            ->from(self::tableName())
            ->orderBy( 'lft' )
            ->all();
                foreach ( $items as $key=>$item ){
                    $res[$item['id']] = str_repeat('-', $item['depth']-1) . $item['title'];
                }
            return $res;
            //return ArrayHelper::map($items,'id', 'name');
        }

    
    public static function findFullOne($id)
    {
        $cat=self::tableName();
            $primaryKey = parent::primaryKey();
            if (isset($primaryKey[0])) {
                $condition = [$cat.'.'.$primaryKey[0] => $id];
            } else {
                throw new InvalidConfigException(get_called_class() . ' must have a primary key.');
            }
        $query = parent::find($id)
            ->select($cat.'.*,prev.id as prev, next.id as next, parent.id as parent,parent.title as parent_name')
            ->leftJoin($cat.' parent', $cat.'.lft > parent.lft AND '. $cat.'.rgt<parent.rgt AND '. $cat.'.tree=parent.tree')
            ->leftJoin($cat.' prev', $cat.'.lft-1 = prev.rgt AND '. $cat.'.tree=prev.tree')
            ->leftJoin($cat.' next', $cat.'.rgt+1 = next.lft AND '. $cat.'.tree=next.tree')
            ->joinWith(['image'])
            ->andWhere($condition);
        return $query->one();
    }



    public static function findFull()
    {
        $cat=self::tableName();
        $query = parent::find()
            ->select(new Expression($cat.'.*,CONCAT(REPEAT("-",'.$cat.'.depth-1),'.$cat.'.title) as title,'. $cat.'.slug, prev.id as prev, next.id as next, r.slug as tree_name'))
//            ->select($cat.'.title,'. $cat.'.slug, prev.id as prev, next.id as next, parent.id as parent, parent.title as parent_name, r.slug as tree_name')
//            ->leftJoin($cat.' parent', $cat.'.lft > parent.lft AND '. $cat.'.rgt<parent.rgt AND '. $cat.'.tree=parent.tree')
            ->leftJoin($cat.' prev', $cat.'.lft-1 = prev.rgt AND '. $cat.'.tree=prev.tree')
            ->leftJoin($cat.' next', $cat.'.rgt+1 = next.lft AND '. $cat.'.tree=next.tree')
            ->leftJoin($cat.' r', $cat.'.tree = r.id')
//            ->andWhere($cat.'.lft>1')
            ->orderBy( [ $cat.'.tree' => SORT_ASC, $cat.'.lft' => SORT_ASC]);
        return $query;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
                'id' => \Yii::t ( 'app', 'ID' ),
                'tree' => \Yii::t ( 'app', 'Root' ),
                'lft' => \Yii::t ( 'app', 'Lft' ),
                'rgt' => \Yii::t ( 'app', 'Rgt' ),
                'depth' => \Yii::t ( 'app', 'depth' ),
                'title' => \Yii::t ( 'app', 'Title' ),
                'description' => \Yii::t ( 'app', 'Description' ),
            'image_id' => \Yii::t('app','титульная картинка'),
           'add_image_id' => Yii::t('post','Сделать титульной'),
            'delImage' => \Yii::t('app','к удалению'),
        ];
    }
    /**
     * Configure extra behaviors
     *
     * @see Component::behaviors()
     */
    public function behaviors() {
        return [
            'slug' => [
                'class' => 'Zelenin\yii\behaviors\Slug',
                'attribute' => 'title',
                'slugAttribute' => 'slug',
                'ensureUnique' => true,
                'replacement' => '-',
                'lowercase' => true,
                'transliterateOptions' => 'Russian-Latin/BGN;'
            ],
			'nestedset'=>[
                'class' => NestedSetsBehavior::className(),
                'treeAttribute' => 'tree',
			],
		];
	}

    public function transactions()
    {
        return [
            self::SCENARIO_DEFAULT => self::OP_ALL,
        ];
    }

    public static function find()
    {
        return new PageQuery(get_called_class());
    }

    public static function options(){
                $res = self::find()
                ->select(['id,CONCAT(REPEAT("-",depth),title) as text'])
                ->from(self::tableName())
                ->orderBy(['tree'=>SORT_ASC,'lft'=>SORT_ASC])
                ->asArray()
                ->all()
                ;
        return ArrayHelper::map($res,'id','text');
    }

    public static function listoption(){
                $query = (new Query())
                ->select(['id as value,CONCAT(REPEAT("-",depth),title) as text'])
                ->from(self::tableName())
                ->orderBy(['tree'=>SORT_ASC,'lft'=>SORT_ASC])
                ;
    $command=$query->createCommand();
    $command->fetchMode=[PDO::FETCH_ASSOC];
      $res = $command->queryAll( );
        return $res;
    }
}
