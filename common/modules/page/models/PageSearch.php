<?php

namespace common\modules\page\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\modules\page\models\Page;

/**
 * PageSearch represents the model behind the search form about `common\modules\page\models\Page`.
 */
class PageSearch extends Page
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [[ 'visible', 'sequence', 'go_to_type', 'external_link_type', 'removable', 'image', 'present', 'box', 'textbox'], 'integer'],
            [['title', 'caption', 'content', 'note', 'slug', 'title', 'description', 'keywords', 'external_link'], 'safe'],
            ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $this->detachBehavior('slug');
        $cat=self::tableName();
        $query = Page::findFull();

        $dataProvider = new ActiveDataProvider([
                'query' => $query,
                ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }
        //  vd( $query);
        
        $query->andFilterWhere([
            'id' => $this->id,
            $cat . '.visible' => $this->visible,
            'sequence' => $this->sequence,
            'go_to_type' => $this->go_to_type,
            'external_link_type' => $this->external_link_type,
            'removable' => $this->removable,
             $cat . '.image' => $this->image,
             $cat . '.present' => $this->present,
             $cat . '.box' => $this->box,
             $cat . '.textbox' => $this->textbox,
        ]);

        $query->andFilterWhere(['like', $cat . '.title', $this->title])
            ->andFilterWhere(['like', 'caption', $this->caption])
            ->andFilterWhere(['like', 'content', $this->content])
            ->andFilterWhere(['like', 'note', $this->note])
            ->andFilterWhere(['like', $cat . '.slug', $this->slug])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'keywords', $this->keywords])
            ->andFilterWhere(['like', 'external_link', $this->external_link]);

        return $dataProvider;
    }
}
