<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\modules\page\models\Page;
use common\modules\page\models\Status;
use kartik\dropdown\DropdownX;

/* @var $this yii\web\View */
/* @var $searchModel common\modules\page\models\pageSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Page';
$this->params['breadcrumbs'][] = $this->title;
$statuses=Status::options();
?>
<div class="page-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <p>
        <?= Html::a('Create page', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?
    //= GridView::widget([
    //echo \dosamigos\grid\GroupGridView::widget([
    ?>
   <?= kartik\grid\GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'export' => false,
    //    'mergeColumns'=>['tree_name'],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
             'id',
            'tree_name',
            //'rgt',
            [
		'attribute' =>'status',
		'value' => function($model) use ($statuses) {
			if (null == $model->status){
				return '-';
			}else{
				return $statuses[$model->status];
			}
		},
	    ],
		'title',
        'slug',
        [
            'class'=>'kartik\grid\BooleanColumn',
            'attribute'=>'visible', 
            'vAlign'=>'middle',
        ],
        [
            'class'=>'kartik\grid\BooleanColumn',
            'attribute'=>'box', 
            'vAlign'=>'middle',
        ],
        [
            'class'=>'kartik\grid\BooleanColumn',
            'attribute'=>'textbox', 
            'vAlign'=>'middle',
        ],

        [
            'class' => 'yii\grid\ActionColumn',
                'template' => '{view} {up} {down} {update} {delete}',
                'buttons' => [
                'view' => function ($url, $model,$index) {
                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', 'http://'. substr(Yii::$app->urlManager->hostInfo,13).'/'
                            . $model->slug,
                        [
                        'target' => '_blank',
                        'title' => Yii::t('yii', 'View'),
                            'data-pjax' => '0',
                            ]
                            );
                        },
                'up' => function ($url, $model) {
                        //if ($model->prev()->one())
                        return Html::a('<span class="glyphicon glyphicon-chevron-up"></span>', $url, [
                        'title' => Yii::t('yii', 'Move Up'),
                            'data-pjax' => '0',
                            ]);
                        },

                 'down' => function ($url, $model) {
                           return Html::a('<span class="glyphicon glyphicon-chevron-down"></span>', $url, [
                            'title' => Yii::t('yii', 'Move Down'),
                            'data-pjax' => '0',
                            'class' => 'grid-action',
                            ]);
                        },
                ],
            ],
        ],
    ]);
    ?>

<?php foreach (Page::listoption() as $m ) : ?>
        <div><?=$m['text']?></div>
<?php endforeach; ?>

</div>
