<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\modules\page\models\Page;
use kartik\dropdown\DropdownX;

/* @var $this yii\web\View */
/* @var $searchModel common\modules\page\models\pageSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Page';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="page-index">

<?=Page::getPage( ) ?>
    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create page', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= kartik\grid\GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'root',
            //'lft',
            //'rgt',
            //'level',
            'name',
            'caption',
            // 'content:ntext',
						[
							'class'=>'kartik\grid\BooleanColumn',
							'attribute'=>'visible', 
							'vAlign'=>'middle',
						],
            // 'sequence',
            // 'note',
             'slug',
            // 'title',
            // 'description',
            // 'keywords',
            // 'go_to_type',
            // 'external_link',
            // 'external_link_type',
            // 'removable',
            // 'image',
						[
							'class'=>'kartik\grid\BooleanColumn',
							'attribute'=>'present', 
							'vAlign'=>'middle',
						],
						[
							'class'=>'kartik\grid\BooleanColumn',
							'attribute'=>'box', 
							'vAlign'=>'middle',
						],
						[
							'class'=>'kartik\grid\BooleanColumn',
							'attribute'=>'textbox', 
							'vAlign'=>'middle',
						],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
