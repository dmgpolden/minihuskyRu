<?php

use common\modules\page\models\Page;
use common\modules\page\models\Status;
use dosamigos\fileupload\FileUploadUI;
use vova07\imperavi\Widget;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\ActiveForm as ActiveForm2;
/* @var $this View */
/* @var $model Page */
/* @var $form ActiveForm2 */
// vd(\Yii::getAlias('@static/temp'));
?>

<div class="page-form">

<?php
         $form = ActiveForm::begin([
//      'type' => ActiveForm::TYPE_INLINE,
//                          'fieldConfig' => ['autoPlaceholder'=>true],
                                ]);
                                ?>
<?php //vd($model->image ) ; ?>
<?php
    if( $model->image )
        echo Html::img( '@tmb'. $model->image->name ) ; ?>

<?php
    echo $form->field($model, 'parent_id')->dropDownList([0=>'---']+Page::options());
?>
<?php
    echo $form->field($model, 'status')->dropDownList([0=>'---']+Status::options());
?>
    <?= $form->field($model, 'title')->textInput(['maxlength' => 255]) ?>
    <?= $form->field($model, 'slug')->textInput(['maxlength' => 255]) ?>

    <div class="form-group">
    <?= $form->field($model, 'caption',['options'=>['class'=>'col-lg-6']])->textInput(['maxlength' => 255]) ?>
    <?= $form->field($model, 'note',['options'=>['class'=>'col-lg-6']])->textInput(['maxlength' => 200]) ?>
    </div>

<?php echo $form->field($model, 'content')->textarea(['rows' => 6]) ;
    echo Widget::widget([
        'selector' => '#page-content',
        'model'=>$model,
         'attribute'=>'content',
        'settings' => [
        'lang' => 'ru',
        'minHeight' => 200,
        'buttonSource' => true,
        'replaceDivs' => false,
        'formatting' => ['p', 'blockquote', 'pre', 'h1', 'h2', 'h3', 'h4', 'h5'],
        'pastePlainText' => true,
            'plugins' => [
            //'extimgupl',
            'imagemanager',
        //    'clips',
//            'fullscreen'
            ],
'thumbLinkClass'=>'athumbnail', //Класс по-умолчанию для ссылки на полное изображение вокруг thumbnail
'thumbClass'=>'thumbnail pull-left', //Класс по-умолчанию для  thumbnail
'defaultUplthumb'=>true, //Вставлять по-умолчанию после загрузки превью? если нет - 
        'imageManagerJson' => Url::to(['images-get']),
        'imageUpload' => Url::to(['image-upload']),
        ]
    ]);
?>

    <div class="form-group">
    <?= $form->field($model, 'visible',['options'=>['class'=>'col-lg-3']])->checkbox() ?>
    <?= $form->field($model, 'present',['options'=>['class'=>'col-lg-3']])->checkbox() ?>
    <?= $form->field($model, 'box',['options'=>['class'=>'col-lg-3']])->checkbox() ?>
    <?= $form->field($model, 'textbox',['options'=>['class'=>'col-lg-3'],])->checkbox() ?>
    </div>

        <?=Yii::t( 'page','Tags')?>
    <?= $form->field($model, 't_title')->textInput(['maxlength' => 255]) ?>
    <?= $form->field($model, 'description')->textInput(['maxlength' => 255]) ?>
    <?= $form->field($model, 'keywords')->textInput(['maxlength' => 255]) ?>

    <?php //= $form->field($model, 'go_to_type')->textInput() ?>

    <?php //= $form->field($model, 'external_link')->textInput(['maxlength' => 255]) ?>

    <?php //= $form->field($model, 'external_link_type')->textInput() ?>

    <?php //= $form->field($model, 'removable')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>


<?php

$js=<<<JS
 $('.delImage').on('click', function () {
    if ( $(this).is(':checked')){
       $('#image_'+this.value).animate({
           opacity: 0.4,})
        }
    else {
       $('#image_'+this.value).animate({
           opacity: 1,})
        }
   });

JS;

$this->registerJs($js) ;
?>
<?php
    if ($model->isNewRecord ){
        echo 'Загрузка картинок будет доступна после создания записи';
    } else { ?>
<div class="row">
<?php
if ( $im = $model->image ) : ?>
        <div class='thumbnail col-xs-2'>
            <?=Html::img( '@p'. $im->name, ['id'=>'image_'.$im->id, 
                'class'=>'img-thumbnail img-responsive'] );?>
    <?= $form->field($model, 'image_id')->checkbox(['class'=>'btn btn-danger ','value'=>$im->id]) ?>
        </div>    
<?php endif; ?>
</div>

<div class="row" style="display: flex; flex-wrap: wrap;" >
<?php
    if ( $model->images ){
        foreach( $model->images as $im ) {?>
        <div class='thumbnail col-xs-2' style="height: 200px;">
            <div style="height: 110px">
            <?=Html::img( '@p'. $im->name, ['id'=>'image_'.$im->id, 
            'class'=>'img-thumbnail img-responsive'] );?>
            </div>
    <?= $form->field($model, 'delImage[]')->checkbox(['class'=>'btn btn-danger delImage','value'=>$im->id,'uncheck'=>null,'id'=>'delImage'.$im->id]) ?>
    <?= $form->field($model, 'add_image_id')->radio(['class'=>'btn btn-danger ','value'=>$im->id,'uncheck'=>null]) ?>
        </div>    
<?php
        } 
    }
?>
</div>


<?= FileUploadUI::widget([
        'name' => 'file',       
        'url' => ['uploadTemp', 'id' => $model->id],
        'gallery' => true,
        'fieldOptions' => [
           'accept' => 'image/*'
        ],
            'clientOptions' => [
            'maxFileSize' => 3*1024*1024
        ],
        'downloadTemplateView'=>'@common/extensions/fileapi/views/download',
]);


    }
?>
<?php $form->end(); ?>
</div>
