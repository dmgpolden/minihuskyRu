<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\modules\page\models\Page;

/* @var $this yii\web\View */
/* @var $model common\modules\page\models\Page */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Pages', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="page-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'shop',
            ],
        ]) ?>
    </p>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'root',
            'lft',
            'rgt',
            'level',
            'name',
            'caption',
            'content:ntext',
            'visible',
            'sequence',
            'note',
            'slug',
            'title',
            'description',
            'keywords',
            'go_to_type',
            'external_link',
            'external_link_type',
            'removable',
            'image',
            'present',
            'box',
            'textbox',
        ],
    ]) ?>

</div>
<? foreach( $model->getImages() as $im ) { ?>
<?		$items[] = 
    [
        'url' => 'http://static.test.net.pol-den.ru/big'. $im['filepath'],
        'src' => 'http://static.test.net.pol-den.ru/tmb'. $im['filepath'],
        'options' => array('title' =>  @$model['title'])
    ];
} ?>

<?=( is_array( @$items) ) ?  dosamigos\gallery\Gallery::widget(['items' => $items]) : '' ;?>

