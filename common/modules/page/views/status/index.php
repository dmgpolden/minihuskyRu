<?php

use common\modules\page\models\Status;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\Pjax;
/* @var $this View */
/* @var $dataProvider ActiveDataProvider */

$this->title = 'Statuses';
$this->params['breadcrumbs'][] = $this->title;

$statusArray=Status::positionOptions();
?>
<div class="status-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Status', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'file',
                'value' => function($model){
                    if( null !== $model->file ){
                        return '/p/' . $model->file;
                    } else {
                        return '-';
                    }
                },
                'format' => 'image',
            ],
            'name',
            'slug',
            [
              'attribute' => 'position',
              'value' => function($model) use ($statusArray) {
                    if( null !== $model->position){
                        return $statusArray[$model->position];
                    } else {
                        return '-';
                    }
                },
            ],
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
