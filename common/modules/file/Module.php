<?php

namespace common\modules\file;

use Yii;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'common\modules\file\controllers';

		
			public $imageAllowedMimeTypes;
			public $imageHeight;
			public $imageMaxHeight;
			public $imageWidth;
			public $imageMaxWidth;
			public $imageMaxSize;

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }

		public function imageTempPath( $image = null)
		   {
		     $path = '@static/temp/';
		     if ( $image !== null) {
		       $path .= '/' . $image;
		     }
		     return Yii::getAlias( $path);
		   }
					 
}
