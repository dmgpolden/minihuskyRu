<?php

namespace common\modules\file\models;

use Yii;
use yii\validators\UniqueValidator;

/**
 * This is the model class for table "item_file".
 *
 * @property integer $id
 * @property string $filename
 * @property string $title
 * @property string $description
 * @property string $datetime
 */
class File extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%file}}';
    }
    
    public static function find()
    {
        return parent::find()->select(['*', 'CONCAT(  "/",name,".",ext) as name' ]);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name','ext'], 'required'],
            [['size'], 'integer'],
            [['name'], 'unique'],
            [['description'], 'string'],
            [['datetime'], 'safe'],
            [['name','ext','type', 'title'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Filename',
            'title' => 'Title',
            'description' => 'Description',
            'datetime' => 'Datetime',
        ];
    }

    public function getItems()
        {
            return $this->hasMany(Item::className(), ['id' => 'item_id'])
                ->viaTable('order_item', ['order_id' => 'id']);
        }

}
