<?php

namespace common\modules\file\models;

use Yii;
use yii\validators\UniqueValidator;

/**
 * This is the model class for table "item_file".
 *
 * @property integer $id
 * @property integer $item_type
 * @property integer $item_id
 * @property integer $file_id
 */
class ItemFile extends \yii\db\ActiveRecord
{
		const ITEM_POST = 0 ;
		const ITEM_MENU = 1 ;
		const ITEM_NEWS = 2 ;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%item_file}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['item_type', 'item_id', 'item_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'item_type' => 'Item Type',
            'item_id' => 'Item ID',
            'file_id' => 'File ID',
        ];
    }
}
