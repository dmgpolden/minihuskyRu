<?php

namespace common\modules\file\controllers;

use yii\web\Controller;
use common\modules\shop\models\Shop;
use yii\web\UploadedFile;
use common\modules\file\Upload;
use common\modules\file\models\File;
use common\modules\file\models\ItemFile;
use common\extensions\fileapi\actions\UploadAction;

class DefaultController extends Controller
{

    public function actions()
    {
        return [
          'uploadTemp' => [
              'class' => UploadAction::className(),
              'path' => \Yii::getAlias('@static/image'),
              'item' => 'common\modules\post\models\Post',
              'types' => ['jpg', 'png', 'gif'],
              'minHeight' => 100,
              'maxHeight' => 2000,
              'minWidth' => 100,
              'maxWidth' => 3000,
              'maxSize' => 3145728 // 3*1024*1024 = 3MB
          ],
          ];
    }

}
