<?php

namespace common\modules\post;

use yii\base\BootstrapInterface;

class Module extends \yii\base\Module implements BootstrapInterface
{
    public $controllerNamespace = 'common\modules\post\controllers';

    public function bootstrap($app)
    {
        \Yii::$app->get('i18n')->translations['post*'] = [
            'class' => 'yii\i18n\PhpMessageSource',
            'basePath' => __DIR__ . '/messages',
        ];
//        vd($this->id,false);
        $categories=\common\modules\post\models\Category::find()->where(['lft'=>1])->all();
        foreach($categories as $category){
            $app->get('i18n')->translations[$category->slug.'*'] = [
                'class'    => 'yii\i18n\PhpMessageSource',
                'basePath' => __DIR__ . '/messages',
            ];
            
            $this->controllerMap[$category->slug]='common\modules\post\controllers\DefaultController';
            $app->getUrlManager()->addRules([
            $category->slug=>$this->id. '/'. $category->slug . '/index',
            ], false);
/*            $viewDir=\Yii::getAlias('@common').'/modules/post/views/'.$category->slug;
        if(!file_exists($viewDir)){
            vd($category->slug,false);
            \yii\helpers\FileHelper::createDirectory($viewDir);
        }
*/

        }
    }

}
