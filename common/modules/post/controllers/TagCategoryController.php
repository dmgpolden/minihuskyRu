<?php

namespace common\modules\post\controllers;

use common\modules\post\models\Post;
use Yii;
use Zelenin\yii\modules\Tag\controllers\DefaultController;

class TagCategoryController extends DefaultController
{
   public $viewsPath = '@common/modules/post/views/tag-category';
   public function init()
    {
        $this->modelClass = Post::className();
        $this->modelAttribute = 'tag';
        $this->entityName = 'Tags';
        parent::init();
//        vd($this);
    }  
}
