<?php

namespace common\modules\post\controllers;

use common\extensions\fileapi\actions\UploadAction;
use common\modules\file\models\ItemFile;
use common\modules\file\Upload;
use common\modules\post\models\Category;
use common\modules\post\models\Post;
use common\modules\post\models\PostSearch;
use vova07\imperavi\actions\GetImagesAction;
use Yii;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * PostController implements the CRUD actions for Post model.
 */
class DefaultController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Post models.
     * @return mixed
     */
    public function actionIndex()
    {   
        //vd(Yii::$app->request->get(),false);
        $searchModel = new PostSearch();
        $category=Category::find()->where(['slug'=>$this->id])->one();
        
        $dataProvider = $searchModel->search(
            [
                'PostSearch'=>array_merge(
                key_exists('PostSearch', Yii::$app->request->queryParams) ? Yii::$app->request->queryParams['PostSearch'] : [],
                    ['category_id'=>$category->id]
                )
            ]
        );

            $view=\Yii::getAlias('@common').'/modules/post/views/'.$category->slug.'/index';
        if(!file_exists($view)){
            $view = '@common/modules/post/views/default/index';
        }

        return $this->render($view, [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'category' => $category,
        ]);
    }

    /**
     * Displays a single Post model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
            $model = $this->findModel($id);
            
            $view=\Yii::getAlias('@common').'/modules/post/views/'.$model->category->slug.'/view';
        if(!file_exists($view)){
            $view = '@common/modules/post/views/default/view';
        }

        return $this->render($view, [
            'model' => $model,
        ]);
    }

    /**
     * Creates a new Post model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $root=Category::find()->where(['slug'=>$this->id])->one();

        $model = new Post();
        $model->load(Yii::$app->request->post());
        if ($model->load(Yii::$app->request->post()) &&  $model->validate() ) {
                $model->save();
                return $this->redirect(['update','id'=>$model->id]);
            } else {
//            $view=\Yii::getAlias('@common').'/modules/post/views/'.$model->category->slug.'/create';
//        if(!file_exists($view)){
            $view = '@common/modules/post/views/default/create';
//        }
                return $this->render($view, [
                    'model' => $model,
                    'root'=>$root,
                ]);
            }
    }

    /**
     * Updates an existing Post model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $root=Category::find()->where(['slug'=>$this->id])->one();
        $model = $this->findModel($id);
            if ($model->load(Yii::$app->request->post())) {
//            vd (Yii::$app->request->post());
                 if ( $model->validate( ) ) {
                        $model->save();
                        return $this->redirect(['index']);
                 }
            }
            $view=\Yii::getAlias('@common').'/modules/post/views/'.$model->category->slug.'/update';
        if(!file_exists($view)){
            $view = '@common/modules/post/views/default/update';
        }
         return $this->render($view, [
                   'model' => $model,
                    'root'=>$root,
                ]);
    }

    public function Process($model_id,$images)
    {
        foreach( $images as $im ) {
            $file = Upload::upload( $im );
         vd( $file->errors,false);
                  Upload::FiletoItem($file->id, ItemFile::ITEM_NEWS, $model_id);
        }
//                        vd( $model);
    }

    /**
     * Deletes an existing Post model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        if (Yii::$app->getRequest()->isAjax){
            return $this->renderAjax(['index']);
        }else{
            return $this->redirect(['index']);
        }
    }

    /**
     * Finds the Post model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Post the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Post::find()->where([Post::tableName().'.id'=>$id])->joinWith('category')->one()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actions()
    {
        return [
          'uploadTemp' => [
              'class' => UploadAction::className(),
              'path' => \Yii::getAlias('@static/image'),
              'item' => 'common\modules\post\models\Post',
              'types' => ['jpg', 'png', 'gif'],
              'minHeight' => 100,
              'maxHeight' => 5000,
              'minWidth' => 100,
              'maxWidth' => 5000,
              'maxSize' => 3145728 // 3*1024*1024 = 3MB
            ],
            'images-get' => [
                'class' => 'vova07\imperavi\actions\GetImagesAction',
                'url' =>\Yii::getAlias('@big/'), // Directory URL address, where files are stored.
                'path' => '@static/image', // Or absolute path to directory where files are stored.
            ],
            'image-upload' => [                                                                                                                 'class' => 'vova07\imperavi\actions\UploadAction',                                                                               'url' =>\Yii::getAlias('@big/'), // Directory URL address, where files are stored.
               'path' => '@static/image', // Or absolute path to directory where files are stored.
            ],   
       ];
    }

}
