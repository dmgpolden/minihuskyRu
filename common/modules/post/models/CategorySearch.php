<?php

namespace common\modules\post\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\modules\post\models\Category;

/**
 * CategorySearch represents the model behind the search form about `app\models\Category`.
 */
class CategorySearch extends Category
{
    public $id;
    public $root;
    public $lft;
    public $rgt;
    public $depth;
    public $title;
    public $description;

    public function rules()
    {
        return [
            [['id', 'root', 'lft', 'rgt', 'depth'], 'integer'],
            [['title', 'description'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'root' => Yii::t('app', 'Root'),
            'lft' => Yii::t('app', 'Lft'),
            'rgt' => Yii::t('app', 'Rgt'),
            'depth' => Yii::t('app', 'Level'),
            'title' => Yii::t('app', 'Title'),
            'description' => Yii::t('app', 'Description'),
        ];
    }

    public function search($params)
    {
        $query = Category::findFull();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        if($this->root)
           $query->andWhere([self::tableName().'.root'=>$this->root]);
//        $this->addCondition($query, 'root');
        $this->addCondition($query, 'lft');
        $this->addCondition($query, 'rgt');
        $this->addCondition($query, 'depth');
        $this->addCondition($query, 'title', true);
        $this->addCondition($query, 'description', true);
        return $dataProvider;
    }

    protected function addCondition($query, $attribute, $partialMatch = false)
    {
        if (($pos = strrpos($attribute, '.')) !== false) {
            $modelAttribute = substr($attribute, $pos + 1);
        } else {
            $modelAttribute = $attribute;
        }

        $value = $this->$modelAttribute;
        if (trim($value) === '') {
            return;
        }
        if ($partialMatch) {
            $query->andWhere(['like', $attribute, $value]);
        } else {
            $query->andWhere([$attribute => $value]);
        }
    }
}
