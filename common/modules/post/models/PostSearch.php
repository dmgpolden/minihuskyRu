<?php

namespace common\modules\post\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\modules\post\models\Post;

/**
 * PostSearch represents the model behind the search form about `common\modules\post\models\Post`.
 */
class PostSearch extends Post
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category_id', 'visible', 'sequence', 'go_to_type', 'external_link_type', 'removable', 'image', 'present', 'box', 'textbox'], 'integer'],
            [['title', 'caption', 'content', 'note', 'slug', 'title', 'description', 'keywords', 'external_link'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {   
            if(isset($params['category_id']))
                $this->category_id=$params['category_id'];

        $query = Post::find()->joinWith('category')
            ->orderBy('id DESC');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }
        
//  vd($params);
//  $query->andWhere([self:: tableName().'.visible'=>1]);
        
        $query->andFilterWhere([
            self:: tableName().'.id' => $this->id,
            self:: tableName().'.category_id' => $this->category_id,
            'root' => $this->root,
            self:: tableName().'.visible' => $this->visible,
            'sequence' => $this->sequence,
            'go_to_type' => $this->go_to_type,
            'external_link_type' => $this->external_link_type,
            self:: tableName().'.removable' => $this->removable,
            self:: tableName().'.image' => $this->image,
            self:: tableName().'.present' => $this->present,
            self:: tableName().'.box' => $this->box,
            self:: tableName().'.textbox' => $this->textbox,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'caption', $this->caption])
            ->andFilterWhere(['like', 'content', $this->content])
            ->andFilterWhere(['like', 'note', $this->note])
            ->andFilterWhere(['like', 'slug', $this->slug])
            ->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'keywords', $this->keywords])
            ->andFilterWhere(['like', 'external_link', $this->external_link]);

        return $dataProvider;
    }
}
