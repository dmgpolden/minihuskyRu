<?php

namespace common\modules\post\models;

use Yii;
use yii\helpers\ArrayHelper;
use \yii\db\Query;
use \yii\helpers\Html;
use \yii2\web\UploadedFile;
use yii\behaviors\SluggableBehavior;
use common\modules\file\models\ItemFile;
use common\modules\file\models\File;
//use yii\sphinx\;

/**
 * This is the model class for table "post".
 *
 * @property integer $id
 * @property integer $category_id
 * @property integer $root
 * @property integer $lft
 * @property integer $rgt
 * @property integer $depth
 * @property string $title
 * @property string $caption
 * @property string $content
 * @property integer $visible
 * @property integer $sequence
 * @property string $note
 * @property string $slug
 * @property string $title
 * @property string $description
 * @property string $keywords
 * @property integer $go_to_type
 * @property string $external_link
 * @property integer $external_link_type
 * @property integer $removable
 * @property integer $image_id
 * @property integer $present
 * @property integer $box
 * @property integer $textbox
 */
class Post extends \yii\db\ActiveRecord
{
    public $filepath;        
    public $delImage;
    public $add_image_id;
    public $del_image_id;
    public $parent_name;
    public $root;
    /**
     * @inheritdoc
     */
    public static function className()
    {
        return 'common\modules\post\models\Post';
    }
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{post}}';
    }
    
    public static function tablePostFile()
    {
        return 'post_file';
    }
    
    function beforeSave($insert)
    {
       if($this->add_image_id){
        $this->image_id=$this->add_image_id;
       }
        return parent::beforeSave($insert);
    }

    function afterSave($insert, $changedAttributes)
    {
        $cache = \Yii::$app->cache;
         \yii\caching\TagDependency::invalidate($cache,['post']);
        
       if($this->add_image_id){
            \Yii::$app->db->createCommand()
            ->delete(Post::tablePostFile(),
            ['post_id'=>$this->id,
            'file_id'=>$this->add_image_id
            ])
            ->execute();
       }

       if (is_array($this->delImage))     {
            $this->unlinkFiles();
       }
       
       if(array_key_exists('sphinx',Yii::$app->components)){
           if($insert){
                Yii::$app->sphinx->createCommand()
                    ->insert('{{%post}}', [
                        'content' => $this->content,
                        'id' => $this->id,
                        'visible' => $this->visible,
                     ])->execute();
           } else {
                Yii::$app->sphinx->createCommand()
                    ->replace('{{%post}}', [
                        'id' => $this->id,
                        'visible' => $this->visible,
                        'content' => $this->content,
                     ])->execute();
                
           }
       }
    }
    
    function afterDelete()
    {
            $this->unlinkPostFiles();
            
            $cache = \Yii::$app->cache;
         \yii\caching\TagDependency::invalidate($cache,['post']);

                Yii::$app->sphinx->createCommand()
                    ->delete('{{%post}}', [
                        'id' => $this->id
                     ])->execute();
    }
        
        
    private function unlinkFiles()
    {   
            \Yii::$app->db->createCommand()
            ->delete(Post::tablePostFile(),
            ['post_id'=>$this->id,
            'file_id'=>$this->delImage
            ])
            ->execute();
    }

    private function unlinkPostFiles()
    {   
            \Yii::$app->db->createCommand()
            ->delete(Post::tablePostFile(),
            ['post_id'=>$this->id,
            ])
            ->execute();
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category_id', 'visible', 'sequence', 'go_to_type', 'external_link_type', 'removable', 'present', 'box', 'textbox'], 'integer'],
            [[ 'delImage','add_image_id'], 'safe'],
            [['content'], 'string'],
            [['category_id','title'], 'required'],
            [['title', 'caption', 'slug','t_title', 'description', 'keywords', 'external_link'], 'string', 'max' => 255],
            [['note'], 'string', 'max' => 200],
            [['create_at'], 'date','format'=>'yyyy-mm-dd'],
            [['image_id'], 'integer'],

        ];
    }
    
    public function getCategory()
    {
            return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }

    public function getImage()
    {
            return $this->hasOne(File::className(), ['id' => 'image_id']);
    }

    public function getImages()
    {
            return $this->hasMany(File::className(), ['id' => 'file_id'])
                ->viaTable('post_file', ['post_id' => 'id']);
//                ->asArray();
    }

    public function getFiles()
    {
            return $this->hasMany(File::className(), ['id' => 'file_id'])
                ->viaTable('post_file', ['post_id' => 'id']);
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'title' =>  Yii::t('post','Title'),
            'caption' => Yii::t('post','Caption'),
            'create_at' => Yii::t('post','Create at'),
            'content' => Yii::t('post','Content'),
            'visible' => Yii::t('post','Visible'),
            'sequence' => Yii::t('post','Sequence'),
            'note' => Yii::t('post','Note'),
            'slug' => Yii::t('post','Slug'),
            'title' => Yii::t('post','Title'),
            'description' => Yii::t('post','Description'),
            'keywords' => Yii::t('post','Keywords'),
            'go_to_type' => 'Go To Type',
            'external_link' => 'External Link',
            'external_link_type' => 'External Link Type',
            'removable' => 'Removable',
            'present' => Yii::t('post','Present'),
            'box' => Yii::t('post','Box'),
            'textbox' => Yii::t('post','Textbox'),
            'image_id' => Yii::t('post','титульная картинка'),
            'add_image_id' => Yii::t('post','Сделать титульной'),

            'category_id' => Yii::t('post','Category'),
            'delImage' => Yii::t('post','к удалению'),
        ];
    }
    public function behaviors() {
        return [
            ['class' => SluggableBehavior::className(),
              'attribute' => 'title',
              'slugAttribute' => 'slug',
               'ensureUnique' => true
            ],  
        ];
    }

}
