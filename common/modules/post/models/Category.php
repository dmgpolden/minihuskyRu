<?php

namespace common\modules\post\models;

use yii\db\ActiveQuery;
use \yii\db\Query;
use \yii\helpers\ArrayHelper;
use yii\behaviors\SluggableBehavior;

/**
 * This is the model class for table "tbl_category".
 *
 * @property string $id
 * @property string $tree
 * @property string $lft
 * @property string $rgt
 * @property integer $depth
 * @property string $title
 * @property string $description
 */
class Category extends \yii\db\ActiveRecord {

    public $parent_name;
    public $next;
    public $prev;
    /**
     * @inheritdoc
     */


    public static function tableName() {
        return '{{%post_category}}';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
                [ [ 'tree', 'lft', 'rgt', 'depth' ], 'integer' ],
                [ [ 'title' ], 'required' ],
                [ [ 'slug' ], 'string' ],
                [ [ 'description','keywords', 't_title' ], 'string' ],
                [ [ 'title' ], 'string', 'max' => 255 ],
                [['parent_id'], 'safe']
        ];
    }
    
    function afterSave($insert, $changedAttributes)
    {
            $cache = \Yii::$app->cache;
            \yii\caching\TagDependency::invalidate($cache,['post']);
    }

    public static function findFullOne($id)
    {
        $cat=self::tableName();
            $primaryKey = parent::primaryKey();
            if (isset($primaryKey[0])) {
                $condition = [$cat.'.'.$primaryKey[0] => $id];
            } else {
                throw new InvalidConfigException(get_called_class() . ' must have a primary key.');
            }
        $query = parent::find($id)
            ->select($cat.'.*,prev.id as prev, next.id as next, parent.id as parent,parent.title as parent_name')
            ->leftJoin($cat.' parent', $cat.'.lft > parent.lft AND '. $cat.'.rgt<parent.rgt AND '. $cat.'.tree=parent.tree')
            ->leftJoin($cat.' prev', $cat.'.lft-1 = prev.rgt AND '. $cat.'.tree=prev.tree')
            ->leftJoin($cat.' next', $cat.'.rgt+1 = next.lft AND '. $cat.'.tree=next.tree')
            ->andWhere($condition);
        return $query->one();
    }

    public static function findFull()
    {
        $cat=self::tableName();
        $query = parent::find()
            ->select($cat.'.*,prev.id as prev, next.id as next, parent.id as parent,parent.title as parent_name')
            ->leftJoin($cat.' parent', $cat.'.lft > parent.lft AND '. $cat.'.rgt<parent.rgt AND '
                            . $cat.'.tree=parent.tree AND '
                            . $cat.'.depth-1=parent.depth'
                            )

//             ->leftJoin($cat.' parent',['and',['>',$cat.'.lft','parent'.'.'.'lft'],['<',$cat.'.rgt','parent.rgt'],
//                                            [$cat.'.tree'=>'parent.tree'],/*['parent.depth'=>$cat.'.depth-1']*/])
            ->leftJoin($cat.' prev', $cat.'.lft-1 = prev.rgt AND '. $cat.'.tree=prev.tree')
            ->leftJoin($cat.' next', $cat.'.rgt+1 = next.lft AND '. $cat.'.tree=next.tree')
            ->orderBy( [ 'tree' => SORT_ASC, 'lft' => SORT_ASC]);
        return $query;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
                'id' => \Yii::t ( 'app', 'ID' ),
                'tree' => \Yii::t ( 'app', 'Root' ),
                'lft' => \Yii::t ( 'app', 'Lft' ),
                'rgt' => \Yii::t ( 'app', 'Rgt' ),
                'depth' => \Yii::t ( 'app', 'depth' ),
                'title' => \Yii::t ( 'app', 'Title' ),
                'description' => \Yii::t ( 'app', 'Description' )
        ];
    }
    /**
     * Configure extra behaviors
     *
     * @see \yii\base\Component::behaviors()
     */
    public function behaviors() {
        return [
                'slug' => [
                    'class' => 'Zelenin\yii\behaviors\Slug',
                        'attribute' => 'title',
                        'slugAttribute' => 'slug',
                        'ensureUnique' => true,
                        'replacement' => '-',
                        'lowercase' => true,
                        'transliterateOptions' => 'Russian-Latin/BGN;'
                        ],
                    'nestedSet' => [
                        'class' => \creocoder\nestedsets\NestedSetsBehavior::className(),
                        'treeAttribute' => 'tree',
                ]
        ];
    }

    public static function trees()
    {
            $q=new Query();
        return $query = $q->andWhere("lft = 1");
    }
    
    public static function options($tree=false){
                $q = self::find()
                ->select(['id,CONCAT(REPEAT("-",depth),title) as text'])
                ->from(self::tableName())
                ->orderBy(['tree'=>SORT_ASC,'lft'=>SORT_ASC])
                ;
                  if ( is_int($tree) ){
                      $q->andWhere(['tree'=>$tree]);
                  }
/*                $c=$q->createCommand();
                vd($c->params,false);
                vd($c->sql,false);
                vd($c->queryAll( ),false)
                */
              $res= $q->asArray()->all();
        return ArrayHelper::map($res,'id','text');
    }


    public static function listoption(){
                $query = (new Query())
                ->select(['id as value,CONCAT(REPEAT("-",depth),title) as text'])
                ->from(self::tableName())
                ->orderBy(['tree'=>SORT_ASC,'lft'=>SORT_ASC])
                ;
    $command=$query->createCommand();
    $command->fetchMode=[\PDO::FETCH_ASSOC];
      $res = $command->queryAll( );
        return $res;
    }

}
