<?php

namespace common\modules\post\models;

use Yii;
use creocoder\behaviors\NestedSetQuery;

class PostQuery extends \yii\db\ActiveQuery
{
    public function behaviors() {
        return [
            [
                'class' => NestedSetQuery::className(),
            ],
        ];
    }
}

