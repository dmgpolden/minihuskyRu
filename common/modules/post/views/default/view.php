<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;
use frontend\modules\post\Module;
use kartik\datecontrol\DateControl;

/* @var $this yii\web\View */
/* @var $model frontend\modules\news\models\Shop */
$this->registerMetaTag(['name' => 'keywords', 'content' => $model->keywords]);
$this->registerMetaTag(['name' => 'description', 'content' => $model->description]);

$this->title = $model->title;
?>
<div class="news-view">

    <h1><?= Html::encode($this->title) ?></h1>
<div>
<?=  Yii::$app->formatter->asDate( $model->create_at, Yii::$app->modules['datecontrol']['displaySettings']['date']) ?>
</div>
  <?php /* preview for news
  if($model->image != null ) {  
     echo Html::img('@p'.$model->image->name,['class'=>'img-thumbnail img-responsive']); 
  } */ ?>
<div>
<?= $model->content ?>
</div>

  
    <div class="album row" >
<?php       if($images = $model->getImages()->all() ) {
               foreach($images as $im ) {?>
               <div class="col-lg-6 col-md-6 col-sm-8 col-xs-12 gallery-box"> <a href="<?=Url::to('@orig'.@$im->name)?>" class="gallery" tittle="<?=$model->title?>"><img src="<?=Url::to('@mdl'.@$im->name)?>" alt="<?=$model->title?>" class="img-thumbnail"/></a> 
          <? // 'thumbnail col-lg-3 col-md-3 col-sm-6 col-xs-6 gallery-box' ?></div>
<?php       } 
        } ?>
    </div>

</div>

<?php $this->registerJS('$("a.gallery").colorbox({rel:"gallery",width:"95%"})' );
