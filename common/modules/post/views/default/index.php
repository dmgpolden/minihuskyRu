<?php

use yii\helpers\Url;
use yii\helpers\Html;
//use yii\grid\GridView;
use \kartik\grid\GridView;
use common\modules\post\models\Post;
use common\modules\post\models\Category;
use kartik\dropdown\DropdownX;

/* @var $this yii\web\View */
/* @var $searchModel common\modules\post\models\postSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title =  Yii::t($category->slug,ucfirst($category->slug));
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="post-index">

<?// =Post::getPostPage( ) ?>
    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('post','Create'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php \yii\widgets\Pjax::begin(); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'export' => false,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'title',
/*            ['value'=>'category.title',
            'attribute'=>'category_id',
            'filter'=>Category::options(),
            ],
*/            
            'create_at',
                [
                    'class' => '\kartik\grid\BooleanColumn',
                    'attribute'=>'visible', 
                ],
             'caption:ntext',
            // 'visible',
            // 'sequence',
            // 'note',
                        // 'slug',
            // 'title',
            // 'description',
            // 'keywords',
            // 'go_to_type',
            // 'external_link',
            // 'external_link_type',
            // 'removable',
            // 'image',
            // 'present',
            // 'box',
            // 'textbox',

        [
            'class'=>'kartik\grid\BooleanColumn',
            'attribute'=>'box', 
            'vAlign'=>'middle',
        ],
        [
            'class'=>'kartik\grid\BooleanColumn',
            'attribute'=>'textbox', 
            'vAlign'=>'middle',
        ],


            ['class' => 'yii\grid\ActionColumn',
                'buttons' => [
                'view' => function ($url, $model,$index) {
                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', 'http://'. substr(Yii::$app->urlManager->hostInfo,13).'/'
                            . $model->category->slug.'/'.$model->id,
                        [
                        'target' => '_blank',
                        'title' => Yii::t('yii', 'View'),
                            'data-pjax' => '0',
                            ]
                            );
                        },
                ],
            ],
        ],
    ]); ?>
<?php  \yii\widgets\Pjax::end(); ?>

</div>

<?php
$js=<<<JS
jQuery.pjax.defaults.timeout = false;//IMPORTANT
//$.pjax.reload({container:'#w1'})
JS;

$this->registerJs( $js);

