<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\InputWidget;
use yii\widgets\ActiveForm;
//use kartik\widgets\ActiveForm;
use common\modules\post\models\Post;
use common\modules\post\models\Category;
use dosamigos\fileupload\FileUploadUI;
use dosamigos\fileupload\FileUpload;
use kartik\datecontrol\DateControl;
use kartik\switchinput\SwitchInput;
use vova07\imperavi\Widget;
use Zelenin\yii\modules\Tag\widgets\Tag;

//use mihaildev\ckeditor\CKEditor;
/* @var $this yii\web\View */
/* @var $model common\modules\post\models\Post */
/* @var $form yii\widgets\ActiveForm */
//vd($model);
?>

<div class="post-form">

    <?php $form = ActiveForm::begin([
                        //'layout' => 'horizontal', 
               //           'fieldConfig' => ['autoPlaceholder'=>true],
                        'options' => ['enctype'=>'multipart/form-data']]); ?>


        <?= $form->field($model, 'create_at')->widget(DateControl::classname(), [
'type'=>DateControl::FORMAT_DATE, 'pluginOptions'=>['autoclose'=>true]
]);    
?>
<div class="row">
    <?= $form->field($model, 'category_id')->dropDownList(["" => "--- Select Parent Category ---"] 
                                                                + Category::options($root->tree),
                            ['options' =>
                                [
                                    $root->id => ['selected ' => 'selected']
                                ]
                            ]
                            );?>
    <?= $form->field($model, 'title')->textInput(['maxlength' => 255]) ?>
    <?= $form->field($model, 'slug')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'caption')->textInput(['maxlength' => 255]) ?>

<?php
/*echo $form->field($model, 'content')->widget(CKEditor::className(),[
    'editorOptions' => [
        'preset' => 'full', //разработанны стандартные настройки basic, standard, full данную возможность не обязательно использовать
        'inline' => false, //по умолчанию false
    ],
]);*/
?>
<?php echo $form->field($model, 'content')->textarea(['rows' => 6]) ;
    echo \vova07\imperavi\Widget::widget([
        'selector' => '#post-content',
        'model'=>$model,
         'attribute'=>'content',
        'settings' => [
        'lang' => 'ru',
        'minHeight' => 200,
        'buttonSource' => true,
        'replaceDivs' => false,
        'pastePlainText' => true,
            'plugins' => [
            'extimgupl',
            'imagemanager',
        //    'clips',
//            'fullscreen'
            ],
'thumbLinkClass'=>'athumbnail', //Класс по-умолчанию для ссылки на полное изображение вокруг thumbnail
'thumbClass'=>'thumbnail pull-left', //Класс по-умолчанию для  thumbnail
'defaultUplthumb'=>true, //Вставлять по-умолчанию после загрузки превью? если нет - 
        'imageManagerJson' => Url::to(['images-get']),
        'imageUpload' => Url::to(['image-upload']),
        ]
    ]);
?>

    <?php //= $form->field($model, 'content')->textarea(['rows' => 6]) ?>
    <div class="form-group">
    <?= $form->field($model, 'visible',['options'=>['class'=>'col-lg-3']])->checkbox() ?>
    <?= $form->field($model, 'present',['options'=>['class'=>'col-lg-3']])->checkbox() ?>
    <?= $form->field($model, 'box',['options'=>['class'=>'col-lg-3']])->checkbox() ?>
    <?= $form->field($model, 'textbox',['options'=>['class'=>'col-lg-3'],])->checkbox() ?>
    </div>
        <?=Yii::t( 'post','Tags')?>
    <?= $form->field($model, 't_title')->textInput(['maxlength' => 255]) ?>
    <?= $form->field($model, 'description')->textInput(['maxlength' => 255]) ?>
    <?= $form->field($model, 'keywords')->textInput(['maxlength' => 255]) ?>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('post','Create') : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
<?php

     ActiveForm::end(); ?>

<?php

$js=<<<JS
 $('.delImage').on('click', function () {
    if ( $(this).is(':checked')){
       $('#image_'+this.value).animate({
           opacity: 0.4,})
        }
    else {
       $('#image_'+this.value).animate({
           opacity: 1,})
        }
   });

JS;

$this->registerJs($js) ;
?>
<?php
    if ($model->isNewRecord ){
        echo 'Загрузка картинок будет доступна после создания записи';
    } else { ?>
 <div class="row">
<?php
if ( $im = $model->image ) : ?>
        <div class='thumbnail col-xs-2'>
            <?=Html::img( '@p'. $im->name, ['id'=>'image_'.$im->id, 
                'class'=>'img-thumbnail img-responsive'] );?>
    <?= $form->field($model, 'image_id')->checkbox(['class'=>'btn btn-danger ','value'=>$im->id]) ?>
        </div>    
<?php endif; ?>
</div>       
<div class="row">
<?php
    if ( $model->images ){
        foreach( $model->images as $im ) {?>
        <div class='thumbnail col-xs-2'>
            <?=Html::img( '@p'. $im->name, ['id'=>'image_'.$im->id, 
                'class'=>'img-thumbnail img-responsive'] );?>
    <?= $form->field($model, 'delImage[]')->checkbox(['class'=>'btn btn-danger delImage','value'=>$im->id,'uncheck'=>null]) ?>
    <?= $form->field($model, 'add_image_id')->radio(['class'=>'btn btn-danger ','value'=>$im->id,'uncheck'=>null]) ?>
        </div>    
<?php
        } 
    }
?>
</div>
<?= FileUploadUI::widget([
        'name' => 'file',       
        'url' => ['uploadTemp', 'id' => $model->id],
        'gallery' => true,
        'fieldOptions' => [
           'accept' => 'image/*'
         ],
         'clientOptions' => [
            'maxFileSize' => 3145728,
         ],
]);?>

<?php
    }
?>
</div>
</div> 
