<?php

use common\modules\post\models\Category;
use  \yii\widgets\Pjax;
?>
<?php Pjax::begin( ); ?>	
<div id='cattree'>
<?= Category::getPage( ) ;?>
</div>
<?php Pjax::end( ); ?>

)
<?php $this->registerJS( "$( document).pjax( 'a', '#cattree')" ); ?>
