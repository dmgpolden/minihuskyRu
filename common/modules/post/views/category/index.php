<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\Pjax;
use common\modules\post\models\Category;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\CategorySearch $searchModel
 */
    Pjax::begin(['options' => ['class' => 'pjax-wraper']]);

$this->title = Yii::t('app', 'Categories');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="category-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php //  echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create {modelClass}', [
  'modelClass' => 'Category',
]), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

<?php
    $list=Category::listoption();
    //echo \dosamigos\grid\GroupGridView::widget([
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        //'mergeColumns'=>['root.slug','parent_name'],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            /*[
                'class' => \dosamigos\grid\EditableColumn::className(),
                'attribute' => 'parent',
                'value' => function ($data){return $data->parent_name;},
                'url' => 'updateparent',
                'type' => 'select2',
                'editableOptions' => [
                     'mode' => 'pop',
                    'pk' =>function ($data){return $data->id;} ,
                    'placement' => 'right',
                 'source' =>json_encode($list),
                  'placeholder'=>'Select a Parent',
                ],
            ],
            */
          //   'root',
            /* 
            'id',
             'lft',
             'rgt',
             'depth',
             'parent',
             'prev',
             'next',
             */
             //'parent_name',
            ['attribute' => 'title', 'value' => function($data){ return str_repeat('--', $data->depth) . ' ' . $data->title; }],
             'slug',
            'description:ntext',

            [
            'class' => 'yii\grid\ActionColumn',
                'template' => '{up} {down} {update} {delete} {view}',
                'buttons' => [
                'view' => function ($url, $model,$index) {
                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', 'http://'. substr(Yii::$app->urlManager->hostInfo,13).'/'.$model->slug,
                        [
                        'target' => '_blank',
                        'title' => Yii::t('yii', 'View'),
                            'data-pjax' => '0',
                            ]
                            );
                        },
                'up' => function ($url, $model,$index) {
                        return Html::a('<span class="glyphicon glyphicon-chevron-up"></span>', $url, [
                        'title' => Yii::t('yii', 'Move Up'),
                            'data-pjax' => '0',
                            ]);
                        },

                 'down' => function ($url, $model) {
                           return Html::a('<span class="glyphicon glyphicon-chevron-down"></span>', $url, [
                            'title' => Yii::t('yii', 'Move Down'),
                            'data-pjax' => '0',
                            'class' => 'grid-action',
                            ]);
                        },
                ],
            ],
        ],
    ]);

$js="$(function(){
    $('body').on('click', '.grid-action', function(e){
        var href = $(this).attr('href');
        var self = this;
        $.get(href, function(){
            var pjax_id = $(self).closest('.pjax-wraper').attr('id');
            $.pjax.reload('#' + pjax_id);
        });
        return false;
    })
})";
//$this->registerJS($js);

?>
</div>
<?php

    Pjax::end();
