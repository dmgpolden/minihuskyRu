<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\FileHelper;
use vova07\fileapi\behaviors\UploadBehavior;

/**
 * This is the model class for table "key_storage_item".
 *
 * @property integer $key
 * @property integer $value
 */
class KeyStorageItem extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%key_storage_item}}';
    }

    public function behaviors()
    {
        return [
            [
              'class' => TimestampBehavior::className(),
            ],
/*        'uploadBehavior' => [
            'class' => UploadBehavior::className(),
            'attributes' => [
                'value' => [
                    'tempPath' => '@static/temp',
                    'path' => '@static/favicon',
                    'url' => Yii::getAlias('@statwww/favicon'),
                ]
            ]
        ]*/
        ];

    }

    function afterSave($insert, $changedAttributes)
    {
      if(isset($changedAttributes['value'])  && $this->key == 'favicon' ){
         foreach ( FileHelper::findFiles(Yii::getAlias('@webroot/favicon'), ['except' => ['.gitignore']] ) as $file) {
               unlink($file) ;
        }
         foreach ( FileHelper::findFiles(Yii::getAlias('@frontend/web/favicon'), ['except' => ['.gitignore']]) as $file) {
               unlink($file) ;
        }
      }
            $cacheKey = sprintf('%s.%s', Yii::$app->keyStorage->cachePrefix, $this->key);
         \Yii::$app->cache->delete($cacheKey);
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['key'], 'required'],
            [['value'], 'required',
                'when'=>function($model){
                        return $model->key !=='favicon';
                        },
                'whenClient'=> "function (attribute, value) {
                                return $('#keystorageitem-key').val() !=='favicon';
                               }",
            ],
            [['key'], 'string', 'max'=>128],
            [['value', 'comment'], 'safe'],
            [['key'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'key' => Yii::t('common', 'Key'),
            'value' => Yii::t('common', 'Value'),
            'comment' => Yii::t('common', 'Comment'),
        ];
    }
}
