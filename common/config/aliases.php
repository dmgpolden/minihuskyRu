<?php
Yii::setAlias('bower', dirname(dirname(__DIR__)) . '/vendor/bower-asset');
Yii::setAlias('npm', dirname(dirname(__DIR__)) . '/vendor/npm-asset');
Yii::setAlias('common', dirname(__DIR__));
Yii::setAlias('frontend', dirname(dirname(__DIR__)) . '/frontend');
Yii::setAlias('backend', dirname(dirname(__DIR__)) . '/backend');
Yii::setAlias('console', dirname(dirname(__DIR__)) . '/console');
//Yii::setAlias('tests', '@frontend' . '/tests');
Yii::setAlias('tests', dirname(dirname(__DIR__)) . '/tests');
Yii::setAlias('static', dirname(dirname(__DIR__)) . '/static');
Yii::setAlias('host',(isset($_SERVER['SERVER_NAME']))?$_SERVER['SERVER_NAME']:'local');
Yii::setAlias('statwww', '');
//Yii::setAlias('statwww', 'http://static.'.str_replace('admin.','',Yii::getAlias('@host')));
Yii::setAlias('tmb',   '@statwww/tmb');
Yii::setAlias('big',   '@statwww/big');
Yii::setAlias('box',   '@statwww/box');
Yii::setAlias('mdl',   '@statwww/mdl');
Yii::setAlias('orig',   '@statwww/orig');
Yii::setAlias('p', '@statwww/p');
Yii::setAlias('sl',  '@statwww/sl');
Yii::setAlias('xxxlarge',  '@statwww/xxxlarge');
Yii::setAlias('xxlarge',  '@statwww/xxlarge');
Yii::setAlias('xlarge',  '@statwww/xlarge');
Yii::setAlias('large',  '@statwww/large');
Yii::setAlias('medium',  '@statwww/medium');
Yii::setAlias('small',  '@statwww/small');
Yii::setAlias('xsmall',  '@statwww/xsmall');
