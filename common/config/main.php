<?php
use \kartik\datecontrol\Module;
return [
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'id' => 'minihuskyRu',
    'components' => [
        'i18n' => [
            'translations' => [
                'app'=>[
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath'=>'@common/messages',
                ],
                '*'=> [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath'=>'@common/messages',
                    'fileMap'=>[
                        'common'=>'common.php',
                        'backend'=>'backend.php',
                        'frontend'=>'frontend.php',
                    ],
                ],
                'pet' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    //'basePath' => '@app/messages',
                    //'sourceLanguage' => 'en',
                    'fileMap' => [
                        'pet' => 'pet.php',
                        'pet/error' => 'error.php',
                    ],
                ],
            ],
        ],
        'keyStorage'=>[
            'class'=>'common\components\keyStorage\KeyStorage',
            'cachingDuration' => 3600,
        ],

    ], // components
'modules' => [
   'gridview' =>  [
        'class' => '\kartik\grid\Module'
    ],
				'utility' => [
						'class' => 'c006\utility\migration\Module',
				],
				'image' => [
				            'class' => 'common\modules\file\Module',
										'imageAllowedMimeTypes'=>'image/*',
						       ],
				'datecontrol' => [
				'class' => 'kartik\datecontrol\Module',
				 
				// format settings for displaying each date attribute
				'displaySettings' => [
				Module::FORMAT_DATE => 'php:d-m-Y',
				Module::FORMAT_TIME => 'H:i:s A',
				Module::FORMAT_DATETIME => 'd-m-Y H:i',
				],
				 
				// format settings for saving each date attribute
				'saveSettings' => [
				Module::FORMAT_DATE => 'php:Y-m-d', // saves as unix timestamp
				Module::FORMAT_TIME => 'php:H:i:s',
				Module::FORMAT_DATETIME => 'php:Y-m-d H:i:s',
				],
				 
				// automatically use kartik\widgets for each of the above formats
				'autoWidget' => true,
				 
				// default settings for each widget from kartik\widgets used when autoWidget is true
				'autoWidgetSettings' => [
				Module::FORMAT_DATE => ['type'=>2, 'pluginOptions'=>['autoClose'=>true]], // example
				Module::FORMAT_DATETIME => [], // setup if needed
				Module::FORMAT_TIME => [], // setup if needed
				],
				// custom widget settings that will be used to render the date input instead of kartik\widgets,
				// this will be used when autoWidget is set to false at module or widget level.
				'widgetSettings' => [
				Module::FORMAT_DATE => [
				'class' => 'yii\jui\DatePicker', // example
				'options' => [
				'options'=>['class'=>'form-control'],
				'clientOptions' => ['dateFormat' => 'dd-mm-yy'],
				]
				]
				]
				// other settings
				]

    ],
];
