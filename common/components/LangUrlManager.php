<?php
namespace common\components;

use common\components\MultilangHelper;
use yii\web\UrlManager;
use Yii;

class LangUrlManager extends UrlManager
{

    public function createUrl($params)
    {
			$url = parent::createUrl( $params);
			        return MultilangHelper::addLangToUrl( $url);	
		}

}
