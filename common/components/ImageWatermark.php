<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace common\components;

use Exception;
use Imagine\Gd\Imagine;
use Imagine\Image\Box;
use Imagine\Image\Point;

/**
 * Description of ImageWatermark
 * Create Watermark
 *
 * @author dmg
 */
class ImageWatermark {
    
    /*
     * @type string 
     * file Original
     */
    private $fileOriginal; 
    
    /*
     * @type string 
     * file Result
     */
    private $fileResult; 
    
    /*
     * @type string 
     * image Watermark
     */
    private $imageWatermark; 
    
    /**
     * @type string 
     * position Watermark
     * One of this: bottomRight, bottomLeft, topRight, topLeft
     **/
    private $positionWatermark; 
    
    /**
     * @param $fileOriginal Source filename with full path
     * @param $fileResult Target filename with full path
     * @param $imageWatermark Source watermark filename with full path
     * @param $positionWatermark position Watermark
     * One of this: bottomRight, bottomLeft, topRight, topLeft
    **/ 
    public function __construct($fileOriginal, $fileResult, $imageWatermark, $positionWatermark) {
        $this->imagine = new Imagine();
        $this->fileOriginal = $fileOriginal;        
        $this->fileResult = $fileResult;  
        $this->imageWatermark = $imageWatermark;
        if( !in_array($positionWatermark, ['bottomRight', 'bottomLeft', 'topRight', 'topLeft'])){
            throw new Exception('Error position: ' . $positionWatermark . ', must be one of this: bottomRight, bottomLeft, topRight, topLeft' ); 
        }
            $this->positionWatermark = $positionWatermark;
    }
    
    public function generateWatermark() {
        $image=$this->imagine->open($this->fileOriginal);
        $size = $image->getSize();
        $watermark=$this->imagine->open($this->imageWatermark)->thumbnail(new Box($size->getWidth()/3, $size->getHeight()/3));
        $wSize = $watermark->getSize();
        switch($this->positionWatermark):
            case 'bottomRight':
                $position = new Point($size->getWidth() - $wSize->getWidth(), $size->getHeight() - $wSize->getHeight());
                break;
            case 'bottomLeft':
                $position = new Point(0, $size->getHeight() - $wSize->getHeight());
                break;
            case 'topLeft':
                $position = new Point(0, 0);
                break;
            case 'topRight':
                $position = new Point($size->getWidth() - $wSize->getWidth(), 0);
                break;
        endswitch;
        
        $image->paste($watermark, $position);
        try {
            $image->save($this->fileResult);
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
        return true;
    }

}
