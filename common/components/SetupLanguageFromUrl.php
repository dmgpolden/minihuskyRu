<?php

namespace common\components;

use yii\base\Component;
use yii\base\BootstrapInterface;
use common\components\MultilangHelper;
use yii;


class SetupLanguageFromUrl extends Component  implements BootstrapInterface
{
    public function bootstrap($app)
    {
		  if( YII_ENV != 'test' ) {
         $pathInfo = MultilangHelper::processLangInUrl($app->getRequest()->getPathInfo());
			$app->getRequest()->setPathInfo($pathInfo);
			}
    }
} 
