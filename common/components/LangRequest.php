<?php
namespace common\components;

use Yii;
use yii\web\Request;
use common\components\MultilangHelper;


class LangRequest extends Request
{
    private $_requestUri;
 
    public function getPathInfo()
    {
//			vd( parent::getPathInfo( ));
        if ($this->_pathInfo === null)
            $this->_pathInfo = MultilangHelper::processLangInUrl(parent::getPathInfo());
        return $this->_pathInfo;
    }
 
}
