<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user common\models\User */

?>
            <?=$body='Анкета с сайта';?>
            <?=$body->getAttributeLabel('username').': '.$body->username;
            <?=$body->getAttributeLabel('old').': '.$body->old;?>
            <?=$body->getAttributeLabel('country').': '.$body->country;?>
            <?=$body->getAttributeLabel('city').': '.$body->city;?>
            <?=$body->getAttributeLabel('tel').': '.$body->tel;?>
            <?=$body->getAttributeLabel('email').': '.$body->email;?>
            <?=$body->getAttributeLabel('whereinfo').': '.$body->whereinfo;?>
            <?=$body->getAttributeLabel('whenbuy').': '.$body->whenbuy;?>
            <?=$body->getAttributeLabel('wasdog').': '.$body->wasdog;?>
            <?=$body->getAttributeLabel('havepet').': '.$body->havepet;?>
            <?=$body->getAttributeLabel('havechildren').': '.$body->havechildren;?>
            <?=$body->getAttributeLabel('olddog').': '.$body->olddog;?>
            <?=$body->getAttributeLabel('sexdog').': '.$body->sexdog;?>
            <?=$body->getAttributeLabel('colordog').': '.$body->colordog;?>
            <?=$body->getAttributeLabel('typedog').': '.$body->typedog;?>
            <?=$body->body;?>

Follow the link below to reset your password:

<?= Html::a(Html::encode($resetLink), $resetLink) ?>
