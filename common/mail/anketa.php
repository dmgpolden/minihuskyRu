<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user common\models\User */
?>
            Анкета с сайта <?=Yii::$app->request->hostInfo; ?>
            <div>
            <?=$body->getAttributeLabel('username').': '.$body->username;?>
            <?=$body->getAttributeLabel('old').': '.$body->old;?>
            </div>
            <div>
            <?=$body->getAttributeLabel('country').': '.$body->country;?>
            <?=$body->getAttributeLabel('city').': '.$body->city;?>
            </div>
            <div>
            <?=$body->getAttributeLabel('tel').': '.$body->tel;?>
            <?=$body->getAttributeLabel('email').': '.$body->email;?>
            </div>
            <div>
            <?=$body->getAttributeLabel('whereinfo').': '.$body->whereinfo;?>
            </div>
            <div>
            <?=$body->getAttributeLabel('whenbuy').': '.$body->whenbuy;?>
            </div>
            <div>
            <?=$body->getAttributeLabel('wasdog').': '?>
            <?=($body->wasdog==1)?'ДА':'НЕТ';?>
            </div>
            <div>
            <?=$body->getAttributeLabel('havepet').': '?>
            <?=($body->havepet==1)?'ДА':'НЕТ'?>
            </div>
            <div>
            <?=$body->getAttributeLabel('havechildren').': '?>
            <?=($body->havechildren==1)?'ДА':'НЕТ'?>
            </div>
            <div>
            <?=$body->getAttributeLabel('olddog').': '.$body->getOption('olddog',$body->olddog);?>
            </div>
            <div>
            <?=$body->getAttributeLabel('sexdog').': '.$body->getOption('sexdog',$body->sexdog);?>
            </div>
            <div>
            <?=$body->getAttributeLabel('colordog').': '.$body->getOption('colordog',$body->colordog);?>
            </div>
            <div>
            <?=$body->getAttributeLabel('eyedog').': '.$body->getOption('eyedog',$body->eyedog);?>
            </div>
            <div>
            <?=$body->getAttributeLabel('typedog').': '.$body->getOption('typedog',$body->typedog);?>
            </div>
            <div>
            <?=$body->getAttributeLabel('typefor').': '.$body->getOption('typefor',$body->typefor);?>
            </div>
            <div>
            <?=$body->body;?>
            </div>

