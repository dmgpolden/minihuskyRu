<?php

use yii\db\Schema;
use yii\db\Migration;

class m151217_221709_add_tag_title extends Migration
{
    public function up()
    {
        $this->addColumn('{{%page}}', 't_title', Schema::TYPE_STRING);
        $this->addColumn('{{%post}}', 't_title', Schema::TYPE_STRING);

    }

    public function down()
    {
        $this->dropColumn('{{%page}}', 't_title');
        $this->dropColumn('{{%post}}', 't_title');
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
