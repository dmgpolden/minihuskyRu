<?php

use yii\db\Schema;
use yii\db\Migration;

class m150514_174114_parent_id_for_page extends Migration
{
    public function up()
    {
        $this->addColumn('{{%page}}', 'parent_id', Schema::TYPE_INTEGER);

    }

    public function down()
    {
        $this->dropColumn('{{%page}}', 'parent_id');
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
