<?php

use yii\db\Schema;
use yii\db\Migration;

class m150507_111006_change_column_root_to_tree_all extends Migration
{
    public function up()
    {
        $this->renameColumn('{{%post_category}}','root','tree');
        $this->renameColumn('{{%page}}','root','tree');
    }

    public function down()
    {
        $this->renameColumn('{{%post_category}}','tree', 'root');
        $this->renameColumn('{{%page}}',         'tree', 'root');
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
