<?php

use yii\db\Schema;
use yii\db\Migration;

class m150514_160831_change_column_level_depth_page extends Migration
{
    public function up()
    {
        $this->renameColumn('{{%page}}','level','depth');
    }

    public function down()
    {
        $this->renameColumn('{{%page}}','depth','level');
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
