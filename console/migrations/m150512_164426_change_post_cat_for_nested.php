<?php

use yii\db\Schema;
use yii\db\Migration;

class m150512_164426_change_post_cat_for_nested extends Migration
{
    public function up()
    {
        $this->renameColumn('{{%post_category}}','level','depth');
    }

    public function down()
    {
        $this->renameColumn('{{%post_category}}','depth','level');
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
