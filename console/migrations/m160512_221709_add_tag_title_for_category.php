<?php

use yii\db\Schema;
use yii\db\Migration;

class m160512_221709_add_tag_title_for_category extends Migration
{
    public function up()
    {
        $this->addColumn('{{%post_category}}', 't_title', Schema::TYPE_STRING);

    }

    public function down()
    {
        $this->dropColumn('{{%post_careteory}}', 't_title');

    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
