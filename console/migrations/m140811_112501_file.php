<?php

use yii\db\Schema;
use yii\db\Migration;

class m140811_112501_file extends Migration
{
    public function up()
    {
				$tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

			$this->createTable('{{%file}}', [
            'id' => Schema::TYPE_PK,
            'name' => Schema::TYPE_STRING . ' NOT NULL',
            'ext' => Schema::TYPE_STRING . ' NOT NULL',
            'size' => Schema::TYPE_STRING . ' NOT NULL',
            'type' => Schema::TYPE_STRING . ' NOT NULL',
    		'title' => Schema::TYPE_STRING,
			'description' => Schema::TYPE_TEXT,
            'datetime' => Schema::TYPE_TIMESTAMP . ' DEFAULT NOW()',
        ], $tableOptions);
				$this->createIndex(  'name', '{{%file}}', 'name');

    }

    public function down()
    {
					if ( Yii::$app->db->getTableSchema( '{{%file}}'))	
						$this->dropTable(  '{{%file}}');
    }
}
