<?php

use yii\db\Schema;
use yii\db\Migration;

class m140928_201141_shop extends Migration
{
    public function up()
    {
				$tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

			$this->createTable('{{%shop_category}}', [
                    'id' => Schema::TYPE_PK,
                    'root' => Schema::TYPE_INTEGER,
                    'lft' => Schema::TYPE_INTEGER,
                    'rgt' => Schema::TYPE_INTEGER,
                    'level' => Schema::TYPE_INTEGER,
                    'name' => 'VARCHAR(255) NOT NULL DEFAULT \'Имя раздела\'',
                    'slug' => 'VARCHAR(255) NULL',
                    'caption' => 'VARCHAR(255) NULL',
                    'content' => 'LONGTEXT NULL',
                    'visible' => 'TINYINT(1) NULL DEFAULT \'1\'',
                    'note' => 'VARCHAR(200) NULL',
                    'description' => 'VARCHAR(255) NULL',
                    'title' => 'VARCHAR(255) NULL',
                    'keywords' => 'VARCHAR(255) NULL',
                    'removable' => 'TINYINT(1) NOT NULL DEFAULT \'1\'',
                    'image_id' => 'INT(8) NULL',
                    'present' => 'TINYINT(1) NULL',
                    'box' => 'TINYINT(1) NULL',
                    'textbox' => 'TINYINT(1) NOT NULL',
					], $tableOptions);	
				$this->createIndex(  'root', '{{%shop_category}}', 'root');
				$this->createIndex(  'lft', '{{%shop_category}}',  'lft');
				$this->createIndex(  'rgt', '{{%shop_category}}',  'rgt');
				$this->createIndex(  'level', '{{%shop_category}}','level');
				$this->createIndex(  'slug', '{{%shop_category}}','slug');

			$this->insert( '{{%shop_category}}', ['name'=>'/', 'root'=> 1, 'level' => 1, 'lft' =>1,'rgt'=>2 ]);
			
			$this->createTable('{{%shop_brand}}', [
                    'id' => Schema::TYPE_PK,
                    'name' => 'VARCHAR(255) NOT NULL DEFAULT \'наименование\'',
                    'slug' => 'VARCHAR(255) NULL',
                    'caption' => 'VARCHAR(255) NULL',
                    'content' => 'LONGTEXT NULL',
                    'note' => 'VARCHAR(200) NULL',
                    'visible' => 'TINYINT(1) NULL DEFAULT \'1\'',
                    'sequence' => 'INT(2) NULL DEFAULT \'1\'',
                    'title' => 'VARCHAR(255) NULL',
                    'description' => 'VARCHAR(255) NULL',
                    'keywords' => 'VARCHAR(255) NULL',
                    'present' => 'TINYINT(1) NULL',
                    'box' => 'TINYINT(1) NULL',
                    'textbox' => 'TINYINT(1) NOT NULL',
					], $tableOptions);	

			$this->createTable('{{%shop_product}}', [
            'id' => Schema::TYPE_PK,
            'category_id' => Schema::TYPE_INTEGER,
            'brand_id' => Schema::TYPE_INTEGER,
						'name' => 'VARCHAR(255) NOT NULL DEFAULT \'наименование\'',
						'slug' => 'VARCHAR(255) NULL',
						'caption' => 'VARCHAR(255) NULL',
						'content' => 'LONGTEXT NULL',
						'note' => 'VARCHAR(200) NULL',
						'visible' => 'TINYINT(1) NULL DEFAULT \'1\'',
						'sequence' => 'INT(2) NOT NULL DEFAULT \'1\'',
						'title' => 'VARCHAR(255) NULL',
						'description' => 'VARCHAR(255) NULL',
						'keywords' => 'VARCHAR(255) NULL',
						'present' => 'TINYINT(1) NULL',
						'box' => 'TINYINT(1) NULL',
						'textbox' => 'TINYINT(1) NOT NULL',
					], $tableOptions);	
				$this->createIndex(  'category_idx', '{{%shop_product}}', 'category_id');
				$this->createIndex(  'brand_idx', '{{%shop_product}}', 'brand_id');
		$this->	addForeignKey( 'fk_shop_product_category', '{{%shop_product}}', 'category_id', '{{%shop_category}}', 'id',  $delete = null, $update = null );
		$this->	addForeignKey( 'fk_shop_product_brand', '{{%shop_product}}', 'brand_id', '{{%shop_brand}}', 'id',  $delete = null, $update = null );


            $this->createTable('{{%shop_prod_file}}', [
            'id' => Schema::TYPE_PK,
            'prod_id' => Schema::TYPE_INTEGER,
            'file_id' => Schema::TYPE_INTEGER,
            ]);
            $this->createIndex(  'prod', '{{%shop_prod_file}}','prod_id');
            $this->createIndex(  'file', '{{%shop_prod_file}}','file_id');
			
            $this->createTable('{{%shop_file}}', [
            'id' => Schema::TYPE_PK,
            'name' => Schema::TYPE_STRING . ' NOT NULL',
            'ext' => Schema::TYPE_STRING . ' NOT NULL',
            'size' => Schema::TYPE_STRING . ' NOT NULL',
            'type' => Schema::TYPE_STRING . ' NOT NULL',
    		'title' => Schema::TYPE_STRING,
			'description' => Schema::TYPE_TEXT,
            'datetime' => Schema::TYPE_TIMESTAMP . ' DEFAULT NOW()',
        ], $tableOptions);
				$this->createIndex(  'name', '{{%shop_file}}', 'name');


    }

    public function down()
    {
			$this->dropTable('{{%shop_product}}');
			$this->dropTable('{{%shop_category}}');
    }
}
