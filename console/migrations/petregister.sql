SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

CREATE SCHEMA IF NOT EXISTS `mydb` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
USE `mydb` ;

-- -----------------------------------------------------
-- Table `mydb`.`breed`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `mydb`.`breed` (
  `id` INT NOT NULL ,
  `name` VARCHAR(255) NULL ,
  `parent` INT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`standart`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `mydb`.`standart` (
  `id` INT NOT NULL ,
  `name` VARCHAR(45) NULL ,
  `breed_id` INT NOT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_standart_breed1_idx` (`breed_id` ASC) ,
  CONSTRAINT `fk_standart_breed1`
    FOREIGN KEY (`breed_id` )
    REFERENCES `mydb`.`breed` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`class`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `mydb`.`class` (
  `id` INT NOT NULL ,
  `name` VARCHAR(45) NULL ,
  `breed_id` INT NOT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_class_breed1_idx` (`breed_id` ASC) ,
  CONSTRAINT `fk_class_breed1`
    FOREIGN KEY (`breed_id` )
    REFERENCES `mydb`.`breed` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`pet`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `mydb`.`pet` (
  `id` INT NOT NULL ,
  `name` VARCHAR(45) NULL ,
  `fullname` VARCHAR(255) NULL ,
  `birthday` DATETIME NULL ,
  `dieday` DATETIME NULL ,
  `weight` INT NULL ,
  `stature` INT NULL ,
  `tbl_user_uid` INT(11) NOT NULL ,
  `breed_id` INT NOT NULL ,
  `standart_id` INT NOT NULL ,
  `class_id` INT NOT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_pet_breed1_idx` (`breed_id` ASC) ,
  INDEX `fk_pet_standart1_idx` (`standart_id` ASC) ,
  INDEX `fk_pet_class1_idx` (`class_id` ASC) ,
  CONSTRAINT `fk_pet_breed1`
    FOREIGN KEY (`breed_id` )
    REFERENCES `mydb`.`breed` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_pet_standart1`
    FOREIGN KEY (`standart_id` )
    REFERENCES `mydb`.`standart` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_pet_class1`
    FOREIGN KEY (`class_id` )
    REFERENCES `mydb`.`class` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`action`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `mydb`.`action` (
  `id` INT NOT NULL ,
  `name` VARCHAR(45) NULL COMMENT '		' ,
  `desc` VARCHAR(255) NULL ,
  `text` TEXT NULL ,
  `date` DATETIME NULL ,
  `count` INT NULL ,
  `action_id` INT NOT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_action_action1_idx` (`action_id` ASC) ,
  CONSTRAINT `fk_action_action1`
    FOREIGN KEY (`action_id` )
    REFERENCES `mydb`.`action` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`action_has_dog`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `mydb`.`action_has_dog` (
  `action_id` INT NOT NULL ,
  `dog_id` INT NOT NULL ,
  `class_id` INT NOT NULL ,
  `standart_id` INT NOT NULL ,
  `breed_id` INT NOT NULL ,
  `action_id` INT NOT NULL ,
  PRIMARY KEY (`action_id`, `dog_id`) ,
  INDEX `fk_event_has_dog_dog1_idx` (`dog_id` ASC) ,
  INDEX `fk_event_has_dog_class1_idx` (`class_id` ASC) ,
  INDEX `fk_event_has_dog_standart1_idx` (`standart_id` ASC) ,
  INDEX `fk_event_has_dog_breed1_idx` (`breed_id` ASC) ,
  INDEX `fk_event_has_dog_action1_idx` (`action_id` ASC) ,
  CONSTRAINT `fk_event_has_dog_dog1`
    FOREIGN KEY (`dog_id` )
    REFERENCES `mydb`.`pet` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_event_has_dog_class1`
    FOREIGN KEY (`class_id` )
    REFERENCES `mydb`.`class` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_event_has_dog_standart1`
    FOREIGN KEY (`standart_id` )
    REFERENCES `mydb`.`standart` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_event_has_dog_breed1`
    FOREIGN KEY (`breed_id` )
    REFERENCES `mydb`.`breed` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_event_has_dog_action1`
    FOREIGN KEY (`action_id` )
    REFERENCES `mydb`.`action` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`registrator`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `mydb`.`registrator` (
  `id` INT NOT NULL ,
  `name` VARCHAR(45) NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`pet_has_registrator`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `mydb`.`pet_has_registrator` (
  `pet_id` INT NOT NULL ,
  `registrator_id` INT NOT NULL ,
  `number` VARCHAR(45) NULL ,
  PRIMARY KEY (`pet_id`, `registrator_id`) ,
  INDEX `fk_pet_has_registrator_registrator1_idx` (`registrator_id` ASC) ,
  INDEX `fk_pet_has_registrator_pet1_idx` (`pet_id` ASC) ,
  CONSTRAINT `fk_pet_has_registrator_pet1`
    FOREIGN KEY (`pet_id` )
    REFERENCES `mydb`.`pet` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_pet_has_registrator_registrator1`
    FOREIGN KEY (`registrator_id` )
    REFERENCES `mydb`.`registrator` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`files`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `mydb`.`files` (
  `id_file` INT(8) NOT NULL AUTO_INCREMENT COMMENT 'id' ,
  `file_path` VARCHAR(255) NOT NULL COMMENT 'Путь к файлу' ,
  `id_file_type` INT(8) NULL DEFAULT NULL COMMENT 'Тип файла' ,
  `count` INT(6) NULL DEFAULT NULL COMMENT 'Количество загрузок' ,
  `id_object` VARCHAR(255) NULL DEFAULT NULL COMMENT 'Объект' ,
  `id_instance` INT(8) NULL DEFAULT NULL COMMENT 'ИД экземпляра' ,
  `id_parameter` VARCHAR(255) NULL DEFAULT NULL COMMENT 'Свойство объекта' ,
  `create_date` INT(10) NULL DEFAULT NULL COMMENT 'Дата создания файла' ,
  `status_process` TINYINT(1) NOT NULL DEFAULT '0' COMMENT 'Статус создания превью-файла' ,
  PRIMARY KEY (`id_file`) ,
  INDEX `id_tmp` (`id_object` ASC, `id_instance` ASC, `id_parameter` ASC, `id_file_type` ASC, `file_path` ASC) )
ENGINE = InnoDB
AUTO_INCREMENT = 2102
DEFAULT CHARACTER SET = utf8
COMMENT = 'Файлы';


-- -----------------------------------------------------
-- Table `mydb`.`menu`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `mydb`.`menu` (
  `id` INT(8) NOT NULL AUTO_INCREMENT COMMENT 'id' ,
  `name_ru` VARCHAR(255) NOT NULL DEFAULT 'Имя раздела' COMMENT 'Название в меню' ,
  `caption_ru` VARCHAR(255) NULL DEFAULT NULL COMMENT 'Заголовок раздела' ,
  `content_ru` LONGTEXT NULL DEFAULT NULL COMMENT 'Содержимое раздела' ,
  `visible` TINYINT(1) NULL DEFAULT '1' COMMENT 'Видимость' ,
  `id_parent` INT(8) NULL DEFAULT NULL COMMENT 'Смена родительского раздела' ,
  `sequence` INT(2) NOT NULL DEFAULT '1' COMMENT '&nbsp;' ,
  `note` VARCHAR(200) NULL DEFAULT NULL COMMENT 'Примечания' ,
  `alias` VARCHAR(255) NULL DEFAULT NULL COMMENT 'В адресной строке' ,
  `title_teg` VARCHAR(255) NULL DEFAULT NULL COMMENT '<title>' ,
  `meta_description` VARCHAR(255) NULL DEFAULT NULL COMMENT '<meta name=\"description\">' ,
  `meta_keywords` VARCHAR(255) NULL DEFAULT NULL COMMENT '<meta name=\"keywords\">' ,
  `go_to_type` INT(8) NOT NULL DEFAULT '6' COMMENT 'При отсутствии контента:' ,
  `id_module_template` INT(8) NULL DEFAULT NULL COMMENT 'Набор модулей' ,
  `external_link` VARCHAR(255) NULL DEFAULT NULL COMMENT 'Ссылка (авто)' ,
  `external_link_type` TINYINT(1) NULL DEFAULT NULL COMMENT 'Открывать в новом окне (авто)' ,
  `removable` TINYINT(1) NOT NULL DEFAULT '1' COMMENT 'Разрешить удаление' ,
  `image` INT(8) NULL DEFAULT NULL COMMENT 'Картинка для раздела' ,
  `caption_en` VARCHAR(255) NULL DEFAULT NULL COMMENT 'Заголовок раздела EN' ,
  `name_en` VARCHAR(255) NULL DEFAULT 'name category' COMMENT 'Название в меню EN' ,
  `content_en` LONGTEXT NULL DEFAULT NULL COMMENT 'Содержимое раздела EN' ,
  `present` TINYINT(1) NULL DEFAULT '0' COMMENT 'В презент' ,
  `level` INT(8) NULL DEFAULT '0' COMMENT 'Вложенность' ,
  `box` TINYINT(1) NULL DEFAULT '0' COMMENT 'Бокс' ,
  `textbox` TINYINT(1) NOT NULL DEFAULT '0' COMMENT 'Text Box' ,
  PRIMARY KEY (`id`) ,
  UNIQUE INDEX `alias` (`alias` ASC) ,
  INDEX `id` (`id` ASC, `id_parent` ASC, `sequence` ASC) )
ENGINE = InnoDB
AUTO_INCREMENT = 191
DEFAULT CHARACTER SET = utf8
COMMENT = 'Меню';

USE `mydb` ;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
