<?php

use yii\db\Schema;
use yii\db\Migration;

class m140808_204219_post extends Migration
{
    public function up()
    {
                $tableOptions = null;
        if ($this->db->drivername === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }
            $this->createTable('{{%post}}', [
            'id' => Schema::TYPE_PK,
                        'title' => 'VARCHAR(255) NOT NULL DEFAULT \'Имя раздела\'',
                        'category_id' => Schema::TYPE_INTEGER,
                        'create_at' => 'DATE',
                        'caption' => 'VARCHAR(255) NULL',
                        'content' => 'LONGTEXT NULL',
                        'visible' => 'TINYINT(1) NULL ',
                        'sequence' => 'INT(2) NULL ',
                        'note' => 'VARCHAR(200) NULL',
                        'slug' => 'VARCHAR(255) NULL',
                        'title' => 'VARCHAR(255) NULL',
                        'description' => 'VARCHAR(255) NULL',
                        'keywords' => 'VARCHAR(255) NULL',
                        'go_to_type' => 'INT(8) NULL ',
                        'external_link' => 'VARCHAR(255) NULL',
                        'external_link_type' => 'TINYINT(1) NULL',
                        'removable' => 'TINYINT(1) NULL ',
                        'image_id' => 'INT(8) NULL',
                        'present' => 'TINYINT(1) NULL',
                        'box' => 'TINYINT(1) NULL',
                        'textbox' => 'TINYINT(1) NULL',
                    ], $tableOptions);
                $this->createIndex(  'slug', '{{%post}}','slug');
                $this->createIndex(  'category_id', '{{%post}}','category_id');
            $this->insert( '{{%post}}', ['title'=>'первый пункт новость ', 'slug'=>'raz' ,  'id'=> 1,
                    'category_id'=>1]);
            $this->insert( '{{%post}}', ['title'=>'2 пункт новость', 'slug'=>'dva', 'id'=> 2, 'category_id'=>1]);
            $this->insert( '{{%post}}', ['title'=>'test punkt post', 'slug'=>'tre', 'id'=> 3, 'category_id'=>1]);

            $this->createTable('{{%post_file}}', [
            'id' => Schema::TYPE_PK,
            'post_id' => Schema::TYPE_INTEGER,
            'file_id' => Schema::TYPE_INTEGER,
            ]);
                $this->createIndex(  'post', '{{%post_file}}','post_id');
                $this->createIndex(  'file', '{{%post_file}}','file_id');

			$this->createTable('{{%post_category}}', [
                    'id' => Schema::TYPE_PK,
                    'root' => Schema::TYPE_INTEGER,
                    'lft' => Schema::TYPE_INTEGER,
                    'rgt' => Schema::TYPE_INTEGER,
                    'level' => Schema::TYPE_INTEGER,
                    'title' => 'VARCHAR(255) NOT NULL DEFAULT \'Имя раздела\'',
                    'slug' => 'VARCHAR(255) NULL',
                    'caption' => 'VARCHAR(255) NULL',
                    'content' => 'LONGTEXT NULL',
                    'visible' => 'TINYINT(1) NULL DEFAULT \'1\'',
                    'note' => 'VARCHAR(200) NULL',
                    'description' => 'VARCHAR(255) NULL',
                    'title' => 'VARCHAR(255) NULL',
                    'keywords' => 'VARCHAR(255) NULL',
                    'removable' => 'TINYINT(1) NOT NULL DEFAULT \'1\'',
                    'image_id' => 'INT(8) NULL',
                    'present' => 'TINYINT(1) NULL',
                    'box' => 'TINYINT(1) NULL',
                    'textbox' => 'TINYINT(1) NOT NULL',
					], $tableOptions);	
				$this->createIndex('root', '{{%post_category}}', 'root');
				$this->createIndex('lft', '{{%post_category}}',  'lft');
				$this->createIndex('rgt', '{{%post_category}}',  'rgt');
				$this->createIndex('level', '{{%post_category}}','level');
				$this->createIndex('slug', '{{%post_category}}','slug');

$this->insert( '{{%post_category}}', ['title'=>'Новости', 'root'=> 1, 'level' => 1, 'lft' =>1,'rgt'=>2 ]);

    }

    public function down()
    {
            $this->dropTable('{{%post}}');
            $this->dropTable('{{%post_file}}');
            $this->dropTable('{{%post_category}}');
    }
}
