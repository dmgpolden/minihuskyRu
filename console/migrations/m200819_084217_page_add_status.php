<?php

use yii\db\Migration;
use yii\db\Schema;

class m200819_084217_page_add_status extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->addColumn('{{%page}}', 'status', Schema::TYPE_SMALLINT);

        $this->createTable('{{%status}}', [
            'id' => Schema::TYPE_PK,
            'name' => Schema::TYPE_STRING . ' NOT NULL',
            'slug' => Schema::TYPE_STRING . ' NOT NULL',
            ], $tableOptions);

        $this->addColumn('{{%status}}', 'file', Schema::TYPE_STRING);
        $this->addColumn('{{%status}}', 'position', Schema::TYPE_STRING);
    }	

    public function safeDown()
    {
        $this->dropColumn('{{%page}}', 'status');
        $this->dropTable('{{%status}}');
    }
}
