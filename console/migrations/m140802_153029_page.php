<?php

use yii\db\Schema;
use yii\db\Migration;

class m140802_153029_page extends Migration
{
    public function up()
    {
				$tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

			$this->createTable('{{%page}}', [
            'id' => Schema::TYPE_PK,
            'root' => Schema::TYPE_INTEGER,
            'lft' => Schema::TYPE_INTEGER,
            'rgt' => Schema::TYPE_INTEGER,
            'level' => Schema::TYPE_INTEGER,
						'title' => 'VARCHAR(255) NOT NULL DEFAULT \'Имя раздела\'',
						'caption' => 'VARCHAR(255) NULL',
						'content' => 'LONGTEXT NULL',
						'visible' => 'TINYINT(1) NULL DEFAULT \'1\'',
						'sequence' => 'INT(2) NOT NULL DEFAULT \'1\'',
						'note' => 'VARCHAR(200) NULL',
						'slug' => 'VARCHAR(255) NULL',
						'title' => 'VARCHAR(255) NULL',
						'description' => 'VARCHAR(255) NULL',
						'keywords' => 'VARCHAR(255) NULL',
						'go_to_type' => 'INT(8) NOT NULL DEFAULT \'6\'',
						'external_link' => 'VARCHAR(255) NULL',
						'external_link_type' => 'TINYINT(1) NULL',
						'removable' => 'TINYINT(1) NOT NULL DEFAULT \'1\'',
						'image_id' => 'INT(8) NULL',
						'present' => 'TINYINT(1) NULL',
						'level' => 'INT(8) NULL',
						'box' => 'TINYINT(1) NULL',
						'textbox' => 'TINYINT(1) NOT NULL',
					], $tableOptions);	
				$this->createIndex(  'root', '{{%page}}', 'root');
				$this->createIndex(  'lft', '{{%page}}',  'lft');
				$this->createIndex(  'rgt', '{{%page}}',  'rgt');
				$this->createIndex(  'level', '{{%page}}','level');
				$this->createIndex(  'slug', '{{%page}}','slug');

			$this->insert( '{{%page}}', ['title'=>'/', 'root'=> 1, 'level' => 1, 'lft' =>1,'rgt'=>2 ]);
			$this->insert( '{{%page}}', ['title'=>'первый пункт меню ', 'slug'=>'raz' ,  'root'=> 1, 'level' => 2, 'lft' =>2,'rgt'=>5 ]);
			$this->insert( '{{%page}}', ['title'=>'2 пункт меню', 'slug'=>'dva', 'root'=> 1, 'level' => 3, 'lft' =>3,'rgt'=>4 ]);
			$this->insert( '{{%page}}', ['title'=>'test punkt page', 'slug'=>'tre', 'root'=> 1, 'level' => 2, 'lft' =>6,'rgt'=>7 ]);


            $this->createTable('{{%page_file}}', [
            'id' => Schema::TYPE_PK,
            'page_id' => Schema::TYPE_INTEGER,
            'file_id' => Schema::TYPE_INTEGER,
            ]);
    }

    public function down()
    {
			$this->dropTable('{{%page}}');
           $this->dropTable('{{%page_file}}');
    }
}
