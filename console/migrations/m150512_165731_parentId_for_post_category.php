<?php

use yii\db\Schema;
use yii\db\Migration;

class m150512_165731_parentId_for_post_category extends Migration
{
    public function up()
    {
        $this->addColumn('{{%post_category}}', 'parent_id', Schema::TYPE_INTEGER);

    }

    public function down()
    {
        $this->dropColumn('{{%post_category}}', 'parent_id');
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
