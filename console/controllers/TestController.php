<?php

namespace console\controllers;

use common\components\ImageWatermark;
use common\modules\file\models\File;
use frontend\modules\page\models\Page;
use frontend\modules\post\models\Post;
use Yii;
use yii\console\Controller;
use yii\helpers\BaseVarDumper;
use yii\helpers\FileHelper;

class TestController extends Controller
{
       
    public function actionUpdateidx(){
       $sel = Post::find()->select(['id','content','visible'])->asArray()->all();

        Yii::$app->sphinx->createCommand()
            ->truncateIndex('{{%post}}')
            ->execute();

        foreach($sel as $s){
            $ins[]=[$s['id'],$s['content'],$s['visible']];
            
        }
//        print_r($ins);

              Yii::$app->sphinx->createCommand()
                ->batchInsert('{{%post}}', ['id','content','visible'],$ins)
                 ->execute();
        
       $sel = Page::find()->select(['id','content','visible'])->asArray()->all();
        Yii::$app->sphinx->createCommand()
            ->truncateIndex('{{%page}}')
            ->execute();

        foreach($sel as $s){
            $ins[]=[$s['id'],$s['content'],$s['visible']];
        }
        Yii::$app->sphinx->createCommand()
            ->batchInsert('{{%page}}', ['id','content','visible'],$ins)
            ->execute();
    }

    public function actionFiledel()
    {
        $files = File::find()->all();
        foreach(
            \Yii::$app->db->createCommand('SELECT file_id FROM post_file UNION SELECT file_id FROM page_file')->queryAll()
                as $file){
             $itemFiles[] = $file['file_id'];
        }
            $this->stdout(print_r( $itemFiles ) ) ;
        foreach($files as $file){
             $db_files[]=substr($file->name,1 );
             $db_id_files[]=$file->id;
        }
/*         foreach ( FileHelper::findFiles(Yii::getAlias('@static/image'), ['except' => ['.gitignore']] ) as $file) {
             $listfile[]=basename($file);
         }
            foreach (array_diff ($listfile ,$db_files ) as $file ){
                    $this->stdout(var_dump(Yii::getAlias('@static/image/'.$file)));
                if (file_exists(Yii::getAlias('@static/image/'.$file))){
                    rename(Yii::getAlias('@static/image/'.$file), Yii::getAlias('@static/temp/'.$file));
                }
            }
            $this->stdout(var_dump(array_diff ($listfile ,$db_files ) ) ) ;
*/
            $file_diff = array_diff ($db_id_files ,$itemFiles ) ;
        $files = File::find()->where(['id'=>$file_diff])->all();
        foreach ($files as $file ){
            $this->stdout( print_r($file->name) ) ;
        }
            //$this->stdout(print_r($file_diff ) ) ;
    }
}

function vd(  $var, $exit = true)
    {
        $dumper = new BaseVarDumper(  );

        echo $dumper::dump(  $var, 10, true);
          if (  $exit)
           exit;
    }
