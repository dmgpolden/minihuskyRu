<?php

namespace console\controllers;

use Yii;
use yii\console\Controller;

class RbacController extends Controller
{
    public function actionGenerate()
    {
        $auth = Yii::$app->authManager;

        // add "createShop" permission
        $createShop = $auth->createPermission('createShop');
        $createShop->description = 'Create a shop';
        $auth->add($createShop);

        // add "updateShop" permission
        $updateShop = $auth->createPermission('updateShop');
        $updateShop->description = 'Update shop';
        $auth->add($updateShop);

        // add "author" role and give this role the "createShop" permission
        $author = $auth->createRole('author');
        $auth->add($author);
        $auth->addChild($author, $createShop);

        // add "admin" role and give this role the "updateShop" permission
        // as well as the permissions of the "author" role
        $admin = $auth->createRole('admin');
        $auth->add($admin);
        $auth->addChild($admin, $updateShop);
        $auth->addChild($admin, $author);

        // Assign roles to users. 1 and 2 are IDs returned by IdentityInterface::getId()
        // usually implemented in your User model.
        $auth->assign($author, 2);
        $auth->assign($admin, 1);
    }
}
