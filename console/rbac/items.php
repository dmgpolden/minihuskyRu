<?php
return [
    'createShop' => [
        'type' => 2,
        'description' => 'Create a shop',
    ],
    'updateShop' => [
        'type' => 2,
        'description' => 'Update shop',
    ],
    'author' => [
        'type' => 1,
        'children' => [
            'createShop',
        ],
    ],
    'admin' => [
        'type' => 1,
        'children' => [
            'updateShop',
            'author',
        ],
    ],
];
