<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-console',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log','gii'],
    'controllerNamespace' => 'console\controllers',
        'controllerMap' => [
/*            'fixture' => [
                'class' => 'yii\faker\FixtureController',
                 'providers' => [
                        'Faker\Provider\ru_RU\Text',
                 ],
                ],
 */
            'test' => [
                'class' => 'console\controllers\TestController',
            ],
        ],
    'modules' => [
                                'gii' => 'yii\gii\Module'
                                ],
    'components' => [
        'authManager' => [
            'class' => 'yii\rbac\PhpManager', // or use 'yii\rbac\DbManager'
            'defaultRoles' => ['admin', 'admin'],
        //    'class' => 'yii\rbac\DbManager', // or use 'yii\rbac\DbManager'
        ],
        'log' => [
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
    ],
    'params' => $params,
];
